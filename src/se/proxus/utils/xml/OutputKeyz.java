package se.proxus.utils.xml;

/*
 * Made by Oliver (2018-11-19, 14:10).
 */

import lombok.experimental.UtilityClass;

@UtilityClass
public class OutputKeyz {
    
    public final static String INDENT_AMOUNT = "{http://xml.apache.org/xslt}indent-amount";
}
