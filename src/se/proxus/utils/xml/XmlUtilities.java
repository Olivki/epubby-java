package se.proxus.utils.xml;
/*
 * Made by Oliver (2018-11-19, 17:01).
 */

import lombok.experimental.UtilityClass;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

@UtilityClass
public class XmlUtilities {
    
    public Node appendContainerTextNode(final Node parent, final Object tagName,
                                        final Object contents) {
        final Document document = parent.getOwnerDocument();
        final Node element = parent.appendChild(document.createElement(String.valueOf(tagName)));
        element.appendChild(document.createTextNode(String.valueOf(contents)));
        return element;
    }
    
    public Node appendComment(final Node parent, final Object data) {
        return parent.appendChild(parent.getOwnerDocument()
                                        .createComment(String.valueOf(data)));
    }
    
}
