package se.proxus.utils.reflection;

import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/*
 * Made by Oliver (2018-11-17, 17:46).
 */

@UtilityClass
public class Fields {
    
    public Method findGetter(final Field field) {
        return Methods.findGetter(field.getName(), field.getDeclaringClass());
    }
    
    @SuppressWarnings("unchecked")
    public <V> V getFieldValue(final Object parent, final Class<V> cls) {
        return (V) Methods.invoke(findGetter(getInstancesOf(parent, cls)[0]), parent);
    }
    
    public <V> boolean hasInstanceOf(final V target, final Class<?> cls) {
        return getInstancesOf(target, cls).length > 0;
    }
    
    // Maybe implement some sort of cache of this in case this will be accessed again?
    public <V> Field[] getInstancesOf(final V target, final Class<?> cls) {
        return Arrays.stream(FieldUtils.getAllFields(target.getClass()))
                     .filter(f -> f.getType()
                                   .equals(cls))
                     .toArray(Field[]::new);
    }
    
    public <V> boolean hasExplicitInstanceOf(final V target, final Class<?> cls) {
        return getExplicitInstancesOf(target, cls).length > 0;
    }
    
    // Maybe implement some sort of cache of this in case this will be accessed again?
    public <V> Field[] getExplicitInstancesOf(final V target, final Class<?> cls) {
        return Arrays.stream(target.getClass()
                                   .getDeclaredFields())
                     .filter(f -> f.getType()
                                   .equals(cls))
                     .toArray(Field[]::new);
    }
    
    public Field[] getFieldsWithAnnotations(final Class<?> cls) {
        return Arrays.stream(cls.getDeclaredFields())
                     .filter(f -> Objects.nonNull(f.getDeclaredAnnotations()))
                     .toArray(Field[]::new);
    }
    
    public Field[] getFieldsWithAnnotation(final Class<?> cls,
                                           final Class<? extends Annotation> annotationCls) {
        final Field[] allFields = cls.getDeclaredFields();
        final List<Field> annotatedFields = new ArrayList<>();
        
        for (final Field field : allFields) {
            if (field.getAnnotation(annotationCls) != null) {
                annotatedFields.add(field);
            }
        }
        
        return annotatedFields.toArray(new Field[0]);
    }
    
}