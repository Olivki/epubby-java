package se.proxus.utils.reflection;

import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.WordUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

/*
 * Made by Oliver (2018-11-19, 16:16).
 */

@UtilityClass
public class Methods {
    
    @SuppressWarnings("unchecked")
    public Object invoke(final Method method, final Object parent) {
        try {
            return method.invoke(parent);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        
        return null;
    }
    
    // Credits http://asgteach.com/2012/11/finding-getters-and-setters-with-java-reflection/
    public static Method[] findGettersAndSetters(final Class<?> cls) {
        final List<Method> foundMethods = new ArrayList<>();
        final Method[] methods = cls.getMethods(); // Change to getDeclaredMethods() if you only
        // want to get the methods in the one class, and all methods that aren't public in that one.
        
        for (final Method method : methods) {
            if (isGetter(method) || isSetter(method)) {
                foundMethods.add(method);
            }
        }
        
        return foundMethods.toArray(new Method[0]);
    }
    
    public static Method[] findGetters(final Class<?> cls) {
        final List<Method> foundMethods = new ArrayList<>();
        final Method[] methods = cls.getMethods();
        
        for (final Method method : methods) {
            if (isGetter(method)) {
                foundMethods.add(method);
            }
        }
        
        return foundMethods.toArray(new Method[0]);
    }
    
    public static Method[] findSetters(final Class<?> cls) {
        final List<Method> foundMethods = new ArrayList<>();
        final Method[] methods = cls.getMethods();
        
        for (final Method method : methods) {
            if (isSetter(method)) {
                foundMethods.add(method);
            }
        }
        
        return foundMethods.toArray(new Method[0]);
    }
    
    // Will break on any kind of obfuscation.
    public static Method findGetter(final String name, final Class<?> cls) {
        for (final Method method : findGetters(cls)) {
            if (method.getName().matches("^get" + WordUtils.capitalize(name) + ".*")) {
                return method;
            }
        }
        
        return null;
    }
    
    public static Method findSetter(final String name, final Class<?> cls) {
        for (final Method method : findSetters(cls)) {
            if (StringUtils.containsIgnoreCase(method.getName(), name)) {
                return method;
            }
        }
        
        return null;
    }
    
    private static boolean isGetter(final Method method) {
        if (Modifier.isPublic(method.getModifiers()) && method.getParameterTypes().length == 0) {
            if (method.getName()
                      .matches("^get[A-Z].*") && !method.getReturnType()
                                                        .equals(void.class)) {
                return true;
            }
            
            if (method.getName()
                      .matches("^is[A-Z].*") && method.getReturnType()
                                                      .equals(boolean.class)) {
                return true;
            }
        }
        
        return false;
    }
    
    private static boolean isSetter(final Method method) {
        return Modifier.isPublic(method.getModifiers()) && method.getReturnType()
                                                                 .equals(void.class) &&
               method.getParameterTypes().length == 1 && method.getName()
                                                               .matches("^set[A-Z].*");
    }
    
}