package se.proxus.utils.pairs;

import lombok.Data;
import lombok.NonNull;

/**
 * <p>A simple implementation of an
 * <a href="https://en.wikipedia.org/wiki/Unordered_pair">Unordered Pair</a>
 * . <br/>In this case, they're labeled as a <b>key</b> and a <b>value</b>, but the naming serves
 * no greater function, they could very well just be labeled <b>a</b> & <b>b</b>. <br/>The
 * {@link #key} variable is immutable, and can therefore not be modified, but the {@link #value}
 * variable can be modified. <br/>For a fully mutable version, see {@link UnorderedPair} and for
 * a fully immutable version, see {@link ImmutablePair}.
 * </p>
 *
 * @param <K>
 *     The class type of the {@link #key} variable.
 * @param <V>
 *     The class type of the {@link #value} variable.
 */
@Data
public final class Pair<K, V> {
    
    private final K key;
    @NonNull private V value;
    
}
