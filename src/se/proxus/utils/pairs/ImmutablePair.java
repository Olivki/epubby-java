package se.proxus.utils.pairs;

import lombok.Data;

/**
 * <p>A simple immutable pair class. <br/>It can not be instantiated so to create a new instance
 * of it you'll need to use the {@link #create(Object, Object)} method. <br/> Any changes made to
 * the value will result in a new instance of this class being passed back.
 * </p>
 *
 * @param <K>
 *     The key parameter.
 * @param <V>
 *     The value parameter
 */
@Data
public final class ImmutablePair<K, V> {
    
    private final K key;
    private final V value;
    
    private ImmutablePair(final K key, final V value) {
        this.key = key;
        this.value = value;
    }
    
    /**
     * <p>Gives back a new instance of {@link ImmutablePair} with the already defined {@link #key}
     * and the {@link #value} set as the one given in the method.</p>
     *
     * @param value
     *     The new value to be set.
     *
     * @return A new instance of {@link ImmutablePair} with the {@link #key} & {@link #value} set
     * accordingly.
     */
    public ImmutablePair<K, V> setValue(final V value) {
        return new ImmutablePair<>(key, value);
    }
    
    /**
     * <p>Creates a new instance of {@link ImmutablePair} with the given {@link #key} &
     * {@link #value}.</p>
     *
     * @param <K>
     *     The class of the {@link #key} object.
     * @param <V>
     *     The class of the {@link #value} object.
     * @param key
     *     The value of the {@link #key}.
     * @param value
     *     The value of the {@link #value}.
     *
     * @return An instance of the {@link ImmutablePair} with the given values.
     */
    public static <K, V> ImmutablePair<K, V> create(final K key, final V value) {
        return new ImmutablePair<>(key, value);
    }
}
