package se.proxus.utils.pairs;

import lombok.Data;
import lombok.NonNull;

/**
 * <p>A simple implementation of an
 * <a href="https://en.wikipedia.org/wiki/Unordered_pair">Unordered Pair</a>
 * . <br/>In this case, they're labeled as a <b>key</b> and a <b>value</b>, but the naming serves
 * no greater function, they could very well just be labeled <b>a</b> & <b>b</b>. <br/>None of
 * the values are immutable, and can therefore be modified as you see fit. For a immutable
 * version of this class, use {@link ImmutablePair}, which is fully immutable.
 * </p>
 *
 * @param <K>
 *     The class type of the {@link #key} variable.
 * @param <V>
 *     The class type of the {@link #value} variable.
 */
@Data
public final class UnorderedPair<K, V> {
    
    @NonNull private K key;
    @NonNull private V value;
    
}
