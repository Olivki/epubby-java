package se.proxus.utils.collections;

import com.google.common.collect.ImmutableMap;
import lombok.experimental.UtilityClass;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@UtilityClass
public class Arrayz {
    
    public final static int INDEX_NOT_FOUND = -1;
    
    public <K, V> ImmutableMap<K, V> toImmutableMap(final K[] keys, final V[] values) {
        if (keys.length > values.length || keys.length < values.length)
            throw new NullPointerException(String.format("Key array and Value array lengths do not match! [%s != %s]",
                                                         keys.length, values.length));
        
        final Map<K, V> tempMap = IntStream.range(0, keys.length)
                .boxed()
                .collect(Collectors.toMap(i -> keys[i], i -> values[i], (a, b) -> b, LinkedHashMap::new));
    
        return ImmutableMap.<K, V>builder().putAll(tempMap).build();
    }
    
    public <V> boolean contains(final V[] array, final V query) {
        return indexOf(array, query) != INDEX_NOT_FOUND;
    }
    
    public <V> int indexOf(final V[] array, final V query) {
        return indexOf(array, query, 0);
    }
    
    public <V> int indexOf(final V[] array, final V query, int startIndex) {
        if (Objects.isNull(array))
            return INDEX_NOT_FOUND;
        
        if (startIndex < 0)
            startIndex = 0;
    
        return Objects.isNull(query)
               ? IntStream.range(startIndex, array.length)
                       .filter(i -> Objects.isNull(array[i]))
                       .findFirst()
                       .orElse(INDEX_NOT_FOUND)
               : IntStream.range(startIndex, array.length)
                       .filter(i -> Objects.equals(query, array[i]))
                       .findFirst()
                       .orElse(INDEX_NOT_FOUND);
    
    }
    
    public <V> String toString(final V[] array) {
        return Arrays.asList(array).toString();
    }
    
    public int[] toArray(final String[] stringArray) {
        return Arrays.stream(stringArray)
                .mapToInt(Integer::parseInt)
                .toArray();
    }
    
    public <V> boolean isEmpty(final V[] array) {
        return Objects.isNull(array) || array.length <= 0;
    }
}
