package se.proxus.utils.collections;

import lombok.experimental.UtilityClass;

import java.util.Arrays;
import java.util.List;

/*
 * Made by Oliver (2018-11-16, 00:07).
 */

@UtilityClass
public class Listz {
    
    @SafeVarargs
    public <V> List<V> of(final V... array) {
        return Arrays.asList(array);
    }
    
    /*public <V> List<V> of(final V... array) {
        final List<V> temp = new ArrayList<>(Arrays.asList(array));
        
        return temp;
    }*/
}