package se.proxus.utils.collections;

import com.google.common.collect.ImmutableList;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

@UtilityClass
public class Mapz {
    
    public <K, V> void removeAll(final Map<K, V> map, final Collection<V> keys) {
        map.keySet()
           .removeAll(keys);
    }
    
    public <K, V> String toString(final Map<K, V> map, @Nullable final String delimiter) {
        final String[] entries = split(map,
                                       Objects.isNull(delimiter)
                                       ? ", "
                                       : delimiter);
        final String builder = Arrays.stream(entries)
                                     .map(entry -> entry + (Objects.isNull(delimiter)
                                                            ? " "
                                                            : delimiter))
                                     .collect(Collectors.joining());
        
        return StringUtils.stripEnd(builder,
                                    Objects.isNull(delimiter)
                                    ? " "
                                    : delimiter);
    }
    
    public <K, V> String[] split(final Map<K, V> map, @NotNull final String delimiter) {
        final List<String> entryList = new LinkedList<>();
        
        map.forEach((key, value) -> {
            final StringBuilder builder = new StringBuilder();
            
            builder.append("{");
            
            if (key instanceof String) {
                builder.append("\"")
                       .append(key)
                       .append("\"");
            } else {
                builder.append(key.toString());
            }
            
            builder.append(delimiter);
            
            if (value instanceof String) {
                builder.append("\"")
                       .append(value)
                       .append("\"");
            } else {
                builder.append(value.toString());
            }
            
            builder.append("}");
            
            entryList.add(builder.toString());
        });
        
        return entryList.toArray(new String[0]);
    }
    
    public <K, V> Map<K, V> filterByKey(final Map<K, V> source, final ImmutableList<V> filter) {
        final Map<K, V> filteredMap = new LinkedHashMap<>();
        
        source.forEach((key, value) -> {
            if (!filter.contains(key)) { return; }
            
            filteredMap.put(key, value);
        });
        
        return filteredMap;
    }
    
    public <K, V> Map<K, V> reverseFilterByKey(final Map<K, V> source,
                                               final ImmutableList<V> filter) {
        final Map<K, V> filteredMap = new LinkedHashMap<>();
        
        source.forEach((key, value) -> {
            if (filter.contains(key)) { return; }
            
            filteredMap.put(key, value);
        });
        
        return filteredMap;
    }
    
    public <K, V> Map<K, V> filterByKey(final Map<K, V> source, final K[] filter) {
        final Map<K, V> filteredMap = new LinkedHashMap<>();
        
        source.forEach((key, value) -> {
            if (!Arrayz.contains(filter, key)) { return; }
            
            filteredMap.put(key, value);
        });
        
        return filteredMap;
    }
    
    public <K, V> Map<K, V> reverseFilterByKey(final Map<K, V> source, final K[] filter) {
        final Map<K, V> filteredMap = new LinkedHashMap<>();
        
        source.forEach((key, value) -> {
            if (Arrayz.contains(filter, key)) { return; }
            
            filteredMap.put(key, value);
        });
        
        return filteredMap;
    }
    
    public <K, V> Map<K, V> filterByValue(final Map<K, V> source, final ImmutableList<V> filter) {
        final Map<K, V> filteredMap = new LinkedHashMap<>();
        
        source.forEach((key, value) -> {
            if (!filter.contains(value)) { return; }
            
            filteredMap.put(key, value);
        });
        
        return filteredMap;
    }
    
    public <K, V> Map<K, V> reverseFilterByValue(final Map<K, V> source,
                                                 final ImmutableList<V> filter) {
        final Map<K, V> filteredMap = new LinkedHashMap<>();
        
        source.forEach((key, value) -> {
            if (filter.contains(value)) { return; }
            
            filteredMap.put(key, value);
        });
        
        return filteredMap;
    }
    
    public <K, V> Map<K, V> filterByValue(final Map<K, V> source, final V[] filter) {
        final Map<K, V> filteredMap = new LinkedHashMap<>();
        
        source.forEach((key, value) -> {
            if (!Arrayz.contains(filter, value)) { return; }
            
            filteredMap.put(key, value);
        });
        
        return filteredMap;
    }
    
    public <K, V> Map<K, V> reverseFilterByValue(final Map<K, V> source, final V[] filter) {
        final Map<K, V> filteredMap = new LinkedHashMap<>();
        
        source.forEach((key, value) -> {
            if (Arrayz.contains(filter, value)) { return; }
            
            filteredMap.put(key, value);
        });
        
        return filteredMap;
    }
    
}
