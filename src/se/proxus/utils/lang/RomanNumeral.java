package se.proxus.utils.lang;

import lombok.experimental.UtilityClass;

import java.util.TreeMap;

// https://stackoverflow.com/questions/12967896/converting-integers-to-roman-numerals-java
@UtilityClass
public class RomanNumeral {

    private final static TreeMap<Long, String> ROMAN_NUMERALS = new TreeMap<>();

    static {
        ROMAN_NUMERALS.put(1000L, "M");
        ROMAN_NUMERALS.put(900L, "CM");
        ROMAN_NUMERALS.put(500L, "D");
        ROMAN_NUMERALS.put(400L, "CD");
        ROMAN_NUMERALS.put(100L, "C");
        ROMAN_NUMERALS.put(90L, "XC");
        ROMAN_NUMERALS.put(50L, "L");
        ROMAN_NUMERALS.put(40L, "XL");
        ROMAN_NUMERALS.put(10L, "X");
        ROMAN_NUMERALS.put(9L, "IX");
        ROMAN_NUMERALS.put(5L, "V");
        ROMAN_NUMERALS.put(4L, "IV");
        ROMAN_NUMERALS.put(1L, "I");

    }
    
    public static String toRomanNumeral(final int number) {
        return toRomanNumeral(number);
    }

    public static String toRomanNumeral(final long number) {
        final long l = ROMAN_NUMERALS.floorKey(number);

        if (number == l)
            return ROMAN_NUMERALS.get(number);

        return ROMAN_NUMERALS.get(l) + toRomanNumeral(number - l);
    }

}
