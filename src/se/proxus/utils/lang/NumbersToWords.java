package se.proxus.utils.lang;

import lombok.experimental.UtilityClass;

import java.text.DecimalFormat;

// http://www.rgagnon.com/javadetails/java-0426.html
@UtilityClass
public final class NumbersToWords {

    private final static String[] tensNames = {
            "",
            " ten",
            " twenty",
            " thirty",
            " forty",
            " fifty",
            " sixty",
            " seventy",
            " eighty",
            " ninety"
    };

    private final static String[] numNames = {
            "",
            " one",
            " two",
            " three",
            " four",
            " five",
            " six",
            " seven",
            " eight",
            " nine",
            " ten",
            " eleven",
            " twelve",
            " thirteen",
            " fourteen",
            " fifteen",
            " sixteen",
            " seventeen",
            " eighteen",
            " nineteen"
    };

    private static String convertLessThanOneThousand(int number) {
        String soFar;

        if (number % 100 < 20) {
            soFar = numNames[number % 100];
            number /= 100;
        } else {
            soFar = numNames[number % 10];
            number /= 10;

            soFar = tensNames[number % 10] + soFar;
            number /= 10;
        }
        
        if (number == 0) { return soFar; }
        
        return numNames[number] + " hundred" + soFar;
    }


    public static String toWord(final long number) {
        // 0 to 999 999 999 999
        if (number == 0) { return "zero"; }

        String stringNumber;

        // pad with "0"
        final String mask = "000000000000";
        final DecimalFormat df = new DecimalFormat(mask);
        stringNumber = df.format(number);

        // XXXnnnnnnnnn
        final int billions = Integer.parseInt(stringNumber.substring(0, 3));
        // nnnXXXnnnnnn
        final int millions = Integer.parseInt(stringNumber.substring(3, 6));
        // nnnnnnXXXnnn
        final int hundredThousands = Integer.parseInt(stringNumber.substring(6, 9));
        // nnnnnnnnnXXX
        final int thousands = Integer.parseInt(stringNumber.substring(9, 12));

        final String tradBillions;
        
        switch (billions) {
            case 0: {
                tradBillions = "";
                break;
            }
            
            case 1: {
                tradBillions = convertLessThanOneThousand(billions) + " billion ";
                break;
            }
            
            default: {
                tradBillions = convertLessThanOneThousand(billions) + " billion ";
            }
        }
        
        String result = tradBillions;

        final String tradMillions;
        
        switch (millions) {
            case 0: {
                tradMillions = "";
                break;
            }
            
            case 1: {
                tradMillions = convertLessThanOneThousand(millions) + " million ";
                break;
            }
            
            default: {
                tradMillions = convertLessThanOneThousand(millions) + " million ";
            }
        }
        
        result = result + tradMillions;

        final String tradHundredThousands;
        
        switch (hundredThousands) {
            case 0: {
                tradHundredThousands = "";
                break;
            }
            
            case 1: {
                tradHundredThousands = "one thousand ";
                break;
            }
            
            default: {
                tradHundredThousands = convertLessThanOneThousand(hundredThousands) + " thousand ";
            }
        }
        
        result = result + tradHundredThousands;

        final String tradThousand;
        tradThousand = convertLessThanOneThousand(thousands);
        result = result + tradThousand;

        // remove extra spaces!
        return result
               .replaceAll("^\\s+", "")
               .replaceAll("\\b\\s{2,}\\b", " ");
    }
}
