package se.proxus.utils.lang;

import lombok.experimental.UtilityClass;
import org.apache.commons.math3.util.FastMath;

@UtilityClass
public class NumberUtilities {
    
    public long round(final Number number) {
        if (number instanceof Byte || number instanceof Short || number instanceof Integer ||
            number instanceof Long) {
            return number.longValue();
        } else if (number instanceof Float || number instanceof Double) {
            return FastMath.round(number.doubleValue());
        }
        
        throw new NullPointerException(number.getClass()
                                             .getName() + " does not correspond with " +
                                       "any of the known number types, aborting rounding.");
    }
    
    public boolean isMaxValue(final Number number) {
        if (number instanceof Byte) {
            return number.byteValue() == Byte.MAX_VALUE;
        } else if (number instanceof Short) {
            return number.shortValue() == Short.MAX_VALUE;
        } else if (number instanceof Integer) {
            return number.intValue() == Integer.MAX_VALUE;
        } else if (number instanceof Long) {
            return number.longValue() == Long.MAX_VALUE;
        } else if (number instanceof Float) {
            return number.floatValue() == Float.MAX_VALUE;
        } else if (number instanceof Double) {
            return number.doubleValue() == Double.MAX_VALUE;
        }
        
        throw new NullPointerException(number.getClass()
                                             .getName() + " does not correspond with " +
                                       "any of the known number types.");
    }
    
    public boolean isMinValue(final Number number) {
        if (number instanceof Byte) {
            return number.byteValue() == Byte.MIN_VALUE;
        } else if (number instanceof Short) {
            return number.shortValue() == Short.MIN_VALUE;
        } else if (number instanceof Integer) {
            return number.intValue() == Integer.MIN_VALUE;
        } else if (number instanceof Long) {
            return number.longValue() == Long.MIN_VALUE;
        } else if (number instanceof Float) {
            return number.floatValue() == Float.MIN_VALUE;
        } else if (number instanceof Double) {
            return number.doubleValue() == Double.MIN_VALUE;
        }
        
        throw new NullPointerException(number.getClass()
                                             .getName() + " does not correspond with " +
                                       "any of the known number types.");
    }
}
