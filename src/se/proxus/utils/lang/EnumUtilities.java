package se.proxus.utils.lang;

import lombok.experimental.UtilityClass;

import java.util.Arrays;

/*
 * Made by Oliver (2018-11-17, 22:07).
 */

@UtilityClass
public class EnumUtilities {
    
    public String[] getNames(final Class<?> enumClass) {
        return Arrays.stream(enumClass.getEnumConstants())
                     .map(a -> ((Enum) a).name())
                     .toArray(String[]::new);
    }
    
}