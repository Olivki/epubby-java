/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
package se.proxus.utils.convert.converters.collections;

import se.proxus.utils.convert.*;
import se.proxus.utils.convert.converters.GenericToStringConverter;
import se.proxus.utils.convert.exceptions.ConversionException;

import java.lang.reflect.Array;
import java.lang.reflect.Modifier;
import java.util.*;

/** Collection <code>Converter</code> classes. */
public class CollectionConverters implements ConverterLoader {

    @Override
    public void loadConverters() {
        Converters.loadContainedConverters(CollectionConverters.class);
        Converters.registerConverter(new GenericToStringConverter<>(Collection.class));
        Converters.registerConverter(new GenericSingletonToList<>(Map.class));
        Converters.registerConverter(new GenericSingletonToSet<>(Map.class));
    }

    private static class ArrayClassToArrayList<S, T> extends AbstractConverter<S, T> {
        public ArrayClassToArrayList(final Class<S> sourceClass, final Class<T> targetClass) {
            super(sourceClass, targetClass);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return sourceClass == this.getSourceClass() && targetClass == this.getTargetClass();
        }

        @Override
        public T convert(final S obj) throws ConversionException {
            final List<Object> list = new ArrayList<>();
            final int len = Array.getLength(obj);
            for (int i = 0; i < len; i++) {
                list.add(Array.get(obj, i));
            }
            return ConversionUtil.cast(list);
        }
    }

    private static class ArrayClassToHashSet<S, T> extends AbstractConverter<S, T> {
        public ArrayClassToHashSet(final Class<S> sourceClass, final Class<T> targetClass) {
            super(sourceClass, targetClass);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return sourceClass == this.getSourceClass() && targetClass == this.getTargetClass();
        }

        @Override
        public T convert(final S obj) throws ConversionException {
            final Set<Object> set = new HashSet<>();
            final int len = Array.getLength(obj);
            for (int i = 0; i < len; i++) {
                set.add(Array.get(obj, i));
            }
            return ConversionUtil.cast(set);
        }
    }

    @SuppressWarnings("unchecked")
    private static class ArrayClassToList<S, T> extends AbstractConverter<S, T> {
        public ArrayClassToList(final Class<S> sourceClass, final Class<T> targetClass) {
            super(sourceClass, targetClass);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return sourceClass == this.getSourceClass() && targetClass == this.getTargetClass();
        }

        @Override
        public T convert(final S obj) throws ConversionException {
            List<Object> list = null;
            try {
                list = (List<Object>) this.getTargetClass().newInstance();
            } catch (final Exception e) {
                throw new ConversionException(e);
            }
            final int len = Array.getLength(obj);
            for (int i = 0; i < len; i++) {
                list.add(Array.get(obj, i));
            }
            return ConversionUtil.cast(list);
        }
    }
    
    @SuppressWarnings("unchecked")
    private static class ArrayClassToSet<S, T> extends AbstractConverter<S, T> {
        public ArrayClassToSet(final Class<S> sourceClass, final Class<T> targetClass) {
            super(sourceClass, targetClass);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return sourceClass == this.getSourceClass() && targetClass == this.getTargetClass();
        }

        @Override
        public T convert(final S obj) throws ConversionException {
            Set<Object> set = new HashSet<>();
            try {
                set = (Set<Object>) this.getTargetClass().newInstance();
            } catch (final Exception e) {
                throw new ConversionException(e);
            }
            final int len = Array.getLength(obj);
            for (int i = 0; i < len; i++) {
                set.add(Array.get(obj, i));
            }
            return ConversionUtil.cast(set);
        }
    }
    
    @SuppressWarnings("unchecked")
    public static class ArrayCreator implements ConverterCreator, ConverterLoader {
        @Override
        public <S, T> Converter<S, T> createConverter(final Class<S> sourceClass, final Class<T> targetClass) {
            if (!targetClass.isArray()) {
               return null;
            }
            if (!Collection.class.isAssignableFrom(sourceClass)) {
               return null;
            }
            if (targetClass.getComponentType() == Boolean.TYPE) {
                return ConversionUtil.cast(new CollectionToBooleanArray(sourceClass, targetClass));
            }
            if (targetClass.getComponentType() == Byte.TYPE) {
                return ConversionUtil.cast(new CollectionToByteArray(sourceClass, targetClass));
            }
            if (targetClass.getComponentType() == Character.TYPE) {
                return ConversionUtil.cast(new CollectionToCharArray(sourceClass, targetClass));
            }
            if (targetClass.getComponentType() == Double.TYPE) {
                return ConversionUtil.cast(new CollectionToDoubleArray(sourceClass, targetClass));
            }
            if (targetClass.getComponentType() == Float.TYPE) {
                return ConversionUtil.cast(new CollectionToFloatArray(sourceClass, targetClass));
            }
            if (targetClass.getComponentType() == Integer.TYPE) {
                return ConversionUtil.cast(new CollectionToIntArray(sourceClass, targetClass));
            }
            if (targetClass.getComponentType() == Long.TYPE) {
                return ConversionUtil.cast(new CollectionToLongArray(sourceClass, targetClass));
            }
            if (targetClass.getComponentType() == Short.TYPE) {
                return ConversionUtil.cast(new CollectionToShortArray(sourceClass, targetClass));
            }
            return ConversionUtil.cast(new CollectionToObjectArray(sourceClass, targetClass));
        }

        @Override
        public void loadConverters() {
            Converters.registerCreator(this);
        }
    }

    /**
     * An object that converts an array to a <code>List</code>.
     */
    public static class ArrayToList<T> extends AbstractConverter<T[], List<T>> {
        public ArrayToList() {
            super(Object[].class, List.class);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            if (!sourceClass.isArray()) {
                return false;
            }
            if (!List.class.isAssignableFrom(targetClass)) {
                return false;
            }
            return Object[].class.isAssignableFrom(sourceClass);
        }

        /**
         * Returns a <code>List</code> that contains the elements in
         * <code>obj</code>.
         */
        @Override
        public List<T> convert(final T[] obj) throws ConversionException {
            return Arrays.asList(obj);
        }
    }
    
    @SuppressWarnings("unchecked")
    private static class CollectionToBooleanArray<T> extends AbstractConverter<Collection, T> {
        public CollectionToBooleanArray(final Class<Collection> sourceClass, final Class<T> targetClass) {
            super(sourceClass, targetClass);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return sourceClass == this.getSourceClass() && targetClass == this.getTargetClass();
        }

        @Override
        public T convert(final Collection obj) throws ConversionException {
            final boolean[] array = new boolean[obj.size()];
            int index = 0;
            for (final Boolean anObj : (Iterable<Boolean>) obj) {
                array[index] = anObj;
                index++;
            }
            return ConversionUtil.cast(array);
        }
    }
    
    @SuppressWarnings("unchecked")
    private static class CollectionToByteArray<T> extends AbstractConverter<Collection, T> {
        public CollectionToByteArray(final Class<Collection> sourceClass, final Class<T> targetClass) {
            super(sourceClass, targetClass);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return sourceClass == this.getSourceClass() && targetClass == this.getTargetClass();
        }

        @Override
        public T convert(final Collection obj) throws ConversionException {
            final byte[] array = new byte[obj.size()];
            int index = 0;
            for (final Byte anObj : (Iterable<Byte>) obj) {
                array[index] = anObj;
                index++;
            }
            return ConversionUtil.cast(array);
        }
    }
    
    @SuppressWarnings("unchecked")
    private static class CollectionToCharArray<T> extends AbstractConverter<Collection, T> {
        public CollectionToCharArray(final Class<Collection> sourceClass, final Class<T> targetClass) {
            super(sourceClass, targetClass);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return sourceClass == this.getSourceClass() && targetClass == this.getTargetClass();
        }

        @Override
        public T convert(final Collection obj) throws ConversionException {
            final char[] array = new char[obj.size()];
            int index = 0;
            for (final Character anObj : (Iterable<Character>) obj) {
                array[index] = anObj;
                index++;
            }
            return ConversionUtil.cast(array);
        }
    }
    
    @SuppressWarnings("unchecked")
    private static class CollectionToDoubleArray<T> extends AbstractConverter<Collection, T> {
        public CollectionToDoubleArray(final Class<Collection> sourceClass, final Class<T> targetClass) {
            super(sourceClass, targetClass);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return sourceClass == this.getSourceClass() && targetClass == this.getTargetClass();
        }

        @Override
        public T convert(final Collection obj) throws ConversionException {
            final double[] array = new double[obj.size()];
            int index = 0;
            for (final Double anObj : (Iterable<Double>) obj) {
                array[index] = anObj;
                index++;
            }
            return ConversionUtil.cast(array);
        }
    }
    
    @SuppressWarnings("unchecked")
    private static class CollectionToFloatArray<T> extends AbstractConverter<Collection, T> {
        public CollectionToFloatArray(final Class<Collection> sourceClass, final Class<T> targetClass) {
            super(sourceClass, targetClass);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return sourceClass == this.getSourceClass() && targetClass == this.getTargetClass();
        }

        @Override
        public T convert(final Collection obj) throws ConversionException {
            final float[] array = new float[obj.size()];
            int index = 0;
            for (final Float anObj : (Iterable<Float>) obj) {
                array[index] = anObj;
                index++;
            }
            return ConversionUtil.cast(array);
        }
    }
    
    @SuppressWarnings("unchecked")
    private static class CollectionToIntArray<T> extends AbstractConverter<Collection, T> {
        public CollectionToIntArray(final Class<Collection> sourceClass, final Class<T> targetClass) {
            super(sourceClass, targetClass);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return sourceClass == this.getSourceClass() && targetClass == this.getTargetClass();
        }

        @Override
        public T convert(final Collection obj) throws ConversionException {
            final int[] array = new int[obj.size()];
            int index = 0;
            for (final Integer anObj : (Iterable<Integer>) obj) {
                array[index] = anObj;
                index++;
            }
            return ConversionUtil.cast(array);
        }
    }
    
    @SuppressWarnings("unchecked")
    private static class CollectionToLongArray<T> extends AbstractConverter<Collection, T> {
        public CollectionToLongArray(final Class<Collection> sourceClass, final Class<T> targetClass) {
            super(sourceClass, targetClass);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return sourceClass == this.getSourceClass() && targetClass == this.getTargetClass();
        }

        @Override
        public T convert(final Collection obj) throws ConversionException {
            final long[] array = new long[obj.size()];
            int index = 0;
            for (final Long anObj : (Iterable<Long>) obj) {
                array[index] = anObj;
                index++;
            }
            return ConversionUtil.cast(array);
        }
    }

    private static class CollectionToObjectArray<T> extends AbstractConverter<Collection, T> {
        public CollectionToObjectArray(final Class<Collection> sourceClass, final Class<T> targetClass) {
            super(sourceClass, targetClass);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return sourceClass == this.getSourceClass() && targetClass == this.getTargetClass();
        }

        @Override
        public T convert(final Collection obj) throws ConversionException {
            return ConversionUtil.cast(obj.toArray());
        }
    }
    
    @SuppressWarnings("unchecked")
    private static class CollectionToShortArray<T> extends AbstractConverter<Collection, T> {
        public CollectionToShortArray(final Class<Collection> sourceClass, final Class<T> targetClass) {
            super(sourceClass, targetClass);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return sourceClass == this.getSourceClass() && targetClass == this.getTargetClass();
        }

        @Override
        public T convert(final Collection obj) throws ConversionException {
            final short[] array = new short[obj.size()];
            int index = 0;
            for (final Short anObj : (Iterable<Short>) obj) {
                array[index] = anObj;
                index++;
            }
            return ConversionUtil.cast(array);
        }
    }
    
    @SuppressWarnings("unchecked")
    public static class ListCreator implements ConverterCreator, ConverterLoader {
        @Override
        public <S, T> Converter<S, T> createConverter(final Class<S> sourceClass, final Class<T> targetClass) {
            if (!sourceClass.isArray()) {
               return null;
            }
            if (!List.class.isAssignableFrom(targetClass)) {
               return null;
            }
            if (Objects.isNull(sourceClass.getComponentType())) {
                return null;
            }
            if ((targetClass.getModifiers() & Modifier.ABSTRACT) == 0) {
                return ConversionUtil.cast(new ArrayClassToList<>(sourceClass, targetClass));
            } else {
                return ConversionUtil.cast(new ArrayClassToArrayList<>(sourceClass, targetClass));
            }
        }

        @Override
        public void loadConverters() {
            Converters.registerCreator(this);
        }
    }

    public static class SetCreator implements ConverterCreator, ConverterLoader {
        @Override
        public <S, T> Converter<S, T> createConverter(final Class<S> sourceClass, final Class<T> targetClass) {
            if (!sourceClass.isArray()) {
               return null;
            }
            if (!Set.class.isAssignableFrom(targetClass)) {
               return null;
            }
            if (Objects.isNull(sourceClass.getComponentType())) {
                return null;
            }
            if ((targetClass.getModifiers() & Modifier.ABSTRACT) == 0) {
                return ConversionUtil.cast(new ArrayClassToSet<>(sourceClass, targetClass));
            } else {
                return ConversionUtil.cast(new ArrayClassToHashSet<>(sourceClass, targetClass));
            }
        }

        @Override
        public void loadConverters() {
            Converters.registerCreator(this);
        }
    }
}
