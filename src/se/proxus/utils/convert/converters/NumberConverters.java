/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
package se.proxus.utils.convert.converters;

import se.proxus.utils.convert.*;
import se.proxus.utils.convert.converters.collections.GenericSingletonToList;
import se.proxus.utils.convert.converters.collections.GenericSingletonToSet;
import se.proxus.utils.convert.exceptions.ConversionException;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import java.util.TimeZone;

/** Number Converter classes. */
public class NumberConverters implements ConverterLoader {

    protected static final Class<?>[] classArray = {BigDecimal.class, BigInteger.class, Byte.class, Double.class, Integer.class, Float.class, Long.class, Short.class};

    protected static Number fromString(final String str, final NumberFormat nf) throws ConversionException {
        try {
            return nf.parse(str);
        } catch (final ParseException e) {
            throw new ConversionException(e);
        }
    }

    protected static <S, T> void registerConverter(final Converter<S, T> converter) {
        if (converter.getSourceClass() != converter.getTargetClass()) {
            Converters.registerConverter(converter);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void loadConverters() {
        Converters.loadContainedConverters(NumberConverters.class);
        for (final Class<?> sourceClass : classArray) {
            registerConverter(new GenericNumberToBigDecimal(sourceClass));
            registerConverter(new GenericNumberToBigInteger(sourceClass));
            registerConverter(new GenericNumberToByte(sourceClass));
            registerConverter(new GenericNumberToDouble(sourceClass));
            registerConverter(new GenericNumberToInteger(sourceClass));
            registerConverter(new GenericNumberToFloat(sourceClass));
            registerConverter(new GenericNumberToLong(sourceClass));
            registerConverter(new GenericNumberToShort(sourceClass));
            registerConverter(new GenericSingletonToList(sourceClass));
            registerConverter(new GenericSingletonToSet(sourceClass));
        }
    }

    /**
     * An abstract <code>Number</code> to <code>String</code> converter class
     * that implements some of the <code>LocalizedConverter</code> methods. 
     */
    public static abstract class AbstractNumberToStringConverter<N extends Number> extends
                                                                                   AbstractLocalizedConverter<N, String> {
        public AbstractNumberToStringConverter(final Class<N> sourceClass) {
            super(sourceClass, String.class);
        }

        @Override
        public String convert(final N obj) throws ConversionException {
            return obj.toString();
        }

        @Override
        public String convert(final N obj, final Locale locale, final TimeZone timeZone, final String formatString) throws ConversionException {
            if (formatString == null) {
                return format(obj, NumberFormat.getNumberInstance(locale));
            } else {
                return format(obj, new DecimalFormat(formatString));
            }
        }

        protected abstract String format(N obj, NumberFormat nf) throws ConversionException;
    }

    /**
     * An abstract <code>String</code> to <code>Number</code> converter class
     * that implements some of the <code>LocalizedConverter</code> methods. 
     */
    public static abstract class AbstractStringToNumberConverter<N extends Number> extends AbstractLocalizedConverter<String, N> {
        public AbstractStringToNumberConverter(final Class<N> targetClass) {
            super(String.class, targetClass);
        }

        protected abstract N convert(Number number) throws ConversionException;

        @Override
        public N convert(final String obj, final Locale locale, final TimeZone timeZone, final String formatString) throws ConversionException {
            if (formatString == null) {
                return convert(fromString(obj, NumberFormat.getNumberInstance(locale)));
            } else {
                return convert(fromString(obj, new DecimalFormat(formatString)));
            }
        }
    }

    /**
     * An object that converts a <code>BigDecimal</code> to a
     * <code>String</code>.
     */
    public static class BigDecimalToString extends AbstractNumberToStringConverter<BigDecimal> {
        public BigDecimalToString() {
            super(BigDecimal.class);
        }

        @Override
        protected String format(final BigDecimal obj, final NumberFormat nf) throws ConversionException {
            return nf.format(obj.doubleValue());
        }
    }

    /**
     * An object that converts a <code>BigInteger</code> to a
     * <code>String</code>.
     */
    public static class BigIntegerToString extends AbstractNumberToStringConverter<BigInteger> {
        public BigIntegerToString() {
            super(BigInteger.class);
        }

        @Override
        protected String format(final BigInteger obj, final NumberFormat nf) throws ConversionException {
            return nf.format(obj.doubleValue());
        }
    }

    /**
     * An object that converts a <code>Byte</code> to a
     * <code>String</code>.
     */
    public static class ByteToString extends AbstractNumberToStringConverter<Byte> {
        public ByteToString() {
            super(Byte.class);
        }

        @Override
        protected String format(final Byte obj, final NumberFormat nf) throws ConversionException {
            return nf.format(obj.floatValue());
        }
    }

    /**
     * An object that converts a <code>Double</code> to a
     * <code>String</code>.
     */
    public static class DoubleToString extends AbstractNumberToStringConverter<Double> {
        public DoubleToString() {
            super(Double.class);
        }

        @Override
        protected String format(final Double obj, final NumberFormat nf) throws ConversionException {
            return nf.format(obj.doubleValue());
        }
    }

    /**
     * An object that converts a <code>Float</code> to a
     * <code>String</code>.
     */
    public static class FloatToString extends AbstractNumberToStringConverter<Float> {
        public FloatToString() {
            super(Float.class);
        }

        @Override
        protected String format(final Float obj, final NumberFormat nf) throws ConversionException {
            return nf.format(obj.floatValue());
        }
    }

    /**
     * An object that converts a <code>Number</code> to a
     * <code>BigDecimal</code>.
     */
    public static class GenericNumberToBigDecimal<N extends Number> extends
                                                                    AbstractConverter<N, BigDecimal> {
        public GenericNumberToBigDecimal(final Class<N> sourceClass) {
            super(sourceClass, BigDecimal.class);
        }

        @Override
        public BigDecimal convert(final N obj) throws ConversionException {
            return new BigDecimal(obj.doubleValue());
        }
    }

    /**
     * An object that converts a <code>Number</code> to a
     * <code>BigInteger</code>.
     */
    public static class GenericNumberToBigInteger<N extends Number> extends AbstractConverter<N, BigInteger> {
        public GenericNumberToBigInteger(final Class<N> sourceClass) {
            super(sourceClass, BigInteger.class);
        }

        @Override
        public BigInteger convert(final N obj) throws ConversionException {
            return BigInteger.valueOf(obj.longValue());
        }
    }

    /**
     * An object that converts a <code>Number</code> to a
     * <code>Byte</code>.
     */
    public static class GenericNumberToByte<N extends Number> extends AbstractConverter<N, Byte> {
        public GenericNumberToByte(final Class<N> sourceClass) {
            super(sourceClass, Byte.class);
        }

        @Override
        public Byte convert(final N obj) throws ConversionException {
            return Byte.valueOf(obj.byteValue());
        }
    }

    /**
     * An object that converts a <code>Number</code> to a
     * <code>Double</code>.
     */
    public static class GenericNumberToDouble<N extends Number> extends AbstractConverter<N, Double> {
        public GenericNumberToDouble(final Class<N> sourceClass) {
            super(sourceClass, Double.class);
        }

        @Override
        public Double convert(final N obj) throws ConversionException {
            return obj.doubleValue();
        }
    }

    /**
     * An object that converts a <code>Number</code> to a
     * <code>Float</code>.
     */
    public static class GenericNumberToFloat<N extends Number> extends AbstractConverter<N, Float> {
        public GenericNumberToFloat(final Class<N> sourceClass) {
            super(sourceClass, Float.class);
        }

        @Override
        public Float convert(final N obj) throws ConversionException {
            return obj.floatValue();
        }
    }

    /**
     * An object that converts a <code>Number</code> to an
     * <code>Integer</code>.
     */
    public static class GenericNumberToInteger<N extends Number> extends AbstractConverter<N, Integer> {
        public GenericNumberToInteger(final Class<N> sourceClass) {
            super(sourceClass, Integer.class);
        }

        @Override
        public Integer convert(final N obj) throws ConversionException {
            return obj.intValue();
        }
    }

    /**
     * An object that converts a <code>Number</code> to a
     * <code>Long</code>.
     */
    public static class GenericNumberToLong<N extends Number> extends AbstractConverter<N, Long> {
        public GenericNumberToLong(final Class<N> sourceClass) {
            super(sourceClass, Long.class);
        }

        @Override
        public Long convert(final N obj) throws ConversionException {
            return obj.longValue();
        }
    }

    /**
     * An object that converts a <code>Number</code> to a
     * <code>Short</code>.
     */
    public static class GenericNumberToShort<N extends Number> extends AbstractConverter<N, Short> {
        public GenericNumberToShort(final Class<N> sourceClass) {
            super(sourceClass, Short.class);
        }

        @Override
        public Short convert(final N obj) throws ConversionException {
            return obj.shortValue();
        }
    }

    /**
     * An object that converts an <code>Integer</code> to a
     * <code>String</code>.
     */
    public static class IntegerToString extends AbstractNumberToStringConverter<Integer> {
        public IntegerToString() {
            super(Integer.class);
        }

        @Override
        protected String format(final Integer obj, final NumberFormat nf) throws ConversionException {
            return nf.format(obj.intValue());
        }
    }

    /**
     * An object that converts a <code>Long</code> to a
     * <code>BigDecimal</code>.
     */
    public static class LongToBigDecimal extends AbstractConverter<Long, BigDecimal> {
        public LongToBigDecimal() {
            super(Long.class, BigDecimal.class);
        }

        @Override
        public BigDecimal convert(final Long obj) throws ConversionException {
            return BigDecimal.valueOf(obj.longValue());
        }
    }

    /**
     * An object that converts a <code>Long</code> to a
     * <code>String</code>.
     */
    public static class LongToString extends AbstractNumberToStringConverter<Long> {
        public LongToString() {
            super(Long.class);
        }

        @Override
        protected String format(final Long obj, final NumberFormat nf) throws ConversionException {
            return nf.format(obj.longValue());
        }
    }

    /**
     * An object that converts a <code>Short</code> to a
     * <code>String</code>.
     */
    public static class ShortToString extends AbstractNumberToStringConverter<Short> {
        public ShortToString() {
            super(Short.class);
        }

        @Override
        protected String format(final Short obj, final NumberFormat nf) throws ConversionException {
            return nf.format(obj.floatValue());
        }
    }

    /**
     * An object that converts a <code>String</code> to a
     * <code>BigDecimal</code>.
     */
    public static class StringToBigDecimal extends AbstractStringToNumberConverter<BigDecimal> {
        public StringToBigDecimal() {
            super(BigDecimal.class);
        }

        @Override
        protected BigDecimal convert(final Number number) throws ConversionException {
            return BigDecimal.valueOf(number.doubleValue());
        }

        @Override
        public BigDecimal convert(final String obj) throws ConversionException {
            return BigDecimal.valueOf(Double.valueOf(obj));
        }
    }

    /**
     * An object that converts a <code>String</code> to a
     * <code>BigInteger</code>.
     */
    public static class StringToBigInteger extends AbstractStringToNumberConverter<BigInteger> {
        public StringToBigInteger() {
            super(BigInteger.class);
        }

        @Override
        protected BigInteger convert(final Number number) throws ConversionException {
            return BigInteger.valueOf(number.longValue());
        }

        @Override
        public BigInteger convert(final String obj) throws ConversionException {
            return new BigInteger(obj);
        }
    }

    /**
     * An object that converts a <code>String</code> to a
     * <code>Byte</code>.
     */
    public static class StringToByte extends AbstractConverter<String, Byte> {
        public StringToByte() {
            super(String.class, Byte.class);
        }

        @Override
        public Byte convert(final String obj) throws ConversionException {
            return Byte.valueOf(obj);
        }
    }

    /**
     * An object that converts a <code>String</code> to a
     * <code>Double</code>.
     */
    public static class StringToDouble extends AbstractStringToNumberConverter<Double> {
        public StringToDouble() {
            super(Double.class);
        }

        @Override
        protected Double convert(final Number number) throws ConversionException {
            return number.doubleValue();
        }

        @Override
        public Double convert(final String obj) throws ConversionException {
            return Double.valueOf(obj);
        }
    }

    /**
     * An object that converts a <code>String</code> to a
     * <code>Float</code>.
     */
    public static class StringToFloat extends AbstractStringToNumberConverter<Float> {
        public StringToFloat() {
            super(Float.class);
        }

        @Override
        protected Float convert(final Number number) throws ConversionException {
            return number.floatValue();
        }

        @Override
        public Float convert(final String obj) throws ConversionException {
            return Float.valueOf(obj);
        }
    }

    /**
     * An object that converts a <code>String</code> to an
     * <code>Integer</code>.
     */
    public static class StringToInteger extends AbstractStringToNumberConverter<Integer> {
        public StringToInteger() {
            super(Integer.class);
        }

        @Override
        protected Integer convert(final Number number) throws ConversionException {
            return number.intValue();
        }

        @Override
        public Integer convert(final String obj) throws ConversionException {
            return Integer.valueOf(obj);
        }
    }

    /**
     * An object that converts a <code>String</code> to a
     * <code>Long</code>.
     */
    public static class StringToLong extends AbstractStringToNumberConverter<Long> {
        public StringToLong() {
            super(Long.class);
        }

        @Override
        protected Long convert(final Number number) throws ConversionException {
            return number.longValue();
        }

        @Override
        public Long convert(final String obj) throws ConversionException {
            return Long.valueOf(obj);
        }
    }

    /**
     * An object that converts a <code>String</code> to a
     * <code>Short</code>.
     */
    public static class StringToShort extends AbstractConverter<String, Short> {
        public StringToShort() {
            super(String.class, Short.class);
        }

        @Override
        public Short convert(final String obj) throws ConversionException {
            return Short.valueOf(obj);
        }
    }
}
