/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
package se.proxus.utils.convert.converters;

import se.proxus.utils.convert.*;
import se.proxus.utils.convert.converters.collections.GenericSingletonToList;
import se.proxus.utils.convert.converters.collections.GenericSingletonToSet;
import se.proxus.utils.convert.exceptions.ConversionException;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/** Date/time <code>Converter</code> classes.
 *
 * @deprecated <p>{@link Date} became outdated with the introduction of the
 * {@link java.time} package in <b>Java 8</b>.</p>
 * */
@Deprecated
public class DateTimeConverters implements ConverterLoader {

    /**
     * Calendar format string: <code>EEE MMM dd HH:mm:ss.SSS z yyyy</code>. 
     */
    public static final String CALENDAR_FORMAT = "EEE MMM dd HH:mm:ss.SSS z yyyy";
    /**
     * JDBC DATE format string: <code>yyyy-MM-dd</code>. 
     */
    public static final String JDBC_DATE_FORMAT = "yyyy-MM-dd";
    /**
     * JDBC TIME format string: <code>HH:mm:ss</code>. 
     */
    public static final String JDBC_TIME_FORMAT = "HH:mm:ss";

    /**
     * Returns an initialized DateFormat object.
     * 
     * @param tz
     * @return DateFormat object
     */
    protected static DateFormat toDateFormat(final TimeZone tz) {
        final DateFormat df = new SimpleDateFormat(JDBC_DATE_FORMAT);
        df.setTimeZone(tz);
        return df;
    }

    /**
     * Returns an initialized DateFormat object.
     * 
     * @param dateTimeFormat
     *            optional format string
     * @param tz
     * @param locale
     *            can be null if dateTimeFormat is not null
     * @return DateFormat object
     */
    protected static DateFormat toDateTimeFormat(final String dateTimeFormat, final TimeZone tz, final Locale locale) {
        DateFormat df = null;
        if (ConversionUtil.isEmpty(dateTimeFormat)) {
            df = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM, locale);
        } else {
            df = new SimpleDateFormat(dateTimeFormat);
        }
        df.setTimeZone(tz);
        return df;
    }

    /**
     * Returns an initialized DateFormat object.
     * 
     * @param tz
     * @return DateFormat object
     */
    protected static DateFormat toTimeFormat(final TimeZone tz) {
        final DateFormat df = new SimpleDateFormat(JDBC_TIME_FORMAT);
        df.setTimeZone(tz);
        return df;
    }

    @Override
    public void loadConverters() {
        Converters.loadContainedConverters(DateTimeConverters.class);
        Converters.registerConverter(new GenericDateToLong<>(Date.class));
        Converters.registerConverter(new GenericDateToLong<>(java.sql.Date.class));
        Converters.registerConverter(new GenericDateToLong<>(Time.class));
        Converters.registerConverter(new GenericDateToLong<>(Timestamp.class));
        Converters.registerConverter(new GenericSingletonToList<>(Calendar.class));
        Converters.registerConverter(new GenericSingletonToList<>(Date.class));
        Converters.registerConverter(new GenericSingletonToList<>(TimeZone.class));
        Converters.registerConverter(new GenericSingletonToList<>(java.sql.Date.class));
        Converters.registerConverter(new GenericSingletonToList<>(Time.class));
        Converters.registerConverter(new GenericSingletonToList<>(Timestamp.class));
        Converters.registerConverter(new GenericSingletonToSet<>(Calendar.class));
        Converters.registerConverter(new GenericSingletonToSet<>(Date.class));
        Converters.registerConverter(new GenericSingletonToSet<>(TimeZone.class));
        Converters.registerConverter(new GenericSingletonToSet<>(java.sql.Date.class));
        Converters.registerConverter(new GenericSingletonToSet<>(Time.class));
        Converters.registerConverter(new GenericSingletonToSet<>(Timestamp.class));
    }

    /**
     * An object that converts a <code>Calendar</code> to a <code>Date</code>.
     */
    public static class CalendarToDate extends AbstractConverter<Calendar, Date> {
        public CalendarToDate() {
            super(Calendar.class, Date.class);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return ConversionUtil.instanceOf(sourceClass, this.getSourceClass()) && Date.class.equals(targetClass);
        }

        @Override
        public Date convert(final Calendar obj) throws ConversionException {
            return obj.getTime();
        }
    }

    /**
     * An object that converts a <code>Calendar</code> to a <code>Long</code>.
     */
    public static class CalendarToLong extends AbstractConverter<Calendar, Long> {
        public CalendarToLong() {
            super(Calendar.class, Long.class);
        }

        /**
         * Returns the millisecond valorem of <code>obj</code>.
         */
        @Override
        public Long convert(final Calendar obj) throws ConversionException {
            return obj.getTimeInMillis();
        }
    }

    /**
     * An object that converts a <code>Calendar</code> to a <code>String</code>.
     */
    public static class CalendarToString extends AbstractLocalizedConverter<Calendar, String> {
        public CalendarToString() {
            super(Calendar.class, String.class);
        }

        /**
         * Converts <code>obj</code> to a <code>String</code> formatted as
         * {@link DateTimeConverters#CALENDAR_FORMAT}. The returned string is
         * referenced to the default time zone.
         */
        @Override
        public String convert(final Calendar obj) throws ConversionException {
            final DateFormat df = new SimpleDateFormat(CALENDAR_FORMAT);
            df.setCalendar(obj);
            return df.format(obj.getTime());
        }

        /**
         * Converts <code>obj</code> to a <code>String</code> using the supplied
         * locale, time zone, and format string. If <code>formatString</code> is
         * <code>null</code>, the string is formatted as
         * {@link DateTimeConverters#CALENDAR_FORMAT}.
         */
        @Override
        public String convert(final Calendar obj, final Locale locale, final TimeZone timeZone, final String formatString) throws ConversionException {
            final DateFormat df = toDateTimeFormat(formatString == null ? CALENDAR_FORMAT : formatString, timeZone, locale);
            df.setCalendar(obj);
            return df.format(obj.getTime());
        }
    }

    /**
     * An object that converts a <code>Calendar</code> to a <code>Timestamp</code>.
     */
    public static class CalendarToTimestamp extends AbstractConverter<Calendar, Timestamp> {
        public CalendarToTimestamp() {
            super(Calendar.class, Timestamp.class);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return ConversionUtil.instanceOf(sourceClass, this.getSourceClass()) && Timestamp.class.equals(targetClass);
        }

        @Override
        public Timestamp convert(final Calendar obj) throws ConversionException {
            return new Timestamp(obj.getTimeInMillis());
        }
    }


    /**
     * An object that converts a <code>Date</code> to a <code>Calendar</code>.
     */
    public static class DateToCalendar extends GenericLocalizedConverter<Date, Calendar> {
        public DateToCalendar() {
            super(Date.class, Calendar.class);
        }

        @Override
        public Calendar convert(final Date obj, final Locale locale, final TimeZone timeZone, final String formatString) throws ConversionException {
            final Calendar cal = Calendar.getInstance(timeZone, locale);
            cal.setTime(obj);
            return cal;
        }
    }

    /**
     * An object that converts a <code>java.util.Date</code> to a
     * <code>java.sql.Date</code>.
     */
    public static class DateToSqlDate extends AbstractConverter<Date, java.sql.Date> {
        public DateToSqlDate() {
            super(Date.class, java.sql.Date.class);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return (Date.class.equals(sourceClass) || Timestamp.class.equals(sourceClass)) && java.sql.Date.class.equals(targetClass);
        }

        /**
         * Returns <code>obj</code> converted to a <code>java.sql.Date</code>.
         */
        @Override
        public java.sql.Date convert(final Date obj) throws ConversionException {
            final Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(obj.getTime());
            return new java.sql.Date(cal.get(Calendar.YEAR) - 1900, cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
        }
    }

    /**
     * An object that converts a <code>java.util.Date</code> to a
     * <code>java.sql.Time</code>.
     */
    public static class DateToSqlTime extends AbstractConverter<Date, Time> {
        public DateToSqlTime() {
            super(Date.class, Time.class);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return sourceClass == this.getSourceClass() && targetClass == this.getTargetClass();
        }

        @Override
        public Time convert(final Date obj) throws ConversionException {
            final Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(obj.getTime());
            return new Time(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
        }
    }

    /**
     * An object that converts a <code>java.util.Date</code> to a
     * <code>String</code>.
     */
    public static class DateToString extends AbstractLocalizedConverter<Date, String> {
        public DateToString() {
            super(Date.class, String.class);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return Date.class.equals(sourceClass) && String.class.equals(targetClass);
        }

        /**
         * Converts <code>obj</code> to a <code>String</code> formatted as
         * {@link DateTimeConverters#CALENDAR_FORMAT}. The returned string is
         * referenced to the default time zone.
         */
        @Override
        public String convert(final Date obj) throws ConversionException {
            final DateFormat df = new SimpleDateFormat(CALENDAR_FORMAT);
            return df.format(obj);
        }

        /**
         * Converts <code>obj</code> to a <code>String</code> using the supplied
         * locale, time zone, and format string. If <code>formatString</code> is
         * <code>null</code>, the string is formatted as
         * {@link DateTimeConverters#CALENDAR_FORMAT}.
         */
        @Override
        public String convert(final Date obj, final Locale locale, final TimeZone timeZone, final String formatString) throws ConversionException {
            final DateFormat df = toDateTimeFormat(formatString == null ? CALENDAR_FORMAT : formatString, timeZone, locale);
            return df.format(obj);
        }
    }

    /**
     * An object that converts a <code>java.util.Date</code> to a
     * <code>java.sql.Timestamp</code>.
     */
    public static class DateToTimestamp extends AbstractConverter<Date, Timestamp> {
        public DateToTimestamp() {
            super(Date.class, Timestamp.class);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return (Date.class.equals(sourceClass) || Timestamp.class.equals(sourceClass)) && Timestamp.class.equals(targetClass);
        }

        /**
         * Returns <code>obj</code> converted to a <code>java.sql.Timestamp</code>.
         */
        @Override
        public Timestamp convert(final Date obj) throws ConversionException {
            return new Timestamp(obj.getTime());
        }
    }

    /**
     * An object that converts a <code>java.util.Date</code> (and its subclasses) to a
     * <code>Long</code>.
     */
    public static class GenericDateToLong<S extends Date> extends AbstractConverter<S, Long> {
        public GenericDateToLong(final Class<S> source) {
            super(source, Long.class);
        }

        /**
         * Returns the millisecond valorem of <code>obj</code>.
         */
        @Override
        public Long convert(final S obj) throws ConversionException {
            return obj.getTime();
        }
    }

    public static abstract class GenericLocalizedConverter<S, T> extends AbstractLocalizedConverter<S, T> {
        protected GenericLocalizedConverter(final Class<S> sourceClass, final Class<T> targetClass) {
            super(sourceClass, targetClass);
        }

        @Override
        public T convert(final S obj) throws ConversionException {
            return convert(obj, Locale.getDefault(), TimeZone.getDefault(), null);
        }

        @Override
        public T convert(final S obj, final Locale locale, final TimeZone timeZone) throws ConversionException {
            return convert(obj, locale, timeZone, null);
        }
    }

    /**
     * An object that converts a <code>Long</code> to a
     * <code>Calendar</code>.
     */
    public static class LongToCalendar extends AbstractLocalizedConverter<Long, Calendar> {
        public LongToCalendar() {
            super(Long.class, Calendar.class);
        }

        /**
         * Returns <code>obj</code> converted to a <code>Calendar</code>,
         * initialized with the default locale and time zone.
         */
        @Override
        public Calendar convert(final Long obj) throws ConversionException {
            final Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(obj);
            return cal;
        }

        /**
         * Returns <code>obj</code> converted to a <code>Calendar</code>,
         * initialized with the specified locale and time zone. The
         * <code>formatString</code> parameter is ignored.
         */
        @Override
        public Calendar convert(final Long obj, final Locale locale, final TimeZone timeZone, final String formatString) throws ConversionException {
            final Calendar cal = Calendar.getInstance(timeZone, locale);
            cal.setTimeInMillis(obj);
            return cal;
        }
    }

    /**
     * An object that converts a <code>Long</code> to a
     * <code>java.util.Date</code>.
     */
    public static class LongToDate extends AbstractConverter<Long, Date> {
        public LongToDate() {
            super(Long.class, Date.class);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return Long.class.equals(sourceClass) && Date.class.equals(targetClass);
        }

        /**
         * Returns <code>obj</code> converted to a <code>java.util.Date</code>.
         */
        @Override
        public Date convert(final Long obj) throws ConversionException {
            return new Date(obj);
        }
    }

    /**
     * An object that converts a <code>Long</code> to a
     * <code>java.sql.Date</code>.
     */
    public static class LongToSqlDate extends AbstractConverter<Long, java.sql.Date> {
        public LongToSqlDate() {
            super(Long.class, java.sql.Date.class);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return Long.class.equals(sourceClass) && java.sql.Date.class.equals(targetClass);
        }

        /**
         * Returns <code>obj</code> converted to a <code>java.sql.Date</code>.
         */
        @Override
        public java.sql.Date convert(final Long obj) throws ConversionException {
            return new java.sql.Date(obj);
        }
    }

    /**
     * An object that converts a <code>Long</code> to a
     * <code>java.sql.Time</code>.
     */
    public static class LongToSqlTime extends AbstractConverter<Long, Time> {
        public LongToSqlTime() {
            super(Long.class, Time.class);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return Long.class.equals(sourceClass) && Time.class.equals(targetClass);
        }

        /**
         * Returns <code>obj</code> converted to a <code>java.sql.Time</code>.
         */
        @Override
        public Time convert(final Long obj) throws ConversionException {
            return new Time(obj);
        }
    }

    /**
     * An object that converts a <code>Long</code> to a
     * <code>java.sql.Timestamp</code>.
     */
    public static class LongToTimestamp extends AbstractConverter<Long, Timestamp> {
        public LongToTimestamp() {
            super(Long.class, Timestamp.class);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return Long.class.equals(sourceClass) && Timestamp.class.equals(targetClass);
        }

        /**
         * Returns <code>obj</code> converted to a <code>java.sql.Timestamp</code>.
         */
        @Override
        public Timestamp convert(final Long obj) throws ConversionException {
            return new Timestamp(obj);
        }
    }

    /**
     * An object that converts a <code>java.sql.Date</code> to a
     * <code>java.util.Date</code>.
     */
    public static class SqlDateToDate extends AbstractConverter<java.sql.Date, Date> {
        public SqlDateToDate() {
            super(java.sql.Date.class, Date.class);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return java.sql.Date.class.equals(sourceClass) && Date.class.equals(targetClass);
        }

        /**
         * Returns <code>obj</code> converted to a <code>java.util.Date</code>.
         */
        @Override
        public Date convert(final java.sql.Date obj) throws ConversionException {
            return new Date(obj.getTime());
        }
    }

    /**
     * An object that converts a <code>java.sql.Date</code> to a
     * <code>String</code>.
     */
    public static class SqlDateToString extends AbstractLocalizedConverter<java.sql.Date, String> {
        public SqlDateToString() {
            super(java.sql.Date.class, String.class);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return java.sql.Date.class.equals(sourceClass) && String.class.equals(targetClass);
        }

        @Override
        public String convert(final java.sql.Date obj) throws ConversionException {
            return obj.toString();
        }

        /**
         * Converts <code>obj</code> to a <code>String</code> using the supplied
         * time zone. The <code>formatString</code> parameter is
         * ignored. The returned string is formatted as
         * {@link DateTimeConverters#JDBC_DATE_FORMAT}.
         */
        @Override
        public String convert(final java.sql.Date obj, final Locale locale, final TimeZone timeZone, final String formatString) throws ConversionException {
            final DateFormat df = toDateFormat(timeZone);
            return df.format(obj);
        }
    }

    /**
     * An object that converts a <code>java.sql.Date</code> to a
     * <code>java.sql.Timestamp</code>.
     */
    public static class SqlDateToTimestamp extends AbstractConverter<java.sql.Date, Timestamp> {
        public SqlDateToTimestamp() {
            super(java.sql.Date.class, Timestamp.class);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return java.sql.Date.class.equals(sourceClass) && Timestamp.class.equals(targetClass);
        }

        /**
         * Returns <code>obj</code> converted to a <code>java.sql.Timestamp</code>.
         */
        @Override
        public Timestamp convert(final java.sql.Date obj) throws ConversionException {
            return new Timestamp(obj.getTime());
        }
    }

    /**
     * An object that converts a <code>java.sql.Time</code> to a
     * <code>String</code>.
     */
    public static class SqlTimeToString extends AbstractLocalizedConverter<Time, String> {
        public SqlTimeToString() {
            super(Time.class, String.class);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return Time.class.equals(sourceClass) && String.class.equals(targetClass);
        }

        @Override
        public String convert(final Time obj) throws ConversionException {
            return obj.toString();
        }

        /**
         * Converts <code>obj</code> to a <code>String</code> using the supplied
         * time zone. The <code>formatString</code> parameter is
         * ignored. The returned string is formatted as
         * {@link DateTimeConverters#JDBC_TIME_FORMAT}.
         */
        @Override
        public String convert(final Time obj, final Locale locale, final TimeZone timeZone, final String formatString) throws ConversionException {
            final DateFormat df = toTimeFormat(timeZone);
            return df.format(obj);
        }
    }

    /**
     * An object that converts a <code>String</code> to a
     * <code>java.util.Calendar</code>.
     */
    public static class StringToCalendar extends AbstractLocalizedConverter<String, Calendar> {
        public StringToCalendar() {
            super(String.class, Calendar.class);
        }

        /**
         * Converts <code>obj</code> to a <code>java.util.Calendar</code> initialized to
         * the default locale and time zone. The string must be formatted as
         * {@link DateTimeConverters#CALENDAR_FORMAT}.
         */
        @Override
        public Calendar convert(final String obj) throws ConversionException {
            try {
                final DateFormat df = new SimpleDateFormat(CALENDAR_FORMAT);
                final Calendar cal = Calendar.getInstance();
                cal.setTime(df.parse(obj));
                return cal;
            } catch (final ParseException e) {
                throw new ConversionException(e);
            }
        }

        /**
         * Converts <code>obj</code> to a <code>java.util.Calendar</code> initialized to
         * the supplied locale and time zone. If <code>formatString</code> is
         * <code>null</code>, the string is formatted as
         * {@link DateTimeConverters#CALENDAR_FORMAT}.
         */
        @Override
        public Calendar convert(final String obj, final Locale locale, final TimeZone timeZone, final String formatString) throws ConversionException {
            final DateFormat df = toDateTimeFormat(formatString == null ? CALENDAR_FORMAT : formatString, timeZone, locale);
            try {
                final Date date = df.parse(obj);
                final Calendar cal = Calendar.getInstance(timeZone, locale);
                cal.setTimeInMillis(date.getTime());
                return cal;
            } catch (final ParseException e) {
                throw new ConversionException(e);
            }
        }
    }

    /**
     * An object that converts a <code>String</code> to a
     * <code>java.util.Date</code>.
     */
    public static class StringToDate extends AbstractLocalizedConverter<String, Date> {
        public StringToDate() {
            super(String.class, Date.class);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return String.class.equals(sourceClass) && Date.class.equals(targetClass);
        }

        /**
         * Converts <code>obj</code> to a <code>java.util.Date</code>.
         * The string must be formatted as
         * {@link DateTimeConverters#CALENDAR_FORMAT}.
         */
        @Override
        public Date convert(final String obj) throws ConversionException {
            try {
                final DateFormat df = new SimpleDateFormat(CALENDAR_FORMAT);
                return df.parse(obj);
            } catch (final ParseException e) {
                throw new ConversionException(e);
            }
        }

        /**
         * Converts <code>obj</code> to a <code>java.util.Date</code>. If
         * <code>formatString</code> is <code>null</code>, the string is formatted as
         * {@link DateTimeConverters#CALENDAR_FORMAT}.
         */
        @Override
        public Date convert(final String obj, final Locale locale, final TimeZone timeZone, final String formatString) throws ConversionException {
            final DateFormat df = toDateTimeFormat(formatString == null ? CALENDAR_FORMAT : formatString, timeZone, locale);
            try {
                return df.parse(obj);
            } catch (final ParseException e) {
                throw new ConversionException(e);
            }
        }
    }

    /**
     * An object that converts a <code>String</code> to a
     * <code>java.sql.Date</code>.
     */
    public static class StringToSqlDate extends AbstractLocalizedConverter<String, java.sql.Date> {
        public StringToSqlDate() {
            super(String.class, java.sql.Date.class);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return String.class.equals(sourceClass) && java.sql.Date.class.equals(targetClass);
        }

        @Override
        public java.sql.Date convert(final String obj) throws ConversionException {
            return java.sql.Date.valueOf(obj);
        }

        /**
         * Converts <code>obj</code> to a <code>java.sql.Date</code> using the supplied
         * time zone. The <code>locale</code> and <code>formatString</code> parameters are
         * ignored. The string must be formatted as
         * {@link DateTimeConverters#JDBC_DATE_FORMAT}.
         */
        @Override
        public java.sql.Date convert(final String obj, final Locale locale, final TimeZone timeZone, final String formatString) throws ConversionException {
            final DateFormat df = toDateFormat(timeZone);
            try {
                return new java.sql.Date(df.parse(obj).getTime());
            } catch (final ParseException e) {
                throw new ConversionException(e);
            }
        }
    }

    /**
     * An object that converts a <code>String</code> to a
     * <code>java.sql.Time</code>.
     */
    public static class StringToSqlTime extends AbstractLocalizedConverter<String, Time> {
        public StringToSqlTime() {
            super(String.class, Time.class);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return String.class.equals(sourceClass) && Time.class.equals(targetClass);
        }

        @Override
        public Time convert(final String obj) throws ConversionException {
            return Time.valueOf(obj);
        }

        /**
         * Converts <code>obj</code> to a <code>java.sql.Time</code> using the supplied
         * time zone. The <code>locale</code> and <code>formatString</code> parameters are
         * ignored. The string must be formatted as
         * {@link DateTimeConverters#JDBC_TIME_FORMAT}.
         */
        @Override
        public Time convert(final String obj, final Locale locale, final TimeZone timeZone, final String formatString) throws ConversionException {
            final DateFormat df = toTimeFormat(timeZone);
            try {
                return new Time(df.parse(obj).getTime());
            } catch (final ParseException e) {
                throw new ConversionException(e);
            }
        }
    }

    /**
     * An object that converts a <code>String</code> to a
     * <code>java.sql.Timestamp</code>.
     */
    public static class StringToTimestamp extends AbstractLocalizedConverter<String, Timestamp> {
        public StringToTimestamp() {
            super(String.class, Timestamp.class);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return String.class.equals(sourceClass) && Timestamp.class.equals(targetClass);
        }

        @Override
        public Timestamp convert(final String obj) throws ConversionException {
            return Timestamp.valueOf(obj);
        }

        /**
         * Converts <code>obj</code> to a <code>java.sql.Timestamp</code>.
         * <p>Note that the string representation is referenced to the <code>timeZone</code>
         * argument, not UTC. The <code>Timestamp</code> that is returned is adjusted to UTC.
         * This behavior is intended to accommodate user-entered timestamps, where users are
         * accustomed to using their own time zone.</p>
         * </p>
         */
        @Override
        public Timestamp convert(final String obj, final Locale locale, final TimeZone timeZone, final String formatString) throws ConversionException {
            try {
                // The String is referenced to the time zone represented by the timeZone
                // argument, but the parsing code assumes a reference to UTC. So, we need
                // to "adjust" the parsed Timestamp's valorem.
                final Timestamp parsedStamp = Timestamp.valueOf(obj);
                final Calendar cal = Calendar.getInstance(timeZone, locale);
                cal.setTime(parsedStamp);
                cal.add(Calendar.MILLISECOND, 0 - timeZone.getOffset(parsedStamp.getTime()));
                final Timestamp result = new Timestamp(cal.getTimeInMillis());
                result.setNanos(parsedStamp.getNanos());
                return result;
            } catch (final Exception e) {
                throw new ConversionException(e);
            }
        }
    }

    /**
     * An object that converts a <code>String</code> ID to a
     * <code>java.util.TimeZone</code>.
     */
    public static class StringToTimeZone extends AbstractConverter<String, TimeZone> {
        public StringToTimeZone() {
            super(String.class, TimeZone.class);
        }

        @Override
        public TimeZone convert(final String obj) throws ConversionException {
            return TimeZone.getTimeZone(obj);
        }
    }

    /**
     * An object that converts a <code>java.sql.Timestamp</code> to a
     * <code>java.util.Date</code>.
     */
    public static class TimestampToDate extends AbstractConverter<Timestamp, Date> {
        public TimestampToDate() {
            super(Timestamp.class, Date.class);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return Timestamp.class.equals(sourceClass) && Date.class.equals(targetClass);
        }

        @Override
        public Date convert(final Timestamp obj) throws ConversionException {
            return new Timestamp(obj.getTime());
        }
    }

    /**
     * An object that converts a <code>java.sql.Timestamp</code> to a
     * <code>java.sql.Date</code>.
     */
    public static class TimestampToSqlDate extends AbstractConverter<Timestamp, java.sql.Date> {
        public TimestampToSqlDate() {
            super(Timestamp.class, java.sql.Date.class);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return Timestamp.class.equals(sourceClass) && java.sql.Date.class.equals(targetClass);
        }

        @Override
        public java.sql.Date convert(final Timestamp obj) throws ConversionException {
            return new java.sql.Date(obj.getTime());
        }
    }

    /**
     * An object that converts a <code>java.sql.Timestamp</code> to a
     * <code>java.sql.Time</code>.
     */
    public static class TimestampToSqlTime extends AbstractConverter<Timestamp, Time> {
        public TimestampToSqlTime() {
            super(Timestamp.class, Time.class);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return Timestamp.class.equals(sourceClass) && Time.class.equals(targetClass);
        }

        @Override
        public Time convert(final Timestamp obj) throws ConversionException {
            return new Time(obj.getTime());
        }
    }

    /**
     * An object that converts a <code>java.sql.Timestamp</code> to a
     * <code>String</code>.
     */
    public static class TimestampToString extends AbstractLocalizedConverter<Timestamp, String> {
        public TimestampToString() {
            super(Timestamp.class, String.class);
        }

        @Override
        public boolean canConvert(final Class<?> sourceClass, final Class<?> targetClass) {
            return Timestamp.class.equals(sourceClass) && String.class.equals(targetClass);
        }

        @Override
        public String convert(final Timestamp obj) throws ConversionException {
            return obj.toString();
        }

        /**
         * Converts <code>obj</code> to a <code>String</code> using the supplied
         * time zone.
         * <p>Note that the string representation is referenced to the <code>timeZone</code>
         * argument, not UTC. The <code>Timestamp</code> is adjusted to the specified
         * time zone before conversion. This behavior is intended to accommodate user interfaces,
         * where users are accustomed to viewing timestamps in their own time zone.</p>
         * </p>
         */
        @Override
        public String convert(final Timestamp obj, final Locale locale, final TimeZone timeZone, final String formatString) throws ConversionException {
            try {
                // The Timestamp is referenced to UTC, but the String result needs to be
                // referenced to the time zone represented by the timeZone argument.
                // So, we need to "adjust" the Timestamp's value before conversion.
                final Calendar cal = Calendar.getInstance(timeZone, locale);
                cal.setTime(obj);
                cal.add(Calendar.MILLISECOND, timeZone.getOffset(obj.getTime()));
                final Timestamp result = new Timestamp(cal.getTimeInMillis());
                result.setNanos(obj.getNanos());
                return result.toString();
            } catch (final Exception e) {
                throw new ConversionException(e);
            }
        }
    }

    /**
     * An object that converts a <code>java.util.TimeZone</code> to a
     * <code>String</code> ID.
     */
    public static class TimeZoneToString extends AbstractConverter<TimeZone, String> {
        public TimeZoneToString() {
            super(TimeZone.class, String.class);
        }

        @Override
        public String convert(final TimeZone obj) throws ConversionException {
            return obj.getID();
        }
    }
}
