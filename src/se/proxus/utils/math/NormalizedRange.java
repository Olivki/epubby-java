package se.proxus.utils.math;

import lombok.ToString;
import lombok.experimental.Delegate;
import org.apache.commons.lang3.Range;
import se.proxus.utils.lang.NumberUtilities;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * <p>A sort of sub-class of the {@link Range} class from the Apache Commons Lang 3 library.</p>
 *
 * <p>This introduces the feature of the value automatically being set to either the set minimum
 * or the set maximum value should it be outside the defined range.</p>
 *
 * <p>This can be useful when working with ranges that will be used to filter user input, as
 * these methods also implements the use of the {@link Consumer} & {@link BiConsumer} functional
 * interfaces so that appropriate actions can be taken when a value that does not fit within the
 * range has been entered.</p>
 *
 * @param <V>
 *     the type parameter
 *
 * @author Oliver Berg
 */
@ToString
public final class NormalizedRange<V extends Number & Comparable<V>> {
    
    @Delegate
    private final Range<V> range;
    
    /**
     * Instantiates a new Normalized range.
     *
     * @param min
     *     the min
     * @param max
     *     the max
     */
    public NormalizedRange(final V min, final V max) {
        this.range = Range.between(min, max);
    }
    
    /**
     * Normalize v.
     *
     * @param value
     *     the value
     *
     * @return the v
     */
    public V normalize(final V value) {
        if (contains(value)) {
            return value;
        } else if (isAfter(value)) {
            return getMinimum();
        } else if (isBefore(value)) {
            return getMaximum();
        }
        
        throw new NullPointerException(String.format(
            "Range[%s, %s] does not contain the Value[%s], but the range is also not before or " +
            "after the value.",
            getMinimum(),
            getMaximum(),
            value));
    }
    
    /**
     * Normalize v.
     *
     * @param value
     *     the value
     * @param consumer
     *     the consumer
     *
     * @return the v
     */
    public V normalize(final V value, final BiConsumer<V, ValueState> consumer) {
        if (contains(value)) {
            consumer.accept(value, ValueState.CONTAINS);
            return value;
        } else if (isAfter(value)) {
            consumer.accept(value, ValueState.TOO_SMALL);
            return getMinimum();
        } else if (isBefore(value)) {
            consumer.accept(value, ValueState.TOO_BIG);
            return getMaximum();
        }
        
        throw new NullPointerException(String.format(
            "Range[%s, %s] does not contain the Value[%s], but the range is also not before or " +
            "after the value.",
            getMinimum(),
            getMaximum(),
            value));
    }
    
    /**
     * Check.
     *
     * @param value
     *     the value
     * @param consumer
     *     the consumer
     */
    // Redundant?
    public void check(final V value, final BiConsumer<V, ValueState> consumer) {
        if (contains(value)) {
            consumer.accept(value, ValueState.CONTAINS);
        } else if (isAfter(value)) {
            consumer.accept(value, ValueState.TOO_SMALL);
        } else if (isBefore(value)) {
            consumer.accept(value, ValueState.TOO_BIG);
        }
    }
    
    /**
     * Check.
     *
     * @param value
     *     the value
     * @param consumer
     *     the consumer
     */
    public boolean contains(final V value, final Consumer<ValueState> consumer) {
        if (contains(value)) {
            consumer.accept(ValueState.CONTAINS);
            return true;
        } else if (isAfter(value)) {
            consumer.accept(ValueState.TOO_SMALL);
            return false;
        } else if (isBefore(value)) {
            consumer.accept(ValueState.TOO_BIG);
            return false;
        }
        
        return false;
    }
    
    public boolean isRedundant() {
        return NumberUtilities.isMinValue(getMinimum()) && NumberUtilities.isMaxValue(getMaximum());
    }
    
    public String getVisual() {
        return getMinimum() + ".." + getMaximum();
    }
    
    /**
     * The enum Value state.
     */
    // Come up with a better name.
    public enum ValueState {
        /**
         * Contains value state.
         */
        CONTAINS,
        /**
         * Too small value state.
         */
        TOO_SMALL,
        /**
         * Too big value state.
         */
        TOO_BIG
    }
}
