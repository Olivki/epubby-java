package se.proxus.utils.math;

import lombok.experimental.UtilityClass;
import org.apache.commons.math3.util.FastMath;

import java.util.concurrent.ThreadLocalRandom;

@UtilityClass
public class MathUtilities {
    
    public static final NormalizedRange<Integer> BYTE_RANGE =
        new NormalizedRange<>((int) Byte.MIN_VALUE, (int) Byte.MAX_VALUE);
    public static final NormalizedRange<Integer> SHORT_RANGE =
        new NormalizedRange<>((int) Short.MIN_VALUE, (int) Short.MAX_VALUE);
    public static final NormalizedRange<Long> INTEGER_RANGE =
        new NormalizedRange<>((long) Integer.MIN_VALUE, (long) Integer.MAX_VALUE);
    
    public int random(final int min, final int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }
    
    // -- BYTE -- \\
    public byte addExact(final byte a, final byte b) {
        return BYTE_RANGE.normalize(FastMath.addExact(a, b)).byteValue();
    }
    
    public byte subtractExact(final byte a, final byte b) {
        return BYTE_RANGE.normalize(FastMath.subtractExact(a, b)).byteValue();
    }
    
    public byte multiplyExact(final byte a, final byte b) {
        return BYTE_RANGE.normalize(FastMath.multiplyExact(a, b)).byteValue();
    }
    
    public byte floorDiv(final byte a, final byte b) {
        return BYTE_RANGE.normalize(FastMath.floorDiv(a, b)).byteValue();
    }
    
    // -- SHORT -- \\
    public short addExact(final short a, final short b) {
        return SHORT_RANGE.normalize(FastMath.addExact(a, b)).shortValue();
    }
    
    public short subtractExact(final short a, final short b) {
        return SHORT_RANGE.normalize(FastMath.subtractExact(a, b)).shortValue();
    }
    
    public short multiplyExact(final short a, final short b) {
        return SHORT_RANGE.normalize(FastMath.multiplyExact(a, b)).shortValue();
    }
    
    public short floorDiv(final short a, final short b) {
        return SHORT_RANGE.normalize(FastMath.floorDiv(a, b)).shortValue();
    }
}
