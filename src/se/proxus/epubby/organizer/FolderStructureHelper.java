package se.proxus.epubby.organizer;

import lombok.experimental.UtilityClass;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import se.proxus.epubby.resources.ResourceType;
import se.proxus.epubby.tools.FileTools;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

@UtilityClass
public final class FolderStructureHelper {
    
    public static void checkStructureValidity(final File bookDirectory) throws IOException {
        for (final File file : Objects.requireNonNull(bookDirectory.listFiles())) {
            if (file.isDirectory()) {
                if (!isAcceptedName(file.getName())) {
                    changeName(file);
                }
            } else {
                final String extension = FileTools.getExtension(file);
                
                if (!(extension.equalsIgnoreCase("ncx") || extension.equalsIgnoreCase("opf"))) {
                    final String destName = getFolderFromExtension(file);
                    final File destDir = FileTools.createDirectory(new File(bookDirectory,
                                                                            destName));
                    
                    FileUtils.moveFileToDirectory(file, destDir, false);
                }
            }
        }
    }
    
    public static void changeName(final File directory) {
        if (!FileTools.isDirectoryEmpty(directory)) {
            final File childFile = Objects.requireNonNull(directory.listFiles())[0];
            
            final String newName = getFolderFromExtension(childFile);
            
            directory.renameTo(new File(directory.getParentFile(), newName));
        }
    }
    
    public static String getNewName(final String fileName) {
        return getFolderFromExtension(FileTools.getExtension(fileName));
    }
    
    public static boolean isAcceptedName(final String folderName) {
        for (final String acceptedName : getAcceptedFolderNames()) {
            if (acceptedName.equals(folderName)) {
                return true;
            }
        }
        
        return false;
    }
    
    public static String[] getAcceptedFolderNames() {
        final String[] acceptedFolderNames = new String[ResourceType.values().length - 2];
        
        for (int i = 0; i < (ResourceType.values().length - 2); i++) {
            final ResourceType type = ResourceType.values()[i];
            acceptedFolderNames[i] = type.getLocation()
                                         .replace("/", "");
        }
        
        return acceptedFolderNames;
    }
    
    public static String getLocation(final ResourceType resourceType) {
        return resourceType.getLocation()
                           .replace("/", "");
    }
    
    public static boolean isValidName(final String name) {
        return StringUtils.startsWithIgnoreCase(name, "OEBPS") || StringUtils.startsWithIgnoreCase(
            name,
            "META-INF") || StringUtils.startsWithIgnoreCase(name, "mimetype");
    }
    
    public static String fixHref(final String faultyHref) {
        if (faultyHref.contains("/")) {
            final String faultyLocation = faultyHref.split("/")[0];
            
            if (!isAcceptedName(faultyLocation)) {
                final String fileName = faultyHref.split("/")[1];
                final String fixedLocation = getNewName(fileName);
                
                return fixedLocation + "/" + fileName;
            }
        }
        
        return faultyHref;
    }
    
    public static String getFolderFromExtension(final File file) {
        return getFolderFromExtension(FileTools.getExtension(file)
                                               .toUpperCase());
    }
    
    public static String getFolderFromExtension(final String extension) {
        return ResourceType.fromExtension(extension).getLocation();
        /*switch (extension) {
            case "NCX":
                return "";
            
            case "OPF":
                return "";
            
            case "HTML":
                return getLocation(ResourceType.PAGES);
            
            case "XHTML":
                return getLocation(ResourceType.PAGES);
            
            case "CSS":
                return getLocation(ResourceType.STYLES);
            
            case "JPG":
                return getLocation(ResourceType.IMAGES);
            
            case "JPEG":
                return getLocation(ResourceType.IMAGES);
            
            case "PNG":
                return getLocation(ResourceType.IMAGES);
            
            case "GIF":
                return getLocation(ResourceType.IMAGES);
            
            case "SVG":
                return getLocation(ResourceType.IMAGES);
            
            case "WEBM":
                return getLocation(ResourceType.VIDEO);
            
            case "MP4":
                return getLocation(ResourceType.VIDEO);
            
            case "MKV":
                return getLocation(ResourceType.VIDEO);
            
            default:
                return null;
        }*/
    }
    
}