package se.proxus.epubby.tools;

import java.util.List;

public final class ListTools {

    public static void changeIndex(final List list, final Object object, final int newIndex) {
        list.remove(list.indexOf(object));
        list.add(newIndex, object);
    }
    
}