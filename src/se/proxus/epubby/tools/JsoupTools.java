package se.proxus.epubby.tools;

import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public final class JsoupTools {

    public static boolean hasClass(final Element element) {
        return element.hasClass(element.className());
    }

    public static boolean exists(final Elements elements) {
        return !elements.isEmpty();
    }

    public static boolean isEmpty(final Element element) {
        return element.children().size() <= 0 && element.ownText().trim().isEmpty();
    }

    public static boolean isParentEmpty(final Element element) {
        return element.parent().children().size() <= 1 && element.parent().ownText().trim().isEmpty();
    }

    public static void removeClass(final Elements elements) {
        for (final Element elementToPrune : elements) {
            elementToPrune.removeClass(elementToPrune.className());
        }
    }

    public static void removeClass(final Element element) {
        element.removeClass(element.className());
    }

    public static void forceRemoveClass(final Elements elements) {
        for (final Element elementToPrune : elements) {
            for (final Attribute attr : elementToPrune.attributes()) {
                if (attr.getKey().equalsIgnoreCase("class")) {
                    elementToPrune.removeAttr(attr.getKey());
                }
            }
        }
    }

    public static void replaceClass(final Elements elements, final String newClass, final boolean force) {
        if (force) {
            forceRemoveClass(elements);
        } else {
            removeClass(elements);
        }

        elements.addClass(newClass);
    }

    public static boolean containsOnly(final Element parentElement, final String match) {
        if (parentElement.select(match).isEmpty()) {
            return false;
        }

        for (final Element element : parentElement.getAllElements()) {
            if (!element.is(match)) {
                if (element.is("div") || element.is("section") || element.is("body")) {
                    continue;
                }

                if (element.is("p") || element.is("span")) {
                    if (element.text().trim().isEmpty()) {
                        continue;
                    }
                }

                return false;
            }
        }

        return true;
    }

    public static boolean contains(final Element element, final String selector) {
        boolean contains = element.is(selector);

        if (element.children().size() > 0 && !(contains)) {
            for (final Element childElement : element.children()) {
                contains = contains(childElement, selector);

                if (contains) {
                    break;
                }
            }
        }

        return contains;
    }

    public static void replaceClass(final Element element, final String newClass, final boolean force) {
        if (hasClass(element)) {
            if (force) {
                forceRemoveClass(element);
            } else {
                removeClass(element);
            }
        }

        element.addClass(newClass);
    }

    public static void forceRemoveClass(final Element element) {
        for (final Attribute attr : element.attributes()) {
            if (attr.getKey().equalsIgnoreCase("class")) {
                element.removeAttr(attr.getKey());
            }
        }
    }

}