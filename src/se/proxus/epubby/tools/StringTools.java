package se.proxus.epubby.tools;

import com.google.common.primitives.Chars;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class StringTools {

    private static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static String numbersToLetters(String text) {
        if (isInteger(text)) {
            for (int numbers = 0; numbers < 10; numbers++) {
                text = text.replace("" + numbers, "" + ALPHABET.charAt(numbers));
            }
        }

        return text;
    }

    public static String lettersToNumbers(final String text) {
        final StringBuilder builder = new StringBuilder();

        for (int curCharIndex = 0; curCharIndex < text.toCharArray().length; curCharIndex++) {
            final char curChar = text.charAt(curCharIndex);

            builder.append(Character.getNumericValue(curChar));
        }

        return builder.toString();
    }

    public static String toBookIdentifier() {
        final String name = lettersToNumbers(shuffle(System.getProperty("user.name")));
        final String shuffled = shuffle((name + System.currentTimeMillis()));
        return Base64.getEncoder().encodeToString(shuffled.getBytes());
    }

    public static boolean fromYesOrNo(final String yesOrNo) {
        if (yesOrNo.equalsIgnoreCase("yes")) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean startsWithUppercase(final String string) {
        return Character.isUpperCase(string.charAt(0));
    }

    public static boolean startsWithPad(final String string) {
        return string.matches("^[0-9]*[_]\\S+");
    }

    public static boolean endsWithPad(final String string) {
        return string.matches("^.+?[_][0-9][0-9]$");
    }

    //https://stackoverflow.com/questions/23390612/how-to-get-position-of-the-first-letter-in-a-string
    public static int findFirstLetterPosition(final String input) {
        for (int index = 0; index < input.length(); index++) {
            if (Character.isLetter(input.charAt(index))) {
                return index;
            }
        }
        return -1; // not found
    }

    public static String sorroundCharAt(final String input, final int index, final String sorround) {
        return input.substring(0, index) + sorround + input.substring(index + 1);
    }

    public static boolean charIsSpecial(final char character) {
        final String specialCharacters = "?!.,#¤%&/()=\":;-_/\\* ";

        return specialCharacters.indexOf(character) > 0;
    }

    public static boolean startsWithSpecial(final String string) {
        final String specialCharacters = "?!.,#¤%&/()=\":;-_/\\* ";

        if (string.isEmpty()) {
            return false;
        }

        return specialCharacters.indexOf(string.charAt(0)) > 0;
    }

    public static String removeLocation(final String location) {
        return location.substring(location.indexOf("../"));
    }

    public static String pad(final int number) {
        final StringBuilder builder = new StringBuilder();

        if (number > 9) {
            builder.append(number);
        } else if (number <= 9 && number >= 0) {
            builder.append("0").append(number);
        }

        return builder.toString();
    }

    public static String toYesOrNo(final boolean check) {
        return (check ? "yes" : "no");
    }

    public static String intToHex(final int integer) {
        return Integer.toHexString(integer);
    }

    public static String insertEveryLetter(final String text, final String extra) {
        String result = "";
        for (int letters = 0; letters < text.length(); letters++) {
            final char character = text.charAt(letters);
            if (character != ' ') {
                result += extra + character;
            } else {
                result += " ";
            }
        }
        return result;
    }

    public static int getNumbersBeforeSequence(final String text, final String sequence) {
        final int sequencePosition = text.indexOf(sequence);
        int lastNumberPosition = 0;

        for (int currentCharacter = sequencePosition - 1;
             currentCharacter <= sequencePosition
                     && currentCharacter >= 0; currentCharacter--) {
            final String currentChar = "" + text.charAt(currentCharacter);

            if (!(isNumber(currentChar))) {
                lastNumberPosition = currentCharacter + 1;
                break;
            }
        }

        final String finalCut =
                text.substring(lastNumberPosition, text.indexOf(sequence));

        return Integer.parseInt(finalCut);
    }

    public static int getNumbersAfterSequence(String text, final String sequence) {
        final int sequencePosition = text.indexOf(sequence);
        text = text.substring(sequencePosition + 1, text.length());
        final Pattern pattern = Pattern.compile("-?\\d+");
        final Matcher matcher = pattern.matcher(text);

        while (matcher.find()) {
            return Integer.parseInt(matcher.group());
        }

        return -1;
    }

    // https://stackoverflow.com/questions/4030928/extract-digits-from-a-string-in-java
    public static String stripNonDigits(final CharSequence input){
        final StringBuilder sb = new StringBuilder(input.length());
        for(int i = 0; i < input.length(); i++){
            final char c = input.charAt(i);
            if(c > 47 && c < 58){
                sb.append(c);
            }
        }
        return sb.toString();
    }

    public static String arrayToStringList(final Object[] array) {
        final StringBuilder stringBuilder = new StringBuilder();

        for (final Object object : array) {
            stringBuilder.append(object).append(System.lineSeparator());
        }

        return StringUtils.stripEnd(stringBuilder.toString(), System.lineSeparator());
    }

    public static String listToStringList(final List<?> array) {
        final StringBuilder stringBuilder = new StringBuilder();

        for (final Object object : array) {
            stringBuilder.append(object).append(System.lineSeparator());
        }

        return StringUtils.stripEnd(stringBuilder.toString(), System.lineSeparator());
    }

    public static String listToString(final List<?> array) {
        final StringBuilder stringBuilder = new StringBuilder();

        for (final Object object : array) {
            stringBuilder.append(object).append(", ");
        }

        return StringUtils.stripEnd(stringBuilder.toString(), ", ");
    }

    public static String substring(final String string, final String firstIndex, final String lastIndex) {
        return StringUtils.substring(string, string.indexOf(firstIndex), string.indexOf(lastIndex));
    }

    public static String substring(final String string, final int end) {
        return StringUtils.substring(string, 0, end);
    }

    public static boolean arrayContains(final String[] array, final String name) {
        for (final String checkAgainst : array) {
            if (StringUtils.containsIgnoreCase(name, checkAgainst)) {
                return true;
            }
        }

        return false;
    }

    public static boolean listContains(final List<String> list, final String name) {
        for (final String checkAgainst : list) {
            if (StringUtils.containsIgnoreCase(name, checkAgainst)) {
                return true;
            }
        }

        return false;
    }

    public static String[] listToStringArray(final List<String> list) {
        final String[] tempArray = new String[list.size()];

        for (int index = 0; index < list.size(); index++) {
            tempArray[index] = list.get(index);
        }

        return tempArray;
    }

    public static String href(final String href) {
        return substringFromLastIndex(href, "/");
    }

    public static String substringFromLastIndex(final String str, final String indexOf) {
        return str.substring(str.lastIndexOf(indexOf) + 1);
    }

    public static String substringToLastIndex(final String str, final String indexOf) {
        return str.substring(0, str.lastIndexOf(indexOf));
    }

    public static String arrayToString(final Object[] array) {
        final StringBuilder stringBuilder = new StringBuilder();

        for (final Object object : array) {
            stringBuilder.append(object).append(", ");
        }

        return StringUtils.stripEnd(stringBuilder.toString(), ", ");
    }

    public static String getFirstWord(final String string) {
        final StringBuilder builder = new StringBuilder();

        for (int index = 0; index < string.toCharArray().length; index++) {
            final char curChar = string.charAt(index);

            if (Character.isWhitespace(curChar)) {
                break;
            } else {
                builder.append(curChar);
            }
        }

        return builder.toString();
    }

    public static String arrayToEnclosedString(final Object[] array) {
        final StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Array [");

        for (int index = 0; index < array.length; index++) {
            final Object object = array[index];

            stringBuilder.append("{").append(object).append("}");

            if ((array.length - 1) > index) {
                stringBuilder.append(", ");
            }
        }

        stringBuilder.append("]");

        return StringUtils.stripEnd(stringBuilder.toString(), ", ");
    }

    public static String humanReadableByteCount(final long bytes, final boolean si) {
        final int unit = si ? 1000 : 1024;
        if (bytes < unit) { return bytes + " B"; }
        final int exp = (int) (Math.log(bytes) / Math.log(unit));
        final String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    /**
     * Creates a linguistic series joining the input objects together. Examples: 1) {} --> "", 2) {"Steve"}
     * --> "Steve", 3) {"Steve", "Phil"} --> "Steve and Phil", 4) {"Steve", "Phil", "Mark"} --> "Steve, Phil
     * and Mark"
     */
    public static String joinNiceString(final Object[] elements) {
        final StringBuilder stringbuilder = new StringBuilder();

        for (int i = 0; i < elements.length; ++i) {
            final String s = elements[i].toString();

            if (i > 0) {
                if (i == elements.length - 1) {
                    stringbuilder.append(" and ");
                } else {
                    stringbuilder.append(", ");
                }
            }

            stringbuilder.append(s);
        }

        return stringbuilder.toString();
    }

    public static int stringToNumber(final String string) {
        final char[] alphabetChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        final Map<Character, Integer> charMap = new HashMap<Character, Integer>();
        int j = 0;
        for (final char c : alphabetChars) {
            charMap.put(c, j++);
        }
        int index = 0;
        int multiplier = 1;
        for (final char currentChar : new StringBuffer(string).reverse().toString().toCharArray()) {
            index += charMap.get(currentChar) * multiplier;
            multiplier *= alphabetChars.length;
        }
        return index;
    }

    public static String numberToCharacterRepresentation(int number) {
        final char[] alphabetChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        String result = "";
        while (true) {
            result = alphabetChars[number % 26] + result;
            if (number < 26) {
                break;
            }
            number /= 26;
        }
        return result;
    }

    public static String shuffle(final String string) {
        final List<Character> chars = Chars.asList(string.toCharArray());
        Collections.shuffle(chars);
        return new String(Chars.toArray(chars));
    }

    /**
     * Creates a linguistic series joining together the elements of the given collection. Examples: 1) {} -->
     * "", 2) {"Steve"} --> "Steve", 3) {"Steve", "Phil"} --> "Steve and Phil", 4) {"Steve", "Phil", "Mark"}
     * --> "Steve, Phil and Mark"
     */
    public static String joinNiceString(final Collection<String> strings) {
        return joinNiceString(strings.toArray(new String[strings.size()]));
    }

    /**
     * creates a new array and sets elements 0..n-2 to be 0..n-1 of the input (n elements)
     */
    public static String[] dropFirstString(final String[] input) {
        final String[] newString = new String[input.length - 1];
        System.arraycopy(input, 1, newString, 0, input.length - 1);
        return newString;
    }

    public static boolean isNumber(final String text) {
        return isInteger(text) || isLong(text) || isFloat(text) || isDouble(text) ||
                isShort(text)
                || isByte(text);
    }

    public static boolean isBoolean(final String text) {
        try {
            Boolean.parseBoolean(text);
            return true;
        } catch (final Exception exception) {
            return false;
        }
    }

    public static boolean isInteger(final String text) {
        try {
            Integer.parseInt(text);
            return true;
        } catch (final NumberFormatException exception) {
            return false;
        }
    }

    public static boolean isFloat(final String text) {
        try {
            Float.parseFloat(text);
            return true;
        } catch (final NumberFormatException exception) {
            return false;
        }
    }

    public static boolean isLong(final String text) {
        try {
            Long.parseLong(text);
            return true;
        } catch (final NumberFormatException exception) {
            return false;
        }
    }

    public static boolean isDouble(final String text) {
        try {
            Double.parseDouble(text);
            return true;
        } catch (final NumberFormatException exception) {
            return false;
        }
    }

    public static boolean isShort(final String text) {
        try {
            Short.parseShort(text);
            return true;
        } catch (final NumberFormatException exception) {
            return false;
        }
    }

    public static boolean isByte(final String text) {
        try {
            Byte.parseByte(text);
            return true;
        } catch (final NumberFormatException exception) {
            return false;
        }
    }
}
