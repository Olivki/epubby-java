package se.proxus.epubby.tools;

import java.awt.image.BufferedImage;

public class ImageTools {

    public static boolean isSameOrLarger(final BufferedImage image1, final int compareWidth, final int compareHeight) {
        return image1.getWidth() >= compareWidth && image1.getHeight() >= compareHeight;
    }

    public static boolean match(final BufferedImage image1, final int compareWidth, final int compareHeight) {
        return image1.getWidth() == compareWidth && image1.getHeight() == compareHeight;
    }

    public static boolean match(final BufferedImage image1, final BufferedImage image2) {
        return image1.getWidth() == image2.getWidth() && image1.getHeight() == image2.getHeight();
    }
    
}