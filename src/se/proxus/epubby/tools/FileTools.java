package se.proxus.epubby.tools;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public final class FileTools {

    public static final int[] OS_ID = new int[EnumOS.values().length];

    public static void init() {
        try {
            OS_ID[EnumOS.LINUX.ordinal()] = 1;
        } catch (final NoSuchFieldError var4) {
            ;
        }

        try {
            OS_ID[EnumOS.SOLARIS.ordinal()] = 2;
        } catch (final NoSuchFieldError var3) {
            ;
        }

        try {
            OS_ID[EnumOS.WINDOWS.ordinal()] = 3;
        } catch (final NoSuchFieldError var2) {
            ;
        }

        try {
            OS_ID[EnumOS.MACOS.ordinal()] = 4;
        } catch (final NoSuchFieldError var1) {
            ;
        }
    }

    public static String getMediaType(final File file) throws IOException {
        final String extension = getExtension(file);
        final String mediaType;

        if (extension.equalsIgnoreCase("ncx")) {
            mediaType = "application/x-dtbncx+xml";
        } else if (extension.equalsIgnoreCase("otf")) {
            mediaType = "application/x-font-opentype";
        } else if (extension.equalsIgnoreCase("ttf")) {
            mediaType = "application/x-font-truetype";
        } else {
            mediaType = Files.probeContentType(file.toPath());
        }

      return mediaType;
    }

    public static File createDirectory(final File directory) {
        directory.mkdirs();
        return directory;
    }

    public static String getLocation(final File file) {
        return "../" + file.getParentFile().getName() + "/" + file.getName();
    }

    public static List<File> getAllFiles(final File directory, final boolean includeDirs) {
        final List<File> allFiles = new LinkedList<>();

        for (final File file : directory.listFiles()) {
            if (file.isDirectory()) {
                allFiles.addAll(getAllFiles(file, includeDirs));

                if (includeDirs) {
                    allFiles.add(file);
                }
            } else {
                allFiles.add(file);
            }
        }

        return allFiles;
    }

    public static File getSubDirectory(final File directory, final String serverName) {
        final File tempFile = new File(directory, serverName);

        if (!(tempFile.exists())) {
            tempFile.mkdirs();
        }

        return tempFile;
    }

    public static File getAppDir(final String fileName) {
        final String userHome = System.getProperty("user.home");
        final File file;

        switch (OS_ID[getOs().ordinal()]) {
            case 1:
            case 2:
                file = new File(userHome, fileName + '/');
                break;

            case 3:
                final String appData = System.getenv("APPDATA");

                if (appData != null) {
                    file = new File(appData, fileName + '/');
                } else {
                    file = new File(userHome, fileName + '/');
                }

                break;

            case 4:
                file = new File(userHome, "Library/Application Support/" + fileName);
                break;

            default:
                file = new File(userHome, fileName + '/');
        }

        if (!file.exists() && !file.mkdirs()) {
            throw new RuntimeException("The working directory could not be created: " + file);
        } else {
            return file;
        }
    }

    public static EnumOS getOs() {
        final String osName = System.getProperty("os.name").toLowerCase();
        return osName.contains("win") ? EnumOS.WINDOWS
                : osName.contains("mac") ? EnumOS.MACOS
                : osName.contains("solaris") ? EnumOS.SOLARIS : osName.contains("sunos")
                ? EnumOS.SOLARIS : osName.contains("linux") ? EnumOS.LINUX
                : osName.contains("unix") ? EnumOS.LINUX : EnumOS.UNKNOWN;
    }

    public static void execute(final File file) {
        if (file.canExecute()) {
            try {
                final Runtime run = Runtime.getRuntime();
                run.exec(file.getPath());
            } catch (final Exception exception) {
                exception.printStackTrace();
            }
        }
    }

    public static File getDirectory(final File directory, final String name) {
        File tempFile = null;

        for (final File file : directory.listFiles()) {
            if (file.getName().equalsIgnoreCase(name) && file.isDirectory()) {
                tempFile = file;
                break;
            } else if (file.isDirectory()) {
                tempFile = getFile(file, name);
            } else {
                continue;
            }

        }

        return tempFile;
    }

    public static File getFile(final File directory, final String name) {
        File tempFile = null;

        for (final File file : directory.listFiles()) {
            if (file.getName().equalsIgnoreCase(name)) {
                tempFile = file;
                break;
            } else if (file.isDirectory()) {
                tempFile = getFile(file, name);
            } else {
                continue;
            }

        }

        return tempFile;
    }

    public static void deleteFilesByName(final String name, final File directory) {
        try {
            for (final File files : directory.listFiles()) {
                if (StringUtils.startsWithIgnoreCase(files.getName(), name)) {
                    FileUtils.forceDelete(files);
                }
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean doesFileExist(final String name, final File directory) {
        try {
            for (final File file : directory.listFiles()) {
                if (StringUtils.startsWithIgnoreCase(file.getName(), name)) {
                    return true;
                }
            }
        } catch (final Exception e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    public static boolean isFolderEmpty(final File directory) {
        return directory.listFiles().length <= 0;
    }

    public static void openFile(final File file) {
        try {
            Desktop.getDesktop().open(file);
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    public static List<File> getFilesWithExtension(final File directory, final String type) {
        final List<File> foundFiles = new LinkedList<>();

        for (final File file : directory.listFiles()) {
            System.out.println(getExtension(file));
        }

        return foundFiles;
    }

    public static String getNameWithoutExtension(final String string) {
        return StringTools.substringToLastIndex(string, ".");
    }

    public static String getNameWithoutExtension(final File file) {
        return StringTools.substringToLastIndex(file.getName(), ".");
    }

    public static String getExtension(final String fileName) {
        return StringTools.substringFromLastIndex(fileName, ".").toUpperCase();
    }

    public static String getExtension(final File file) {
        return StringTools.substringFromLastIndex(file.getName(), ".").toUpperCase();
    }

    public static boolean hasExtension(final File file, final String type) {
        return StringUtils.endsWithIgnoreCase(file.getName(), type);
    }

    public static boolean isDirectoryEmpty(final File file) {
        return file.isDirectory() ? file.listFiles().length <= 0 : false;
    }

    public static List<String> filesToList(final File directory, final boolean listFolders,
            final boolean includeExtension) {
        final List<String> fileNames = new LinkedList<>();

        if (!directory.isDirectory() || isDirectoryEmpty(directory)) {
            return null;
        }

        for (final File file : directory.listFiles()) {
            if (!listFolders) {
                if (file.isDirectory()) {
                    continue;
                }
            }

            if (includeExtension) {
                fileNames.add(file.getName());
            } else {
                fileNames.add(getNameWithoutExtension(file));
            }
        }

        return fileNames;
    }

    public static void renameFiles(final List<Pair<File, File>> files) {
        for (final Pair<File, File> arrayHelper : files) {
            final File fileToRename = arrayHelper.getKey();
            final File fileToCopy = arrayHelper.getValue();

            fileToRename.renameTo(new File(fileToRename.getParentFile(), fileToCopy.getName()));
        }
    }

    public static File createTempDir(final File directory) {
        final String baseName = System.currentTimeMillis() + "-";

        for (int counter = 0; counter < 10000; ++counter) {
            final File tempDir = new File(directory, baseName + counter);
            if (tempDir.mkdir()) {
                return tempDir;
            }
        }

        throw new IllegalStateException(
                "Failed to create directory within 10000 attempts (tried " + baseName + "0 to " + baseName +
                        9999 + ')');
    }

    public static void extract(final File directory, final ZipArchiveEntry entry, final InputStream inputStream) throws IOException {
        final File zipFile = new File(directory, entry.getName());

        if (!zipFile.getParentFile().exists()) {
            zipFile.getParentFile().mkdirs();
        } else if (zipFile.getParentFile().exists() && !(zipFile.getParentFile().isDirectory())) {
            zipFile.getParentFile().delete();
            zipFile.getParentFile().mkdir();
        }

        try {
            final FileOutputStream outputStream = new FileOutputStream(zipFile);
            IOUtils.copy(inputStream, outputStream);
            outputStream.close();
            //inputStream.close();
        } catch (final Exception exception) {
            exception.printStackTrace();
        }

    }

    public static void pack(final File sourceDirectory, final File zipFile) throws IOException {
        if (zipFile.exists()) {
            FileUtils.forceDelete(zipFile);
        }

        final Path zipPath = Files.createFile(zipFile.toPath());
        try (ZipOutputStream zipStream = new ZipOutputStream(Files.newOutputStream(zipPath))) {
            final Path sourcePath = sourceDirectory.toPath();
            Files.walk(sourcePath)
                    .filter(path -> !Files.isDirectory(path))
                    .forEach(path -> {
                        final ZipEntry zipEntry = new ZipEntry(sourcePath.relativize(path).toString());
                        try {
                            zipStream.putNextEntry(zipEntry);
                            Files.copy(path, zipStream);
                            //System.out.println(zipEntry.getName());
                            zipStream.closeEntry();
                        } catch (final IOException e) {
                            System.err.println(e);
                        }
                    });
        }
    }

    enum EnumOS {
        LINUX, SOLARIS, WINDOWS, MACOS, UNKNOWN;
    }
}