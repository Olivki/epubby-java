package se.proxus.epubby.cleaner.cleaners.modes;

import se.proxus.epubby.cleaner.Cleaner;
import se.proxus.epubby.cleaner.CleanerConstants;
import se.proxus.epubby.cleaner.ICleanerMode;


public final class CleanerModeProxus implements ICleanerMode {
    
    @Override
    public void setupCleaners(final Cleaner cleaner) {
        cleaner.registerCleaner(CleanerConstants.HEADERS_CONVERT_NUMBER_TO_WORD);
        cleaner.registerCleaner(CleanerConstants.BOLD_AND_ITALIC);
        cleaner.registerCleaner(CleanerConstants.REPLACE_NAME);
        cleaner.registerCleaner(CleanerConstants.BIG_LETTER_FIRST_SENTENCE);
        cleaner.registerCleaner(CleanerConstants.ADD_IMG_CLASS);
        cleaner.registerCleaner(CleanerConstants.REMOVE_COMMENTS);
        cleaner.registerCleaner(CleanerConstants.PAGE_FILE_NAME_TRIMMER);
        cleaner.registerCleaner(CleanerConstants.BOOK_TITLE_FROM_FILE);
        cleaner.registerCleaner(CleanerConstants.BAKA_TSUKI_FIXER);
        cleaner.registerCleaner(CleanerConstants.IMG_UNWRAPPER);
        cleaner.registerCleaner(CleanerConstants.TRIM);
        cleaner.registerCleaner(CleanerConstants.REMOVE_EMPTY_PARAGRAPHS);
        cleaner.registerCleaner(CleanerConstants.SPAN_CLUTTER);
        cleaner.registerCleaner(CleanerConstants.CONVERT_IMAGE_TO_IMG);
        cleaner.registerCleaner(CleanerConstants.FIX_IMAGE_HREF);
        cleaner.registerCleaner(CleanerConstants.SPLIT_PAGE_AT_IMAGE);
        cleaner.registerCleaner(CleanerConstants.SET_CHAPTER_PAGE_TITLES);
        cleaner.registerCleaner(CleanerConstants.NO_INDENT_FIRST_PAGE);
        cleaner.registerCleaner(CleanerConstants.HEADERS);
        cleaner.registerCleaner(CleanerConstants.CONVERT_STYLES_TO_RULES);

        cleaner.registerCleaner(CleanerConstants.REMOVE_NON_EXISTENT_CALLS);
    }
}