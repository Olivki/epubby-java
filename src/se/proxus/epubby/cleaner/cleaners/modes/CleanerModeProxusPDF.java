package se.proxus.epubby.cleaner.cleaners.modes;

import se.proxus.epubby.cleaner.Cleaner;
import se.proxus.epubby.cleaner.CleanerConstants;
import se.proxus.epubby.cleaner.ICleanerMode;


public final class CleanerModeProxusPDF implements ICleanerMode {
    
    @Override
    public void setupCleaners(final Cleaner cleaner) {
        cleaner.registerCleaner(CleanerConstants.REPLACE_IMAGE_WITH_REFERENCE);
    }
}