package se.proxus.epubby.cleaner.cleaners.modes;

import se.proxus.epubby.cleaner.Cleaner;
import se.proxus.epubby.cleaner.CleanerConstants;
import se.proxus.epubby.cleaner.ICleanerMode;

public final class CleanerModeDebug implements ICleanerMode {
    
    @Override
    public void setupCleaners(final Cleaner cleaner) {
        //CleanerModeConstants.DEFAULT.setupCleaners(cleaner);
        cleaner.registerCleaner(CleanerConstants.TRIM);
        cleaner.registerCleaner(CleanerConstants.SPAN_CLUTTER);
        cleaner.registerCleaner(CleanerConstants.CONVERT_IMAGE_TO_IMG);
        cleaner.registerCleaner(CleanerConstants.FIX_FAULTY_TAGS);
        cleaner.registerCleaner(CleanerConstants.CONVERT_STYLES_TO_RULES);
        cleaner.registerCleaner(CleanerConstants.SPLIT_PAGE_AT_IMAGE);
        cleaner.registerCleaner(CleanerConstants.REMOVE_UNNECESSARY_CLASS);
        cleaner.registerCleaner(CleanerConstants.REMOVE_EMPTY_PARAGRAPHS);
        // strongest gamer doesn't remove empty paragraphs
    }
}