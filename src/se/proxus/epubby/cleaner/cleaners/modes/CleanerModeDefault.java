package se.proxus.epubby.cleaner.cleaners.modes;

import se.proxus.epubby.cleaner.Cleaner;
import se.proxus.epubby.cleaner.CleanerConstants;
import se.proxus.epubby.cleaner.ICleanerMode;

public final class CleanerModeDefault implements ICleanerMode {
    
    @Override
    public void setupCleaners(final Cleaner cleaner) {
        cleaner.registerCleaner(CleanerConstants.TRIM);
        cleaner.registerCleaner(CleanerConstants.REMOVE_EMPTY_PARAGRAPHS);
        cleaner.registerCleaner(CleanerConstants.SPAN_CLUTTER);
        cleaner.registerCleaner(CleanerConstants.CONVERT_IMAGE_TO_IMG);
    }
}