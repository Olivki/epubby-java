package se.proxus.epubby.cleaner.cleaners.modes;

import se.proxus.epubby.cleaner.Cleaner;
import se.proxus.epubby.cleaner.ICleanerMode;

public final class CleanerModeNone implements ICleanerMode {
    
    @Override
    public void setupCleaners(final Cleaner cleaner) {

    }
}