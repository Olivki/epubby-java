package se.proxus.epubby.cleaner.cleaners.modes;

import se.proxus.epubby.cleaner.Cleaner;
import se.proxus.epubby.cleaner.CleanerConstants;
import se.proxus.epubby.cleaner.CleanerModeConstants;
import se.proxus.epubby.cleaner.ICleanerMode;

// Really not recommended to ever use this, lots of clashing might happen.
public final class CleanerModeAll implements ICleanerMode {
    
    @Override
    public void setupCleaners(final Cleaner cleaner) {
        CleanerModeConstants.DEFAULT.setupCleaners(cleaner);

        cleaner.registerCleaner(CleanerConstants.FIX_FAULTY_TAGS);
        cleaner.registerCleaner(CleanerConstants.AGGRESSIVE);
        cleaner.registerCleaner(CleanerConstants.REMOVE_HEAD_STYLE);
        cleaner.registerCleaner(CleanerConstants.REMOVE_HEAD_SCRIPT);
        cleaner.registerCleaner(CleanerConstants.CONVERT_STYLES_TO_RULES);
        cleaner.registerCleaner(CleanerConstants.SPLIT_PAGE_AT_IMAGE);
        cleaner.registerCleaner(CleanerConstants.REMOVE_UNNECESSARY_CLASS);
    }
}