package se.proxus.epubby.cleaner.cleaners.types.misc;

import org.jsoup.nodes.Document;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;
import se.proxus.epubby.resources.stylesheet.StyleSheetReader;

import java.util.LinkedList;
import java.util.List;

public final class CleanerRemoveUnusedRules implements ICleaner {
    
    @Override
    public ICleaner init(final Book book) {
        for (final StyleSheetReader reader : book.getStyleSheets().getReaders()) {
            final List<StyleSheetReader.Rule> rulesToRemove = new LinkedList<>();

            for (final StyleSheetReader.Rule rule : reader.getRules()) {
                if (!rule.getName().startsWith(".")) {
                    continue;
                }

                final long occurrences = book.getPages().getOccurrencesOfClass(rule.getClassName());

                if (occurrences <= 0) {
                    rulesToRemove.add(rule);
                }
            }

            if (rulesToRemove.size() > 0) {
                reader.getRules().removeAll(rulesToRemove);
            }
        }

        return this;
    }

    @Override
    public void cleanPage(final Book book, final Page page, final Document document) {}
}