package se.proxus.epubby.cleaner.cleaners.types.misc;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;
import se.proxus.epubby.resources.stylesheet.StyleSheetReader;
import se.proxus.epubby.tools.JsoupTools;

public final class CleanerRemoveUnnecessaryClass implements ICleaner {

    private String frequentClassName;
    
    @Override
    public ICleaner init(final Book book) {
        return this;
    }

    @Override
    public void cleanPage(final Book book, final Page page, final Document document) {
        for (final Element pElement : document.body().select("p")) {
            if (!JsoupTools.hasClass(pElement)) {
                continue;
            }

            String className = pElement.className();
            final long occurrences = book.getPages().getOccurrenceOfClass("p", className);
            final long amount = book.getPages().getNumberOfElements("p");
            final int percent = (int) ((occurrences * 100) / amount);

            if (percent <= 50) {
                continue;
            }

            JsoupTools.forceRemoveClass(pElement);

            if (page.getLastReaderWithRule(className) == null) {
                className = "p." + className;
            }

            final StyleSheetReader reader = page.getLastReaderWithRule(className);

            final StyleSheetReader.Rule rule = reader.getRule(className);

            if (rule == null) {
                book.getLog().fatal("RuleNull [" + reader.getResource().getHref() + ", " + className + "]");
                break;
            }

            if (!reader.containsRuleName("p")) {
                final StyleSheetReader.Rule pRule = reader.addRule("p");

                pRule.getContent().addAll(rule.getContent());
            }
        }
    }

    public String getFrequentClassName() {
        return frequentClassName;
    }

    public void setFrequentClassName(final String frequentClassName) {
        this.frequentClassName = frequentClassName;
    }
}
