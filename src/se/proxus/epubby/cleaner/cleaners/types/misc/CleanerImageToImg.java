package se.proxus.epubby.cleaner.cleaners.types.misc;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;
import se.proxus.epubby.tools.JsoupTools;

public final class CleanerImageToImg implements ICleaner {
    
    @Override
    public ICleaner init(final Book book) {
        return this;
    }

    @Override
    public void cleanPage(Book book, Page page, Document document) {
        for (final Element imageElement : document.select("image")) {
            final Element svgElement = imageElement.parent();

            String imageHref = imageElement.attr("xlink:href");

            if (svgElement.is("svg") || svgElement.hasClass("svg_outer svg_inner")) {
                imageElement.remove();

                if (!svgElement.select("desc").isEmpty()) {
                    svgElement.select("desc").remove();
                }

                if (svgElement.hasClass("svg_outer svg_inner")) {
                    JsoupTools.forceRemoveClass(svgElement);
                }

                svgElement.append("<img alt=\"\" src=\"" + imageHref + "\"/>");
                svgElement.unwrap();
            } else {
                imageElement.remove();
                svgElement.append("<img alt=\"\" src=\"" + imageHref + "\"/>");
            }
        }
    }
}