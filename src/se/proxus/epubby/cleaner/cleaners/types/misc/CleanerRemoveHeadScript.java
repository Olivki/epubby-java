package se.proxus.epubby.cleaner.cleaners.types.misc;

import org.jsoup.nodes.Document;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;

public final class CleanerRemoveHeadScript implements ICleaner {
    
    @Override
    public ICleaner init(final Book book) {
        return this;
    }

    @Override
    public void cleanPage(final Book book, final Page page, final Document document) {
        document.head().select("script").remove();
    }
}