package se.proxus.epubby.cleaner.cleaners.types.misc;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;

public final class CleanerRemoveComments implements ICleaner {
    
    @Override
    public ICleaner init(final Book book) {
        return this;
    }

    @Override
    public void cleanPage(final Book book, final Page page, final Document document) {
        removeComments(document.body());
    }

    // https://stackoverflow.com/questions/7541843/how-to-search-for-comments-using-jsoup
    private void removeComments(final Node node) {
        for (int index = 0; index < node.childNodeSize(); ) {
            final Node child = node.childNode(index);

            if (child.nodeName().equals("#comment")) {
                child.remove();
            } else {
                removeComments(child);
                index++;
            }
        }
    }
}