package se.proxus.epubby.cleaner.cleaners.types.misc;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;
import se.proxus.epubby.resources.types.ImageResource;
import se.proxus.epubby.resources.stylesheet.StyleSheetReader;
import se.proxus.epubby.tools.ImageTools;
import se.proxus.epubby.tools.JsoupTools;

public final class CleanerAddImgClass implements ICleaner {
    
    @Override
    public ICleaner init(final Book book) {
        return this;
    }

    @Override
    public void cleanPage(Book book, Page page, Document document) {
        for (final Element imgElement : document.select("img")) {
            final String href = imgElement.select("img").attr("src");
            final ImageResource imageResource =
                    (ImageResource) book.getResources().getResourceByHref(href);

            if (imageResource == null) {
                book.getLog().fatal("ImageHrefNull [" + page.getHref() + ", " + href + "]");
                continue;
            }

            if (!ImageTools.isSameOrLarger(imageResource.getImage(),
                                           book.getCleaner().getSettings().getImageWidth(),
                                           book.getCleaner().getSettings().getImageHeight())) {
                continue;
            }

            JsoupTools.replaceClass(imgElement, "insert", true);

            checkAvailability("img.insert", book, page);
        }
    }

    private void checkAvailability(final String ruleName, final Book book, final Page page) {
        if (!page.ruleExists(ruleName)) {
            if (book.getStyleSheets().hasMainReader()) {
                final StyleSheetReader reader = book.getStyleSheets().getMainReader();

                addRule(ruleName, reader);
            } else {
                final StyleSheetReader reader = page.getStyleSheets().get(0);

                addRule(ruleName, reader);
            }
        }
    }

    private void addRule(String ruleName, final StyleSheetReader reader) {
        final StyleSheetReader.Rule rule = reader.addRule(ruleName);

        rule.add("display: block;");
        rule.add("max-width: 100%;");
        rule.add("max-height: 100%;");
        rule.add("height:auto;");
        rule.add("width:auto;");
        rule.add("vertical-align:middle;");
        rule.add("text-align: center;");
        rule.add("text-indent: 0em;");
    }
}