package se.proxus.epubby.cleaner.cleaners.types.misc;

import org.jsoup.nodes.Document;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;

public final class CleanerRemoveFakeTocPages implements ICleaner {
    
    @Override
    public ICleaner init(final Book book) {
        return this;
    }

    @Override
    public void cleanPage(final Book book, final Page page, final Document document) {
        if (page.getHref()
                .equalsIgnoreCase(book.getTableOfContents().getTableOfContentsPage().getPage().getHref())) {
            return;
        }

        int matches = 0;
        boolean isToCPage = false;

        isToCPage = document.body().getElementsContainingText("Contents").size() > 0;

        matches += document.body().select("ol").size();

        matches += document.body().select("li").size();

        if (isToCPage && matches >= 2) {
            book.getResources().removeResource(page.getResource());
        }
    }
}
