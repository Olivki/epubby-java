package se.proxus.epubby.cleaner.cleaners.types.utility;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;

public final class CleanerEmptyParagraphs implements ICleaner {
    
    @Override
    public ICleaner init(final Book book) {
        return this;
    }

    @Override
    public void cleanPage(final Book book, final Page page, final Document document) {
        for (final Element pElement : document.select("p")) {
            boolean isNotSpan = false;

            if (pElement.children().size() > 0) {
                for (final Element childElement : pElement.getAllElements()) {
                    if (childElement.is("p")) {
                        continue;
                    }

                    if (!childElement.is("span")) {
                        isNotSpan = true;
                        break;
                    } else {
                        if (childElement.text().trim().isEmpty()) {
                            isNotSpan = true;
                            break;
                        }
                    }
                }
            } else if (pElement.text().trim().isEmpty()) {
                pElement.remove();
                continue;
            }

            if (isNotSpan) {
                continue;
            }
        }
    }
}