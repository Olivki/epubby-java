package se.proxus.epubby.cleaner.cleaners.types.utility;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;
import se.proxus.epubby.tools.StringTools;

public final class CleanerHeadStyleSheet implements ICleaner {
    
    @Override
    public ICleaner init(final Book book) {
        return this;
    }

    @Override
    public void cleanPage(final Book book, final Page page, final Document document) {
        for (final Element styleSheetElement : document.select("link[rel=stylesheet]")) {
            page.removeStyleSheet(StringTools.href(styleSheetElement.attr("href")));
            styleSheetElement.remove();
        }
    }
}