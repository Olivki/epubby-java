package se.proxus.epubby.cleaner.cleaners.types.utility;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;
import se.proxus.epubby.tools.JsoupTools;

public final class CleanerImgUnwrapper implements ICleaner {
    
    @Override
    public ICleaner init(final Book book) {
        return this;
    }

    @Override
    public void cleanPage(final Book book, final Page page, final Document document) {
        if (JsoupTools.exists(document.body().select("img"))) {
            for (Element imgElement : document.body().select("img")) {
                final Element parent = imgElement.parent();

                if (parent != null) {
                    if (parent.is("div")) {
                        if (parent.attr("style").trim().isEmpty() && parent.attr("class").trim().isEmpty()) {
                            parent.unwrap();
                        }
                    }
                }
            }
        }
    }
}