package se.proxus.epubby.cleaner.cleaners.types.utility;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;
import se.proxus.epubby.resources.stylesheet.StyleSheetReader;
import se.proxus.epubby.tools.JsoupTools;

import java.util.LinkedList;
import java.util.List;

public final class CleanerConvertStyles implements ICleaner {

    private final List<String> styleAttributes = new LinkedList<>();
    private StyleSheetReader reader;
    
    @Override
    public ICleaner init(final Book book) {
        getStyleAttributes().clear();
        final List<String> tempList = book.getPages().getAllStyleAttributes();
        setReader(book.getStyleSheets().getAutomaticReader());

        for (final String content : tempList) {
            final long occurrences = book.getPages().getOccurrencesOfAttribute("style", content);

            if (occurrences <= 1) {
                continue;
            }

            final StyleSheetReader.Rule rule = getReader().addAutomaticRule();

            if (!content.contains(";")) {
                rule.add(content.trim() + ";");
                continue;
            }

            for (final String ruleContent : content.split(";")) {
                rule.add(ruleContent.trim() + ";");
            }

            getStyleAttributes().add(content);
        }

        return this;
    }

    @Override
    public void cleanPage(final Book book, final Page page, final Document document) {
        if (getStyleAttributes().size() <= 0) {
            return;
        }

        page.addStyleSheet(getReader());

        for (final Element element : document.getElementsByAttribute("style")) {
            final String styleContent = element.attr("style").trim();

            if (getStyleAttributes().contains(styleContent)) {
                final String className =
                        getReader().getRules().get(getStyleAttributes().indexOf(styleContent)).getClassName();
                element.removeAttr("style");

                if (JsoupTools.hasClass(element)) {
                    JsoupTools.replaceClass(element, className, true);
                } else {
                    element.addClass(className);
                }
            }
        }
    }

    public StyleSheetReader getReader() {
        return reader;
    }

    public void setReader(final StyleSheetReader reader) {
        this.reader = reader;
    }

    public List<String> getStyleAttributes() {
        return styleAttributes;
    }
}
