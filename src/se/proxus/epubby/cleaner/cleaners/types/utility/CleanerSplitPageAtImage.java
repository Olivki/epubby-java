package se.proxus.epubby.cleaner.cleaners.types.utility;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;
import se.proxus.epubby.resources.types.ImageResource;
import se.proxus.epubby.resources.types.PageResource;
import se.proxus.epubby.resources.toc.TableOfContentsEntry;
import se.proxus.epubby.tools.FileTools;
import se.proxus.epubby.tools.ImageTools;
import se.proxus.epubby.tools.JsoupTools;
import se.proxus.epubby.tools.StringTools;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public final class CleanerSplitPageAtImage implements ICleaner {

    private int pageIndex = 0;
    
    @Override
    public ICleaner init(final Book book) {
        return this;
    }

    @Override
    public void cleanPage(final Book book, final Page page, final Document document) {
        boolean isImagePage = false;
        pageIndex = 0;

        final boolean imgPage = document.select("img").size() > 0;
        final boolean imagePage = document.select("image").size() > 0;

        if (page.isImagePage()) {
            if (!imgPage) {
                return;
            } else {
                isImagePage = true;
            }
        }

        int imagePageType = -1;

        if (imgPage) {
            imagePageType = 1;
        } else if (imagePage) {
            imagePageType = 2;
        }

        //System.out.println(page.getHref() + " image page type: " + imagePageType);

        if (document.body().children().size() <= 1) {
            return;
        }

        if (isImagePage) {
            handleImagePage(book, page, document, imagePageType);
        } else {
            handleTextAndImagePage(book, page, document);
        }
    }

    private void handleImagePage(final Book book, final Page page, final Document document,
            final int imageType) {
        final List<Element> imagesToAdd = new LinkedList<>();

        switch (imageType) {
            case 1:
                getImages(imagesToAdd, book, document, "img", "src");
                break;

            case 2:
                getImages(imagesToAdd, book, document, "image", "xlink:href");
                break;

            case 3:
                getImages(imagesToAdd, book, document, "img", "src");
                getImages(imagesToAdd, book, document, "image", "xlink:href");
                break;
        }


        document.body().children().remove();

        final String shell = document.outerHtml();

        Page pageBefore = null;

        try {
            pageBefore = createSplitPage(book, page, imagesToAdd.get(0), shell, page, true);
        } catch (final IOException e) {
            e.printStackTrace();
        }

        if (book.getTableOfContents().hasEntry(page.getResource())) {
            final TableOfContentsEntry tocEntry = book.getTableOfContents().getEntry(page.getResource());
            assert pageBefore != null;
            tocEntry.setResource(pageBefore.getResource());
        }

        for (int index = 1; index < imagesToAdd.size(); index++) {
            final Element imgElement = imagesToAdd.get(index);

            try {
                pageBefore = createSplitPage(book, page, imgElement, shell, pageBefore, true);
            } catch (final IOException e) {
                e.printStackTrace();
            }
        }

        book.getResources().removeResource(page.getResource());
    }

    private void handleTextAndImagePage(final Book book, final Page page, final Document document) {
        final List<Element> elementsBefore = new LinkedList<>();
        final List<Element> elementsAfter = new LinkedList<>();
        final List<Element> elementsToAdd = new LinkedList<>();

        int imageIndex = -1;

        for (int index = 0; index < document.body().children().size(); index++) {
            final Element element = document.body().child(index);

            if (JsoupTools.contains(element, "img")) {
                final String href = element.select("img").attr("src");
                final ImageResource imageResource =
                        (ImageResource) book.getResources().getResourceByHref(href);

                // This thing should make it so it won't create new pages just because it found a split
                // marker or something similar in the text.
                if (!ImageTools.isSameOrLarger(imageResource.getImage(),
                                               book.getCleaner().getSettings().getImageWidth(),
                                               book.getCleaner().getSettings().getImageHeight())) {
                    continue;
                }

                imageIndex = index;
                elementsToAdd.add(element);
                break;
            }


            elementsBefore.add(element);
        }

        if (imageIndex == -1) {
            return;
        }

        for (int index = (imageIndex + 1); index < document.body().children().size(); index++) {
            final Element element = document.body().child(index);

            elementsAfter.add(element);
        }

        document.body().children().remove();

        final String shell = document.outerHtml();

        for (final Element element : elementsBefore) {
            document.body().appendChild(element);
        }

        try {
            final File resourceFile = page.getResource().getFile();
            final File directory = resourceFile.getParentFile();
            final String extension = "." + FileTools.getExtension(resourceFile).toLowerCase();
            String pageName = FileTools.getNameWithoutExtension(resourceFile);

            if (!endsWithPad(pageName)) {
                for (int numberAppend = 1; numberAppend < 1000; numberAppend++) {
                    final String tempName;

                    tempName = pageName + "_" + StringTools.pad(numberAppend);

                    if (!new File(directory, tempName + extension).exists()) {
                        pageName = tempName;
                        break;
                    }
                }
            }

            page.getResource().renameResourceFile(pageName);

            final Page imagePage = createSplitPage(book, page, elementsToAdd, shell, page, true);

            if (!elementsAfter.isEmpty()) {
                final Page pageAfter = createSplitPage(book, page, elementsAfter, shell, imagePage, false);

                if (pageAfter != null) {
                    book.getPages().updateHrefReferences(page.getResource().getLocationHref(),
                            pageAfter.getResource());
                    book.getCleaner().cleanPageSpecial(pageAfter);
                }
            }
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    private void getImages(final List<Element> imagesToAdd, final Book book, final Document document,
            final String selector, final String attribute) {
        for (int index = 0; index < document.body().children().size(); index++) {
            final Element element = document.body().child(index);

            if (JsoupTools.contains(element, selector)) {
                final String href = element.select(selector).attr(attribute);
                final ImageResource imageResource =
                        (ImageResource) book.getResources().getResourceByHref(href);

                if (!ImageTools.isSameOrLarger(imageResource.getImage(),
                                               book.getCleaner().getSettings().getImageWidth(),
                                               book.getCleaner().getSettings().getImageHeight())) {
                    continue;
                }

                imagesToAdd.add(element);
            }
        }
    }

    private Page createSplitPage(final Book book, final Page page, final Element element,
            final String shell, final Page indexPage, final boolean imagePage) throws IOException {
        final List<Element> tempList = new LinkedList<>();

        tempList.add(element);

        return createSplitPage(book, page, tempList, shell, indexPage, imagePage);
    }

    private Page createSplitPage(final Book book, final Page page, final List<Element> elements,
            final String shell, final Page indexPage, final boolean imagePage) throws IOException {
        final File resourceFile = page.getResource().getFile();
        final File directory = resourceFile.getParentFile();
        String pageName = FileTools.getNameWithoutExtension(resourceFile);
        final String extension = "." + FileTools.getExtension(resourceFile).toLowerCase();

        final Document document = Jsoup.parse(shell, "", Parser.xmlParser());

        for (final Element element : elements) {
            document.body().appendChild(element);
        }

        if (endsWithPad(pageName)) {
            pageName = pageName.replaceAll("[_][0-9][0-9][0-9]*$", "");
        }

        for (int numberAppend = 1; numberAppend < 1000; numberAppend++) {
            final String tempName;

            if (imagePage && !StringUtils.containsIgnoreCase(pageName, "Illustration")) {
                if (StringTools.startsWithUppercase(pageName)) {
                    tempName = pageName + "_Illustration_" + StringTools.pad(numberAppend);
                } else {
                    tempName = pageName + "_illustration_" + StringTools.pad(numberAppend);
                }
            } else {
                tempName = pageName + "_" + StringTools.pad(numberAppend);
            }

            if (!new File(directory, tempName + extension).exists()) {
                pageName = tempName;
                break;
            }
        }

        final File newPageFile = new File(directory, pageName + extension);

        newPageFile.createNewFile();

        if (document.body().html().isEmpty()) {
            return null;
        }

        FileUtils.write(newPageFile, document.outerHtml(), "UTF-8");

        final PageResource pageResource = (PageResource) book.getResources().addResource(newPageFile);

        final int index = book.getPages().getLoadedPages().indexOf(indexPage) + 1;

        final Page newPage = book.getPages().addPage(pageResource, index, true);

        pageResource.setPage(newPage);

        if (book.getTableOfContents().hasEntry(page.getResource())) {
            book.getTableOfContents().getEntry(page.getResource()).correctPageLinks(pageResource);
        }

        return newPage;
    }

    private boolean endsWithPad(final String string) {
        return string.matches("^.+?[_][0-9][0-9]$");
    }
}