package se.proxus.epubby.cleaner.cleaners.types.utility;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;
import se.proxus.epubby.resources.stylesheet.StyleSheetReader;
import se.proxus.epubby.tools.JsoupTools;

public final class CleanerBoldAndItalic implements ICleaner {
    
    @Override
    public ICleaner init(final Book book) {
        return this;
    }

    @Override
    public void cleanPage(final Book book, final Page page, final Document document) {
        transformElement(book, page, document, "i", "italic");
        transformElement(book, page, document, "em", "italic");
        checkForStyles(document, "font-style: italic;", "italic");
        checkAvailability("italic", book, page);

        transformElement(book, page, document, "b", "bold");
        transformElement(book, page, document, "strong", "bold");
        checkForStyles(document, "font-weight: bold;", "bold");
        checkAvailability("bold", book, page);
    }

    private void checkForStyles(final Document document, final String style, final String className) {
        for (final Element styleElement : document.body().getElementsByAttribute("style")) {
            final String styleStored = styleElement.attr("style").trim();

            if (styleStored.equalsIgnoreCase(style)) {
                styleElement.removeAttr("style");
                JsoupTools.replaceClass(styleElement, className, true);
            }
        }
    }

    private void transformElement(final Book book, final Page page, final Document document,
            final String selector, final String className) {
        if (JsoupTools.exists(document.body().select(selector))) {
            for (final Element strongElement : document.body().select(selector)) {
                if (strongElement.parent().is("p")) {
                    final Element spanElement = document.createElement("span").addClass(className);

                    strongElement.before(spanElement);
                    spanElement.appendChild(strongElement);
                    strongElement.unwrap();
                } else {
                    final Element spanElement = document.createElement("p").addClass(className);

                    strongElement.before(spanElement);
                    spanElement.appendChild(strongElement);
                    strongElement.unwrap();
                }
            }
        }
    }

    private void checkAvailability(final String ruleName, final Book book, final Page page) {
        if (!page.ruleExists(ruleName)) {
            if (book.getStyleSheets().hasMainReader()) {
                final StyleSheetReader reader = book.getStyleSheets().getMainReader();

                addRule(ruleName, reader);
            } else {
                final StyleSheetReader reader = page.getStyleSheets().get(0);

                addRule(ruleName, reader);
            }
        }
    }

    private void addRule(String ruleName, final StyleSheetReader reader) {
        if (!ruleName.startsWith(".")) {
            ruleName = "." + ruleName;
        }

        final StyleSheetReader.Rule rule = reader.addRule(ruleName);

        if (ruleName.equalsIgnoreCase("italic")) {
            rule.add("font-style: italic;");
        } else if (ruleName.equalsIgnoreCase("bold")) {
            rule.add("font-weight: bold;");
        }
    }
}