package se.proxus.epubby.cleaner.cleaners.types.cosmetic;

import com.google.common.base.CharMatcher;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;
import se.proxus.epubby.tools.JsoupTools;
import se.proxus.epubby.tools.RomanNumeral;

public final class CleanerHeadersConvertArabicNumeralToRomanNumeral implements ICleaner {

    private int max = 6;
    
    @Override
    public ICleaner init(final Book book) {
        return this;
    }

    @Override
    public void cleanPage(final Book book, final Page page, final Document document) {
        for (int index = 1; index < max; index++) {
            if (JsoupTools.exists(document.body().select("h" + index))) {
                setupHeader(book, page, "h" + index, document);
            }
        }
    }

    // Remember to make the stylesheet merging thing
    private void setupHeader(final Book book, final Page page, final String selector,
            final Document document) {
        final Element header = document.selectFirst(selector);
        final String headerText = header.text();

        final CharMatcher matcher = CharMatcher.inRange('0', '9').precomputed();
        final String numberOutput = matcher.retainFrom(headerText);

        if (numberOutput.isEmpty()) {
            return;
        }

        final int number = Integer.parseInt(numberOutput);
        final String numberWord = RomanNumeral.toRoman(number);
        final String replacedName = headerText.replaceAll("[0-9]+", numberWord);

        header.text(replacedName);

        if (book.getTableOfContents().hasEntry(page.getResource())) {
            book.getTableOfContents().getEntry(page.getResource()).setTitle(replacedName);
        }
    }

    public int getMax() {
        return max;
    }

    public void setMax(final int max) {
        this.max = max;
    }
}
