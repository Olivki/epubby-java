package se.proxus.epubby.cleaner.cleaners.types.cosmetic;

import org.jsoup.nodes.Document;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;
import se.proxus.epubby.resources.toc.TableOfContentsEntry;

public final class CleanerSetChapterPageTitles implements ICleaner {
    
    @Override
    public ICleaner init(final Book book) {
        return this;
    }

    @Override
    public void cleanPage(final Book book, final Page page, final Document document) {
        if (book.getTableOfContents().hasEntry(page.getResource())) {
            final TableOfContentsEntry tocEntry = book.getTableOfContents().getEntry(page.getResource());

            document.title(book.getContent().getMetaData().getTitle() + " - " + tocEntry.getTitle());
        }
    }
}
