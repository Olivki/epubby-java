package se.proxus.epubby.cleaner.cleaners.types.cosmetic;

import org.jsoup.nodes.Document;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;
import se.proxus.epubby.tools.StringTools;

public final class CleanerPageFileNameTrimmer implements ICleaner {
    
    @Override
    public ICleaner init(final Book book) {
        return this;
    }

    @Override
    public void cleanPage(final Book book, final Page page, final Document document) {
        if (StringTools.startsWithPad(page.getResource().getName())) {
            final String name = page.getResource().getName();
            //System.out.println(name.replaceAll("^[0-9]*[_]", ""));
            page.getResource().renameResourceFile(name.replaceAll("^[0-9]*[_]", ""));
        }
    }
}