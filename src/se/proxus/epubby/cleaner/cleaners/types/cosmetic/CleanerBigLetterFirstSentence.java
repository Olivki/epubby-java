package se.proxus.epubby.cleaner.cleaners.types.cosmetic;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;
import se.proxus.epubby.resources.stylesheet.StyleSheetReader;
import se.proxus.epubby.tools.JsoupTools;
import se.proxus.epubby.tools.StringTools;

public final class CleanerBigLetterFirstSentence implements ICleaner {
    
    @Override
    public ICleaner init(final Book book) {
        return this;
    }

    @Override
    public void cleanPage(final Book book, final Page page, final Document document) {
        if (JsoupTools.exists(document.body().select("p"))) {
            final Element pElement = document.body().selectFirst("p");
            final String pText = pElement.text();
            final int letterIndex = StringTools.findFirstLetterPosition(pText);

            if (letterIndex == -1) {
                return;
            }

            final char letter = pText.charAt(letterIndex);
            final String sorround = "<span class=\"bigLetter\">" + letter + "</span>";

            pElement.text(StringTools.sorroundCharAt(pText, letterIndex, sorround));
            checkAvailability("bigLetter", book, page);
        }
    }

    private void checkAvailability(final String ruleName, final Book book, final Page page) {
        if (!page.ruleExists(ruleName)) {
            if (book.getStyleSheets().hasMainReader()) {
                final StyleSheetReader reader = book.getStyleSheets().getMainReader();

                addRule(ruleName, reader);
            } else {
                final StyleSheetReader reader = page.getStyleSheets().get(0);

                addRule(ruleName, reader);
            }
        }
    }

    private void addRule(String ruleName, final StyleSheetReader reader) {
        if (!ruleName.startsWith(".")) {
            ruleName = "." + ruleName;
        }

        final StyleSheetReader.Rule rule = reader.addRule(ruleName);

        rule.add("font-size: 1.8em;");
        rule.add("letter-spacing: .5px;");
    }
}