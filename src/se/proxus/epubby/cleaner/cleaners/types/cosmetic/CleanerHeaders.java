package se.proxus.epubby.cleaner.cleaners.types.cosmetic;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;
import se.proxus.epubby.tools.JsoupTools;

public final class CleanerHeaders implements ICleaner {
    
    @Override
    public ICleaner init(final Book book) {
        return this;
    }

    @Override
    public void cleanPage(final Book book, final Page page, final Document document) {
        for (int index = 1; index < 6; index++) {
            if (JsoupTools.exists(document.body().select("h" + index))) {
                setupHeader("h" + index, document);
            }
        }
    }

    private void setupHeader(final String selector, final Document document) {
        final Element header = document.selectFirst(selector);
        final String headerFullTitle = header.wholeText();
        JsoupTools.replaceClass(header, "chapter-number", true);

        if (headerFullTitle.indexOf(":") > 0) {
            //System.out.println("Chapter title using \":\" detected.");
            final String headerTitle = headerFullTitle.split(":")[0].trim();
            final String headerSubtitle = headerFullTitle.split(":")[1].trim();

            final Element headerSubtitleElement = document.createElement("h1");

            headerSubtitleElement.addClass("chapter-title");
            headerSubtitleElement.appendText(headerSubtitle);

            header.html(headerTitle);
            headerSubtitleElement.appendTo(header);
        } else if (headerFullTitle.indexOf("-") > 0) {
            //System.out.println("Chapter title using \"-\" detected.");
            final String headerTitle = headerFullTitle.split("-")[0].trim();
            final String headerSubtitle = headerFullTitle.split("-")[1].trim();

            final Element headerSubtitleElement = document.createElement("h1");

            headerSubtitleElement.addClass("chapter-title");
            headerSubtitleElement.appendText(headerSubtitle);

            header.html(headerTitle);
            headerSubtitleElement.appendTo(header);
        } else if (headerFullTitle.indexOf("|") > 0) {
            final String headerTitle = headerFullTitle.split("\\|")[0].trim();
            final String headerSubtitle = headerFullTitle.split("\\|")[1].trim();

            final Element headerSubtitleElement = document.createElement("h1");

            headerSubtitleElement.addClass("chapter-title");
            headerSubtitleElement.appendText(headerSubtitle);

            header.html(headerTitle);
            headerSubtitleElement.appendTo(header);
        } else if (document.select(selector + ".chapter-title").isEmpty()) {
            JsoupTools.replaceClass(header, "chapter-number1", true);
        }
    }
}
