package se.proxus.epubby.cleaner.cleaners.types.cosmetic;

import org.jsoup.nodes.Document;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;
import se.proxus.epubby.tools.FileTools;

public final class CleanerBookTitleFromFile implements ICleaner {
    
    @Override
    public ICleaner init(final Book book) {
        book.getContent().getMetaData().setTitle(FileTools.getNameWithoutExtension(book.getFile()));
        return this;
    }

    @Override
    public void cleanPage(Book book, Page page, Document document) {}
}