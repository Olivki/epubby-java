package se.proxus.epubby.cleaner.cleaners.types.cosmetic;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;
import se.proxus.epubby.resources.toc.TableOfContentsEntry;
import se.proxus.epubby.tools.JsoupTools;
import se.proxus.epubby.tools.Pair;

import java.util.LinkedList;
import java.util.List;

public final class CleanerReplaceName implements ICleaner {

    private final List<Pair<String, String>> replacements = new LinkedList<>();
    
    @Override
    public ICleaner init(final Book book) {
        return this;
    }

    @Override
    public void cleanPage(final Book book, final Page page, final Document document) {
        for (final Pair<String, String> replacement : replacements) {
            final String matchName = replacement.getKey();
            final String replacementName = replacement.getValue();
            final String fileMatchName = matchName.replace(" ", "_");

            if (book.getTableOfContents().hasEntry(matchName)) {
                final TableOfContentsEntry entry = book.getTableOfContents().getEntry(matchName);

                entry.setTitle(replacementName);
            }

            if (StringUtils
                    .containsIgnoreCase(page.getResource().getName(), fileMatchName)) {
                page.getResource().renameResourceFile(replacementName.replace(" ", "_").replace("'", ""));
            }

            for (int index = 1; index < 6; index++) {
                if (JsoupTools.exists(document.body().select("h" + index))) {
                    final Element firstHeader = document.body().selectFirst("h" + 1);
                    final String headerText = firstHeader.text();

                    if (headerText.equalsIgnoreCase(matchName)) {
                        firstHeader.text(replacementName);
                    }
                }
            }
        }
    }

    public void addReplacement(final String originalName, final String replacementName) {
        replacements.add(new Pair<>(originalName, replacementName));
    }
}