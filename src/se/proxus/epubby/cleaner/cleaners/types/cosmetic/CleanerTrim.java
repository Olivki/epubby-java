package se.proxus.epubby.cleaner.cleaners.types.cosmetic;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;

public final class CleanerTrim implements ICleaner {
    
    @Override
    public ICleaner init(final Book book) {
        return this;
    }

    @Override
    public void cleanPage(final Book book, final Page page, final Document document) {
        for (Element childElement : document.getAllElements()) {
            if (childElement.hasText()) {
                if (childElement.children().size() > 0) {
                    final int max = childElement.children().size() - 1;
                    final Element first = childElement.child(0);
                    final Element last = childElement.child(max);

                    if (first.hasText() && !first.textNodes().isEmpty()) {
                        final TextNode textNode = first.textNodes().get(0);
                        String text = textNode.text();

                        text = StringUtils.stripStart(text, " ");

                        textNode.text(text);
                    }

                    if (last.hasText() && !last.textNodes().isEmpty()) {
                        final TextNode textNode = last.textNodes().get(0);
                        String text = textNode.text();

                        text = StringUtils.stripEnd(text, " ");

                        textNode.text(text);
                    }
                }
            }
        }
    }
}