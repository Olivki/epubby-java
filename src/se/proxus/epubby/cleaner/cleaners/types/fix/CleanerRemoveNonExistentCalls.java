package se.proxus.epubby.cleaner.cleaners.types.fix;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;
import se.proxus.epubby.tools.JsoupTools;

public final class CleanerRemoveNonExistentCalls implements ICleaner {
    
    @Override
    public ICleaner init(final Book book) {
        return this;
    }

    @Override
    public void cleanPage(final Book book, final Page page, final Document document) {
        for (final Element hrefElement : document.getElementsByAttribute("href")) {
            String href = hrefElement.attr("href");

            if (href.contains("#")) {
                href = href.split("#")[0];
            }

            if (!book.getResources().exists(href)) {
                hrefElement.unwrap();
            }
        }

        for (final Element classElement : document.getElementsByAttribute("class")) {
            final String className = classElement.attr("class");

            if (!book.getStyleSheets().ruleExists(className)) {
                if (book.getStyleSheets().ruleExists(classElement.tagName() + "." + className)) {
                    continue;
                }
                
                JsoupTools.forceRemoveClass(classElement);
            }
        }
    }
}