package se.proxus.epubby.cleaner.cleaners.types.fix;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;

public final class CleanerImageHref implements ICleaner {
    
    @Override
    public ICleaner init(final Book book) {
        return this;
    }

    @Override
    public void cleanPage(final Book book, final Page page, final Document document) {
        for (final Element imageElement : document.select("image")) {
            String href = imageElement.attr("xlink:href").replace(".jpeg", ".jpg");

            if (StringUtils.startsWithIgnoreCase(href, "images")) {
                href = href.replace("images", "../Images");
            }

            imageElement.attr("xlink:href", href);
        }

        for (final Element imgElement : document.select("img")) {
            String href = imgElement.attr("src").replace("jpeg", "jpg");

            if (StringUtils.startsWithIgnoreCase(href, "images")) {
                href = href.replace("images", "../Images");
            }

            imgElement.attr("src", href);
        }
    }
}
