package se.proxus.epubby.cleaner.cleaners.types.fix;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;

public final class CleanerSpanClutter implements ICleaner {
    
    @Override
    public ICleaner init(final Book book) {
        return this;
    }

    @Override
    public void cleanPage(final Book book, final Page page, final Document document) {
        for (final Element spanElement : document.select("span")) {
            if (!spanElement.hasParent()) {
                continue;
            }

            if (spanElement.parent() == null || spanElement.parents() == null) {
                continue;
            }

            if (spanElement.attributes().size() <= 0) {
                spanElement.unwrap();
                continue;
            }

            if (!(spanElement.hasAttr("class") || spanElement.hasAttr("style"))) {
                spanElement.unwrap();
                continue;
            }

            if (spanElement.hasAttr("class")) {
                if (!book.getStyleSheets().ruleExists(spanElement.className())) {
                    spanElement.unwrap();
                    continue;
                }
            }
        }
    }
}