package se.proxus.epubby.cleaner.cleaners.types.fix;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;
import se.proxus.epubby.resources.types.ImageResource;
import se.proxus.epubby.tools.ImageTools;
import se.proxus.epubby.tools.StringTools;

public final class CleanerReplaceImageWithReference implements ICleaner {
    
    @Override
    public ICleaner init(final Book book) {
        return this;
    }

    @Override
    public void cleanPage(final Book book, final Page page, final Document document) {
        for (final Element imgElement : document.body().select("img")) {
            final String href = imgElement.select("img").attr("src");
            final ImageResource imageResource =
                    (ImageResource) book.getResources().getResourceByHref(href);

            if (imageResource == null) {
                book.getLog().fatal("ImageHrefNull [" + page.getHref() + ", " + href + "]");
                continue;
            }

            if (!ImageTools.isSameOrLarger(imageResource.getImage(),
                                           book.getCleaner().getSettings().getImageWidth(),
                                           book.getCleaner().getSettings().getImageHeight())) {
                continue;
            }

            String imageHref = StringTools.href(href);

            /*if (book.getPages().getLoadedPages().indexOf(page) == 0) {
                imageHref = "Cover.png";
            }*/

            imgElement.before("<p style=\"font-size: 0.7em;\">IMAGE_METADATA_(" + imageHref + ")_END</p>");

            imgElement.remove();
        }
    }
}
