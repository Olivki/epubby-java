package se.proxus.epubby.cleaner.cleaners.types.fix;

import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.ICleaner;
import se.proxus.epubby.resources.pages.Page;
import se.proxus.epubby.tools.StringTools;

public final class CleanerFaultyTags implements ICleaner {
    
    @Override
    public ICleaner init(final Book book) {
        return this;
    }

    @Override
    public void cleanPage(final Book book, final Page page, final Document document) {
        for (final Element pElement : document.select("p")) {
            cleaner(pElement.children(), pElement);
        }
    }

    private void cleaner(final Elements children, final Element parent) {
        final StringBuilder builder = new StringBuilder();

        appendFixedTagName(children, builder);

        if (!builder.toString().isEmpty()) {
            final String fixedEntry = builder.toString();
            final String faultyTagName = getTagName(fixedEntry);

            for (final Element faultyElement : parent.select(faultyTagName)) {
                faultyElement.children().remove();

                faultyElement.text(fixedEntry);
                faultyElement.unwrap();
            }

                //System.out.println(fixedEntry);

            final String[] texts = parent.text().split(fixedEntry);
            String before = texts[0];

            final String beforeText = before;
            final String lastWord = beforeText.replaceAll("^.*?(\\w+)\\W*$", "$1");
            final char lastChar = lastWord.charAt(lastWord.length() - 1);

            if (!StringTools.charIsSpecial(lastChar)) {
                before = before + " ";
            }

            String after = "";

            if (texts.length > 1) {
                after = texts[1];

                final String firstWord = StringTools.getFirstWord(after);

                if (!firstWord.isEmpty()) {
                    if (!StringTools.startsWithSpecial(firstWord)) {
                        after = " " + after;
                    }
                }
            }

            String stitchedEntry = before + fixedEntry;

            if (!after.isEmpty()) {
                stitchedEntry = stitchedEntry + after;
            }

            parent.text(stitchedEntry);
        }
    }

    private String getTagName(final String entry) {
        String result = "";

        if (entry.startsWith("[")) {
            result = entry.substring(1);
        }

        if (result.contains("]")) {
            result = result.split("\\]")[0];
        }

        if (result.contains(" ")) {
            result = result.split(" ")[0];
        }

        return result;
    }

    private void appendFixedTagName(final Elements children, final StringBuilder builder) {
        for (final Element child : children) {
            if (StringTools.startsWithUppercase(child.tagName())) {
                if (!builder.toString().isEmpty()) {
                    builder.append(" ");
                }

                builder.append("[").append(child.tagName());

                for (final Attribute attribute : child.attributes()) {
                    builder.append(" ").append(attribute.getKey());
                }

                builder.append("]");

                if (!child.ownText().isEmpty()) {
                    if (!StringTools.startsWithSpecial(child.ownText())) {
                        builder.append(" ");
                    }

                    builder.append(child.ownText());
                }
            }

            if (child.children().size() > 0) {
                appendFixedTagName(child.children(), builder);
            }
        }
    }
}
