package se.proxus.epubby.cleaner;

import lombok.experimental.UtilityClass;
import se.proxus.epubby.cleaner.cleaners.types.cosmetic.*;
import se.proxus.epubby.cleaner.cleaners.types.fix.*;
import se.proxus.epubby.cleaner.cleaners.types.misc.*;
import se.proxus.epubby.cleaner.cleaners.types.utility.*;

@UtilityClass
public final class CleanerConstants {
    
    public static final CleanerTrim TRIM = new CleanerTrim();
    
    public static final CleanerEmptyParagraphs REMOVE_EMPTY_PARAGRAPHS =
        new CleanerEmptyParagraphs();
    
    public static final CleanerFaultyTags FIX_FAULTY_TAGS = new CleanerFaultyTags();
    
    public static final CleanerRemoveHeadStyle REMOVE_HEAD_STYLE = new CleanerRemoveHeadStyle();
    
    public static final CleanerAggressive AGGRESSIVE = new CleanerAggressive();
    
    public static final CleanerRemoveHeadScript REMOVE_HEAD_SCRIPT = new CleanerRemoveHeadScript();
    
    public static final CleanerSpanClutter SPAN_CLUTTER = new CleanerSpanClutter();
    
    public static final CleanerImageHref FIX_IMAGE_HREF = new CleanerImageHref();
    
    public static final CleanerImageToImg CONVERT_IMAGE_TO_IMG = new CleanerImageToImg();
    
    public static final CleanerConvertStyles CONVERT_STYLES_TO_RULES = new CleanerConvertStyles();
    
    public static final CleanerRemoveUnnecessaryClass REMOVE_UNNECESSARY_CLASS =
        new CleanerRemoveUnnecessaryClass();
    
    public static final CleanerSplitPageAtImage SPLIT_PAGE_AT_IMAGE = new CleanerSplitPageAtImage();
    
    public static final CleanerRemoveFakeTocPages REMOVE_FAKE_TOC_PAGES =
        new CleanerRemoveFakeTocPages();
    
    public static final CleanerSetChapterPageTitles SET_CHAPTER_PAGE_TITLES =
        new CleanerSetChapterPageTitles();
    
    public static final CleanerNoIndentFirstPage NO_INDENT_FIRST_PAGE =
        new CleanerNoIndentFirstPage();
    
    public static final CleanerHeaders HEADERS = new CleanerHeaders();
    
    public static final CleanerBoldAndItalic BOLD_AND_ITALIC = new CleanerBoldAndItalic();
    
    public static final CleanerBookTitleFromFile BOOK_TITLE_FROM_FILE =
        new CleanerBookTitleFromFile();
    
    public static final CleanerImgUnwrapper IMG_UNWRAPPER = new CleanerImgUnwrapper();
    
    public static final CleanerBakaTsukiFixer BAKA_TSUKI_FIXER = new CleanerBakaTsukiFixer();
    
    public static final CleanerPageFileNameTrimmer PAGE_FILE_NAME_TRIMMER =
        new CleanerPageFileNameTrimmer();
    
    public static final CleanerHeadStyleSheet HEAD_STYLE_SHEET = new CleanerHeadStyleSheet();
    
    public static final CleanerRemoveComments REMOVE_COMMENTS = new CleanerRemoveComments();
    
    public static final CleanerBigLetterFirstSentence BIG_LETTER_FIRST_SENTENCE =
        new CleanerBigLetterFirstSentence();
    
    public static final CleanerRemoveUnusedRules REMOVE_UNUSED_RULES =
        new CleanerRemoveUnusedRules();
    
    public static final CleanerReplaceName REPLACE_NAME = new CleanerReplaceName();
    
    public static final CleanerAddImgClass ADD_IMG_CLASS = new CleanerAddImgClass();
    
    public static final CleanerReplaceImageWithReference REPLACE_IMAGE_WITH_REFERENCE =
        new CleanerReplaceImageWithReference();
    
    public static final CleanerHeadersConvertNumberToWord HEADERS_CONVERT_NUMBER_TO_WORD =
        new CleanerHeadersConvertNumberToWord();
    
    public static final CleanerHeadersConvertArabicNumeralToRomanNumeral
        CONVERT_ARABIC_NUMERAL_TO_ROMAN_NUMERAL =
        new CleanerHeadersConvertArabicNumeralToRomanNumeral();
    
    public static final CleanerRemoveNonExistentCalls REMOVE_NON_EXISTENT_CALLS =
        new CleanerRemoveNonExistentCalls();
    
}