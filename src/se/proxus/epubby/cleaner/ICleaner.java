package se.proxus.epubby.cleaner;

import org.jsoup.nodes.Document;
import se.proxus.epubby.Book;
import se.proxus.epubby.resources.pages.Page;

public interface ICleaner {

    ICleaner init(final Book book);

    void cleanPage(final Book book, final Page page, final Document document);
    
}
