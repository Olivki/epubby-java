package se.proxus.epubby.cleaner;

import lombok.Data;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.cleaners.types.cosmetic.CleanerBigLetterFirstSentence;
import se.proxus.epubby.resources.pages.Page;

import java.util.LinkedList;
import java.util.List;

@Data
public final class Cleaner {
    
    private final Book book;
    private ICleanerMode cleanerMode = CleanerModeConstants.DEFAULT;
    private final List<ICleaner> registeredCleaners = new LinkedList<>();
    private final Settings settings;
    
    public Cleaner(final Book book) {
        this.book = book;
        this.settings = new Settings(this);
    }
    
    public void registerDefaultCleaners() {
        getCleanerMode().setupCleaners(this);
    }
    
    public void init() {
        CleanerConstants.FIX_IMAGE_HREF.init(getBook());
        
        for (final ICleaner cleaner : getRegisteredCleaners()) {
            cleaner.init(getBook());
        }
    }
    
    public void cleanPage(final Page page) {
        CleanerConstants.FIX_IMAGE_HREF.cleanPage(getBook(), page, page.getDocument());
        
        for (int index = 0; index < getRegisteredCleaners().size(); index++) {
            final ICleaner cleaner = getRegisteredCleaners().get(index);
            
            cleaner.cleanPage(getBook(), page, page.getDocument());
        }
    }
    
    public void cleanPageSpecial(final Page page) {
        CleanerConstants.FIX_IMAGE_HREF.cleanPage(getBook(), page, page.getDocument());
        
        for (int index = 0; index < getRegisteredCleaners().size(); index++) {
            final ICleaner cleaner = getRegisteredCleaners().get(index);
            
            if (cleaner instanceof CleanerBigLetterFirstSentence) {
                continue;
            }
            
            cleaner.cleanPage(getBook(), page, page.getDocument());
        }
    }
    
    public ICleaner registerCleaner(final ICleaner cleaner) {
        if (!getRegisteredCleaners().contains(cleaner) &&
            cleaner != CleanerConstants.FIX_IMAGE_HREF) {
            getRegisteredCleaners().add(cleaner);
        }
        
        return cleaner;
    }
    
    public Book getBook() {
        return book;
    }
    
    public ICleanerMode getCleanerMode() {
        return cleanerMode;
    }
    
    public void setCleanerMode(ICleanerMode cleanerMode) {
        this.cleanerMode = cleanerMode;
    }
    
    private List<ICleaner> getRegisteredCleaners() {
        return registeredCleaners;
    }
    
    public Settings getSettings() {
        return settings;
    }
    
    public class Settings {
        
        private final Cleaner cleaner;
        private int imageWidth = 500;
        private int imageHeight = 500;
        
        public Settings(Cleaner cleaner) {
            this.cleaner = cleaner;
        }
        
        public Cleaner getCleaner() {
            return cleaner;
        }
        
        public int getImageWidth() {
            return imageWidth;
        }
        
        public Settings setImageWidth(int imageWidth) {
            this.imageWidth = imageWidth;
            return this;
        }
        
        public int getImageHeight() {
            return imageHeight;
        }
        
        public Settings setImageHeight(int imageHeight) {
            this.imageHeight = imageHeight;
            return this;
        }
    }
}