package se.proxus.epubby.cleaner;

import se.proxus.epubby.cleaner.cleaners.modes.*;

public final class CleanerModeConstants {

    public static final CleanerModeAll ALL = new CleanerModeAll();

    public static final CleanerModeDefault DEFAULT = new CleanerModeDefault();

    public static final CleanerModeDebug DEBUG = new CleanerModeDebug();

    public static final CleanerModeBasic BASIC = new CleanerModeBasic();

    public static final CleanerModeNone NONE = new CleanerModeNone();

    public static final CleanerModeProxus PROXUS = new CleanerModeProxus();

    public static final CleanerModeProxusPDF PROXUS_PDF = new CleanerModeProxusPDF();
    
}