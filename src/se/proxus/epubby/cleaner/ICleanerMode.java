package se.proxus.epubby.cleaner;

public interface ICleanerMode {
    
    void setupCleaners(final Cleaner cleaner);
    
}