package se.proxus.epubby;

import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;
import se.proxus.epubby.organizer.FolderStructureHelper;
import se.proxus.epubby.tools.FileTools;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;

public final class BookReader {

    public static Book readFile(final File epubFile, final boolean verboseLogging)
            throws IOException, ArchiveException {
        //final File tempDirectory = Files.createTempDir();
        final File tempDirectory = FileTools.createTempDir(epubFile.getParentFile());

        try (ZipFile zipFile = new ZipFile(epubFile)) {
            for (final Enumeration<ZipArchiveEntry> entries = zipFile.getEntries();
                 entries.hasMoreElements(); ) {
                final ZipArchiveEntry zipEntry = entries.nextElement();

                if (!FolderStructureHelper.isValidName(zipEntry.getName())) {
                    continue;
                }

                try (InputStream inputStream = zipFile.getInputStream(zipEntry)) {
                    FileTools.extract(tempDirectory, zipEntry, inputStream);
                    inputStream.close();
                }
            }
        }

        //extractZip(epubFile, tempDirectory);

        final File contentDirectory = new File(tempDirectory, "OEBPS");

        FolderStructureHelper.checkStructureValidity(contentDirectory);

        final Book book = new Book(tempDirectory, epubFile, true, verboseLogging);

        for (final File resourceFile : FileTools.getAllFiles(contentDirectory, false)) {
            book.getResources().addResource(resourceFile);
        }

        return book;
    }

    /*private static void extractZip(final File zipFile, final File destDir) throws IOException {
        final byte[] buf = new byte[1024];
        ZipInputStream zipinputstream = null;
        ZipEntry zipentry;
        zipinputstream = new ZipInputStream(new FileInputStream(zipFile));

        zipentry = zipinputstream.getNextEntry();
        while (zipentry != null) {
            //for each entry to be extracted
            final String entryName = zipentry.getName();
            System.out.println("entryname " + entryName);
            int n;
            final FileOutputStream fileoutputstream;
            final File newFile = new File(entryName);
            final String directory = newFile.getParent();

            if (directory == null) {
                if (newFile.isDirectory()) { break; }
            }

            fileoutputstream = new FileOutputStream(destDir.getAbsolutePath() + entryName);

            while ((n = zipinputstream.read(buf, 0, 1024)) > -1) { fileoutputstream.write(buf, 0, n); }

            fileoutputstream.close();
            zipinputstream.closeEntry();
            zipentry = zipinputstream.getNextEntry();

        }

        zipinputstream.close();
    }*/
}