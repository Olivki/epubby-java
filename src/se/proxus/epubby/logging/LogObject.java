package se.proxus.epubby.logging;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public final class LogObject {

    private String message;
    private Calendar calendar;
    private Class<?> caller;
    private LogType type;

    public LogObject(final String message, final Calendar calendar, final LogType type) {
        setMessage(message);
        setCalendar(calendar);
        setType(type);
    }

    public String getDateToString() {
        final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        return dateFormat.format(getCalendar().getTime());
    }

    public String getHandle() {
        return "[" + getDateToString() + " " + getCaller().getName()
                + " " + getType().name() + "]";
    }

    public String getMessageAndDate() {
        return getHandle() + " " + getMessage();
    }

    public Class<?> getCaller() {
        return caller;
    }

    public void setCaller(final Class<?> caller) {
        this.caller = caller;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public void setCalendar(final Calendar calender) {
        calendar = calender;
    }

    public LogType getType() {
        return type;
    }

    public void setType(final LogType type) {
        this.type = type;
    }
}