package se.proxus.epubby.logging;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

public final class LogManager {

    private static final List<LogObject> STORED_LOGS = new LinkedList<>();
    private static String lastUsedDate;
    private static int amountOfLogs = 1;
    private static File directory;

    public void init(final File directory) {
        final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
        final Calendar cal = Calendar.getInstance();
        final File mainDirectory = new File(directory, "Logs");

        try {
            FileUtils.forceMkdir(mainDirectory);
        } catch (final IOException e) {
            e.printStackTrace();
        }

        final DateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
        final Calendar cal2 = Calendar.getInstance();

        if (getLastUsedDate() == null) {
            setLastUsedDate(dateFormat2.format(cal2.getTime()));
            debug("Last used date is empty, populating it..");
        }

        setDirectory(new File(mainDirectory, dateFormat.format(cal.getTime())));
    }

    public void log(final String message, final String callerClassName, final LogType type) {
        final LogObject log = new LogObject(message, Calendar.getInstance(), type);
        final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        try {
            log.setCaller(classLoader.loadClass(callerClassName));
        } catch (final ClassNotFoundException e) {
            e.printStackTrace();
        }

        checkLogDate();

        getStoredLogs().add(log);
        System.err.println(log.getHandle());
        System.err.println(log.getMessage());
        System.err.println(" ");

        try {
            if (getDirectory() == null) {
                return;
            }

            FileUtils.writeStringToFile(getCurrentLogFile(), log.getHandle() + "\r\n", "UTF-8", true);
            FileUtils.writeStringToFile(getCurrentLogFile(), log.getMessage() + "\r\n", "UTF-8", true);
            FileUtils.writeStringToFile(getCurrentLogFile(), "\r\n", "UTF-8", true);
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    public void checkLogDate() {
        final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final Calendar cal = Calendar.getInstance();
        final String newDate = dateFormat.format(cal.getTime());

        if (!(getLastUsedDate().equalsIgnoreCase(newDate))) {
            debug("[" + getLastUsedDate() + " -> " + newDate + "] New date detected! Clearing old logs.");
            getStoredLogs().clear();
        }

        setLastUsedDate(newDate);
    }

    public void saveLog() {

    }

    public void info(final String message) {
        log(message, getCallerClass(), LogType.INFO);
    }

    public void warn(final String message) {
        log(message, getCallerClass(), LogType.WARNING);
    }

    public void error(final String message) {
        log(message, getCallerClass(), LogType.ERROR);
    }

    public void debug(final String message) {
        log(message, getCallerClass(), LogType.DEBUG);
    }

    public void info(final Object message) {
        log("" + message, getCallerClass(), LogType.INFO);
    }

    public void warn(final Object message) {
        log("" + message, getCallerClass(), LogType.WARNING);
    }

    public void error(final Object message) {
        log("" + message, getCallerClass(), LogType.ERROR);
    }

    public void debug(final Object message) {
        log("" + message, getCallerClass(), LogType.DEBUG);
    }

    public void fatal(final Object message) {
        log("" + message, getCallerClass(), LogType.FATAL);
    }

    public String getPrefix(final Class<?> caller) {
        final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-mm-ss");
        final Calendar cal = Calendar.getInstance();
        return "[" + dateFormat.format(cal.getTime()) + " " + caller.getName() +
                "] ";
    }

    public File getCurrentLogFile() {
        final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final Calendar cal = Calendar.getInstance();
        final String fileName = dateFormat.format(cal.getTime()) + ".txt";

        return new File(getDirectory(), fileName);
    }

    public List<LogObject> getStoredLogs() {
        return STORED_LOGS;
    }

    private File getDirectory() {
        return directory;
    }

    public File setDirectory(final File directory) {
        LogManager.directory = directory;
        return directory;
    }

    public int getAmountOfLogs() {
        return amountOfLogs;
    }

    public void setAmountOfLogs(final int amountOfLogs) {
        this.amountOfLogs = amountOfLogs;
    }

    public Class<?> getCallerCallerClass() {
        final StackTraceElement[] stElements = Thread.currentThread().getStackTrace();
        String callerClassName = null;

        for (int i = 1; i < stElements.length; i++) {
            final StackTraceElement ste = stElements[i];
            if (!ste.getClassName().equals(LogManager.class.getName()) &&
                    ste.getClassName().indexOf("java.lang.Thread") != 0) {
                if (callerClassName == null) {
                    callerClassName = ste.getClassName();
                } else if (!callerClassName.equals(ste.getClassName())) {
                    return ste.getClass();
                }
            }
        }

        return null;
    }

    public String getCallerClass() {
        final StackTraceElement[] stElements = Thread.currentThread().getStackTrace();

        for (int i = 1; i < stElements.length; i++) {
            final StackTraceElement ste = stElements[i];
            if (!(ste.getClassName().equalsIgnoreCase(getClass().getName()))) {
                return ste.getClassName();
            }
        }

        return "";
    }

    public static String getLastUsedDate() {
        return lastUsedDate;
    }

    public void setLastUsedDate(final String lastUsedDate) {
        this.lastUsedDate = lastUsedDate;
    }
}