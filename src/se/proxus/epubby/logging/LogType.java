package se.proxus.epubby.logging;

/**
 * Created by Oliver on 2018-06-21.
 */
public enum LogType {

    INFO("Info"), ERROR("Error"), WARNING("Warning"), DEBUG("Debug"), FATAL("Fatal");

    private String name;

    private LogType(final String name) {
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
