package se.proxus.epubby;


import lombok.Data;
import lombok.Getter;
import lombok.experimental.Accessors;
import se.proxus.epubby.cleaner.Cleaner;
import se.proxus.epubby.logging.LogManager;
import se.proxus.epubby.resources.ResourceManager;
import se.proxus.epubby.resources.content.Content;
import se.proxus.epubby.resources.content.ContentWriter;
import se.proxus.epubby.resources.pages.PageManager;
import se.proxus.epubby.resources.stylesheet.StyleSheetReaderFactory;
import se.proxus.epubby.resources.toc.TableOfContents;
import se.proxus.epubby.resources.toc.TableOfContentsWriter;
import se.proxus.epubby.resources.types.NcxResource;
import se.proxus.epubby.resources.types.OpfResource;
import se.proxus.epubby.tools.FileTools;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.File;
import java.io.IOException;

@Getter
public final class Book {

    private final StyleSheetReaderFactory styleSheets;
    private final File file;
    private final File tempDirectory;
    private final ResourceManager resources;
    private final PageManager pages;
    private final TableOfContents tableOfContents;
    private final Settings settings;
    private final Content content;
    private final Cleaner cleaner;
    private double version = -1.0D;
    private final boolean existingFile;

    public Book(final File tempDirectory, final File file, final boolean existingFile,
                final boolean verboseLogging) {
        settings = new Settings(this).setVerboseLogging(verboseLogging);
        this.tempDirectory = tempDirectory;
        this.file = file;
        this.existingFile = existingFile;
        styleSheets = new StyleSheetReaderFactory(this);
        resources = new ResourceManager(this);
        pages = new PageManager(this);
        tableOfContents = new TableOfContents(this);
        content = new Content(this);
        cleaner = new Cleaner(this);
        FileTools.init();
        getLog().init(FileTools.createDirectory(new File(FileTools.getAppDir("Proxus"), "epubby")));
    }

    public Book init() {
        getCleaner().registerDefaultCleaners();
        getResources().onLoad();
        getResources().resetManifestIDs();
        populate();

        return this;
    }

    public void cleanPages() {
        if (getSettings().isCleanerActive()) {
            getCleaner().init();
            getPages().cleanPages();
        }
    }

    public void saveAllChanges() {
        try {
            getPages().savePages();

            getStyleSheets().saveStyleSheets();

            if (isEpub3() || getSettings().isCreateNavPage()) {
                getTableOfContents().savePage();
            }

            TableOfContentsWriter.toFile(this);
            ContentWriter.toFile(this);
        } catch (final TransformerException | ParserConfigurationException | IOException e) {
            e.printStackTrace();
        }
    }

    private void populate() {
        try {
            getContent().getManifest().populateManifestEntries();
        } catch (final IOException e) {
            e.printStackTrace();
        }

        getContent().getSpine().populateSpine();
    }

    public boolean isEpub3() {
        return getVersion() >= 3 && getVersion() < 4;
    }

    public boolean isEpub2() {
        return getVersion() >= 2 && getVersion() < 3;
    }

    public LogManager getLog() {
        return new LogManager();
    }

    public boolean hasGuide() {
        return !(getContent().getGuide().getReferences().isEmpty());
    }

    public double getVersion() {
        return version;
    }

    public void setVersion(final double version) {
        this.version = version;
    }

    public OpfResource getOpfResource() {
        return (OpfResource) resources.getFirstResourceByExtension("OPF");
    }

    public NcxResource getNcxResource() {
        return (NcxResource) resources.getFirstResourceByExtension("NCX");
    }

    @Data
    @Accessors(chain = true)
    public class Settings {

        private final Book book;
        private boolean verboseLogging = false;
        private boolean cleanerActive = true;
        private boolean prettyPrint = true;
        private boolean createNavPage = true;

        public Settings(final Book book) {
            this.book = book;
        }
    }
}
