package se.proxus.epubby;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.io.FileUtils;
import se.proxus.epubby.tools.FileTools;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public final class BookWriter {

    public static File toFile(final Book book, final File directory, final String name) throws IOException {
        final File epubFile = new File(directory, name + ".epub");

        if (epubFile.exists()) {
            FileUtils.forceDelete(epubFile);
        }

        FileTools.pack(book.getTempDirectory(), epubFile);

        book.getLog().info("BookWriter [" + epubFile.getName() + ", " +
                book.getResources().getLoadedResources().size() + "]");

        FileUtils.forceDeleteOnExit(book.getTempDirectory());

        return epubFile;
    }

    public static File toFile(final Book book, final String name) throws IOException {
        return toFile(book, book.getFile().getParentFile(), name);
    }

    public static File toFile(final Book book, final File epubFile) throws IOException {
        return toFile(book, epubFile.getParentFile(), epubFile.getName());
    }

    public static File toFile(final Book book) throws IOException {
        return toFile(book, book.getFile());
    }

    // https://stackoverflow.com/a/13476752
    private static void addFileToZip(final ZipArchiveOutputStream outputStream, final String path,
            final String base) throws IOException {
        final File file = new File(path);
        final String entryName = base + file.getName();
        final ArchiveEntry zipEntry = outputStream.createArchiveEntry(file, entryName);
        outputStream.putArchiveEntry(zipEntry);

        if (file.isFile()) {
            IOUtils.copy(new FileInputStream(file), outputStream);
            outputStream.closeArchiveEntry();
        } else if (file.isDirectory()) {
            outputStream.closeArchiveEntry();
            final File[] children = file.listFiles();
            if (children != null) {
                for (final File child : children) {
                    if (child.isDirectory()) {
                        addFileToZip(outputStream, child.getAbsolutePath(), entryName + "/");
                    } else {
                        final String childEntryName = base + file.getName();
                        final ArchiveEntry childZipEntry =
                                outputStream.createArchiveEntry(child, childEntryName);
                        outputStream.putArchiveEntry(childZipEntry);

                        FileUtils.copyFile(child, outputStream);
                        outputStream.closeArchiveEntry();
                    }
                }
            }
        }
    }
    
}