package se.proxus.epubby.resources.pages;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Entities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public final class PageWriter {
    
    public static File toFile(final Document document, final File xhtmlFile) throws IOException {
        if (xhtmlFile.exists()) {
            FileUtils.forceDelete(xhtmlFile);
            xhtmlFile.createNewFile();
        }
        
        //FileUtils.writeStringToFile(xhtmlFile, Entities.unescape(document.outerHtml()), "UTF-8");
        FileOutputStream outputStream = new FileOutputStream(xhtmlFile);
        String html = Entities.unescape(document.outerHtml());
        
        //html.replace("<!--?xml version=\"1.0\" encoding=\"utf-8\"?-->", "<?xml version=\"1.0\"
        // encoding=\"utf-8\"?>");
        
        // Get this to work, might just need to set the first line to a certain text
        
        IOUtils.write(html, outputStream, "UTF-8");
        outputStream.close();
        
        return xhtmlFile; // EPUBs are throwing erros like crazy after being put through jsoup boy
    }
    
    public static File toFile(final Page page) throws IOException {
        return toFile(page.getDocument(),
                      page.getResource()
                          .getFile());
    }
}