package se.proxus.epubby.resources.pages;

import lombok.Data;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.CleanerConstants;
import se.proxus.epubby.resources.Resource;
import se.proxus.epubby.resources.types.PageResource;
import se.proxus.epubby.resources.stylesheet.StyleSheetReader;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@Data
public final class PageManager {
    
    private final List<Page> loadedPages = new LinkedList<>();
    private Interceptor interceptor;
    private final List<String> allStyleAttributes = new LinkedList<>();
    private final Book book;
    
    public void cleanPages() {
        final List<Page> loadedPagesClone = new LinkedList<>();
        
        loadedPagesClone.addAll(getLoadedPages());
        
        for (final Page page : loadedPagesClone) {
            getBook().getCleaner()
                     .cleanPage(page);
        }
        
        loadedPagesClone.clear();
    }
    
    public void removeStyleSheet(final StyleSheetReader reader) {
        getLoadedPages().stream()
                        .filter(page -> page.hasStyleSheet(reader))
                        .forEach(page -> {
                            page.removeStyleSheet(reader.getResource()
                                                        .getHref());
                        });
    }
    
    public void removeAllStyleSheets() {
        for (final Page page : getLoadedPages()) {
            if (getBook().getStyleSheets()
                         .getReaders()
                         .size() > 0) {
                for (final StyleSheetReader reader : page.getStyleSheets()) {
                    getBook().getResources()
                             .removeResource(reader.getResource());
                }
                
                CleanerConstants.HEAD_STYLE_SHEET.cleanPage(getBook(), page, page.getDocument());
            }
        }
    }
    
    public void mergeAllStyleSheets(final StyleSheetReader destinationReader) {
        for (final StyleSheetReader usedReader : getAllUsedStyleSheets()) {
            getBook().getStyleSheets()
                     .mergeStyleSheets(usedReader, destinationReader, false);
        }
    }
    
    public StyleSheetReader[] getAllUsedStyleSheets() {
        final List<StyleSheetReader> styleSheetReaders = new LinkedList<>();
        
        for (final Page page : getLoadedPages()) {
            page.getStyleSheets()
                .stream()
                .filter(reader -> !styleSheetReaders.contains(reader))
                .forEach(styleSheetReaders::add);
        }
        
        return styleSheetReaders.toArray(new StyleSheetReader[0]);
    }
    
    public void addGlobalStyleSheet(final StyleSheetReader styleSheetReader) {
        for (final Page page : getLoadedPages()) {
            page.addStyleSheet(styleSheetReader);
        }
    }
    
    public void savePages() throws IOException {
        for (final Page page : getLoadedPages()) {
            PageWriter.toFile(page);
        }
    }
    
    public Page addPage(final File htmlFile) throws IOException {
        
        if (!getBook().getResources()
                      .exists(htmlFile)) {
            getBook().getLog()
                     .error(htmlFile.getName() + " is not loaded as a resource.");
            return null;
        }
        
        final PageResource resource = (PageResource) getBook().getResources()
                                                              .getResource(htmlFile);
        final Page page = PageReader.readResource(resource);
        
        if (!exists(page.getHref())) {
            getLoadedPages().add(page);
        }
        
        return page;
    }
    
    public Page addPage(final String href, final String content) throws IOException {
        if (!getBook().getResources()
                      .exists(href)) {
            getBook().getLog()
                     .error(href + " is not loaded as a resource.");
            return null;
        }
        
        final PageResource resource = (PageResource) getBook().getResources()
                                                              .getResourceByHref(href);
        final Page page = PageReader.readResource(resource);
        
        if (!exists(page.getHref())) {
            getLoadedPages().add(page);
            getBook().getContent()
                     .getSpine()
                     .addEntry(resource);
        }
        
        return page;
    }
    
    public Page addPage(final PageResource resource, final int index) throws IOException {
        final Page page = PageReader.readResource(resource);
        
        if (!exists(page.getHref())) {
            getLoadedPages().add(index, page);
            getBook().getContent()
                     .getSpine()
                     .addEntry(resource, index);
        }
        
        return page;
    }
    
    public Page addPage(final PageResource resource) throws IOException {
        final Page page = PageReader.readResource(resource);
        
        if (!exists(page.getHref())) {
            getLoadedPages().add(page);
            getBook().getContent()
                     .getSpine()
                     .addEntry(resource);
        }
        
        return page;
    }
    
    public Page addPage(final PageResource resource, final int index, final boolean linear)
    throws IOException {
        final Page page = PageReader.readResource(resource);
        
        if (!exists(page.getHref())) {
            getLoadedPages().add(index, page);
            getBook().getContent()
                     .getSpine()
                     .addEntry(resource, linear, index);
        }
        
        return page;
    }
    
    public Page addPage(final PageResource resource, final boolean linear) throws IOException {
        final Page page = PageReader.readResource(resource);
        
        if (!exists(page.getHref())) {
            getLoadedPages().add(page);
            getBook().getContent()
                     .getSpine()
                     .addEntry(resource, linear);
        }
        
        return page;
    }
    
    public void removePage(final Page page) {
        if (exists(page.getHref())) {
            getLoadedPages().remove(getLoadedPages().indexOf(page));
            getBook().getContent()
                     .getSpine()
                     .removeEntry(page.getResource());
            
            if (getBook().getTableOfContents()
                         .hasEntry(page.getResource())) {
                getBook().getTableOfContents()
                         .removeEntry(page.getResource());
            }
        }
    }
    
    public long getOccurrenceOfClass(final String selector, final String className) {
        long number = 0;
        
        for (final Page page : getLoadedPages()) {
            number += page.getOccurrenceOfClass(selector, className);
        }
        
        return number;
    }
    
    public long getNumberOfElements(final String selector) {
        long number = 0;
        
        for (final Page page : getLoadedPages()) {
            number += page.getNumberOfElements(selector);
        }
        
        return number;
    }
    
    public long getOccurrencesOfAttribute(final String attribute) {
        long occurrences = 0;
        
        for (final Page page : getLoadedPages()) {
            for (final Element element : page.getDocument()
                                             .body()
                                             .getElementsByAttribute(attribute)) {
                occurrences += 1;
            }
        }
        
        return occurrences;
    }
    
    /**
     * This one matches both the attribute name and the content of the attribute to get the
     * occurrence.
     *
     * @param attribute
     * @param content
     *
     * @return
     */
    public long getOccurrencesOfAttribute(final String attribute, final String content) {
        long occurrences = 0;
        
        for (final Page page : getLoadedPages()) {
            for (final Element element : page.getDocument()
                                             .body()
                                             .getElementsByAttribute(attribute)) {
                if (element.attr(attribute)
                           .trim()
                           .equalsIgnoreCase(content.trim())) {
                    occurrences += 1;
                } else {
                    continue;
                }
            }
        }
        
        return occurrences;
    }
    
    public long getOccurrencesOfClass(final String content) {
        long occurrences = 0;
        
        for (final Page page : getLoadedPages()) {
            for (final Element element : page.getDocument()
                                             .body()
                                             .getElementsByAttribute("class")) {
                if (element.attr("class")
                           .trim()
                           .equalsIgnoreCase(content.trim())) {
                    occurrences += 1;
                } else {
                    continue;
                }
            }
        }
        
        return occurrences;
    }
    
    public List<String> getAllAttributesByName(final String attribute) {
        final List<String> foundAttributes = new LinkedList<>();
        
        for (final Page page : getLoadedPages()) {
            for (final Element element : page.getDocument()
                                             .body()
                                             .getElementsByAttribute(attribute)) {
                if (foundAttributes.contains(element.attr(attribute)
                                                    .trim())) {
                    continue;
                } else {
                    foundAttributes.add(element.attr(attribute)
                                               .trim());
                }
            }
        }
        
        return foundAttributes;
    }
    
    public Page getPage(final String href) {
        for (final Page page : getLoadedPages()) {
            if (page.getHref()
                    .equalsIgnoreCase(href)) {
                return page;
            }
        }
        
        return null;
    }
    
    public Page getFirstPageWithAttribute(final String attribute, final String content) {
        for (final Page page : getLoadedPages()) {
            if (page.getDocument()
                    .body()
                    .getElementsByAttributeValue(attribute, content)
                    .size() > 0) {
                return page;
            }
        }
        
        return null;
    }
    
    public Page getPageByFragment(final String fragmentIdentifier, final Page knownPage) {
        for (final Page page : getLoadedPages()) {
            final Document document = page.getDocument();
            
            for (final Element hrefElement : document.getElementsByAttribute("href")) {
                final String content = hrefElement.attr("href");
                
                if (content.contains(fragmentIdentifier)) {
                    return page;
                }
            }
        }
        
        return null;
    }
    
    public void updateHrefReferences(final String oldLocationHref, final Resource newResource) {
        //System.out.println(oldLocationHref + " -> " + newResource.getLocationHref());
        for (final Page page : getLoadedPages()) {
            final Document document = page.getDocument();
            final Elements attributeElements = document.getElementsByAttributeValueStarting("href",
                                                                                            oldLocationHref);
            
            if (attributeElements.size() <= 0) {
                continue;
            }
            
            for (final Element hrefElement : attributeElements) {
                final String href = hrefElement.attr("href");
                
                //System.out.println(page.getHref() + " -> " + href);
                
                if (href.contains("#")) {
                    final String[] hrefSplit = href.split("#");
                    final String fragmentIdentifier = hrefSplit[1];
                    final String newLocation =
                        newResource.getLocationHref() + "#" + fragmentIdentifier;
                    final Page newPage = ((PageResource) newResource).getPage();
                    
                    if (!newPage.containsFragment(fragmentIdentifier)) {
                        continue;
                    }
                    
                    hrefElement.attr("href", newLocation);
                } else {
                    hrefElement.attr("href", newResource.getLocationHref());
                }
            }
        }
    }
    
    public List<String> getAllStyleAttributes() {
        if (allStyleAttributes.isEmpty()) {
            allStyleAttributes.addAll(getAllAttributesByName("style"));
        }
        
        return allStyleAttributes;
    }
    
    public Page getPage(final Resource resource) {
        return getPage(resource.getHref());
    }
    
    public boolean exists(final String href) {
        return getPage(href) != null;
    }
    
    public List<Page> getLoadedPages() {
        return loadedPages;
    }
}
