package se.proxus.epubby.resources.pages;

import com.google.common.net.MediaType;
import lombok.Data;
import org.apache.commons.io.FileUtils;
import se.proxus.epubby.Book;
import se.proxus.epubby.resources.pages.elements.ElementLink;
import se.proxus.epubby.resources.pages.elements.IElement;
import se.proxus.epubby.resources.types.PageResource;
import se.proxus.epubby.resources.types.StyleSheetResource;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@Data
public final class PageBuilder {
    
    private final List<ElementLink> loadedStyleSheets = new LinkedList<>();
    private final List<IElement> loadedElements = new LinkedList<>();
    private final Book book;
    private final String title;
    private final String type;
    private final PageResource pageResource;
    
    public PageBuilder(final Book book, final String title, final String type,
                       final PageResource pageResource) {
        this.book = book;
        this.title = title;
        this.type = type;
        this.pageResource = pageResource;
    }
    
    public PageBuilder(final Book book, final String title, final PageResource page) {
        this(book, title, "", page);
    }
    
    public void addStyleSheet(final StyleSheetResource styleSheet) {
        getStyleSheets().add(new ElementLink("stylesheet",
                                             MediaType.CSS_UTF_8.toString(),
                                             styleSheet.getLocationHref()));
    }
    
    public IElement addElement(final IElement element) {
        getElements().add(element);
        return element;
    }
    
    public String toContentString() {
        final Builder builder = new Builder();
        
        builder.appendLine("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
        builder.appendLine("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3" +
                           ".org/TR/xhtml11/DTD/xhtml11.dtd\">");
        builder.appendLine("<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\">");
        builder.appendLine("<head>");
        builder.appendLine("<meta http-equiv=\"Content-Type\" content=\"application/xhtml+xml; " +
                           "charset=utf-8\" />");
        builder.appendLine("<title>")
               .append(getTitle())
               .append("</title>");
        
        for (final ElementLink styleSheet : getStyleSheets()) {
            builder.appendLine(styleSheet.getHtml());
        }
        
        // Scripts here
        
        // Styles here
        
        builder.appendLine("</head>");
        builder.appendLine();
        builder.appendLine("<body>");
        
        if (getBook().isEpub3()) {
            builder.appendLine("<section epub:type=\"")
                   .append(getType())
                   .append("\" id=\"")
                   .append(getPageResource().getFileName())
                   .append("\">");
        }
        
        builder.appendLine("<div>");
        
        for (final IElement element : getElements()) {
            builder.appendLine(element.getHtml());
        }
        
        builder.appendLine("</div>");
        
        if (getBook().isEpub3()) {
            builder.appendLine("</section>");
        }
        
        builder.appendLine("</body>");
        builder.appendLine("</html>");
        
        return builder.toString();
    }
    
    public File toFile() throws IOException {
        FileUtils.write(getPageResource().getFile(), toContentString(), "UTF-8");
        
        return getPageResource().getFile();
    }
    
    public List<ElementLink> getStyleSheets() {
        return loadedStyleSheets;
    }
    
    public List<IElement> getElements() {
        return loadedElements;
    }
    
    final class Builder {
        
        private final StringBuilder builder = new StringBuilder();
        
        public StringBuilder appendLine(final String html) {
            builder.append(html)
                   .append(System.lineSeparator());
            return builder;
        }
        
        public Builder appendLine() {
            builder.append(System.lineSeparator());
            return this;
        }
        
        @Override
        public String toString() {
            return builder.toString();
        }
    }
    
}
