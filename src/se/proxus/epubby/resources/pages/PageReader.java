package se.proxus.epubby.resources.pages;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Entities;
import org.jsoup.parser.Parser;
import se.proxus.epubby.Book;
import se.proxus.epubby.resources.types.PageResource;
import se.proxus.epubby.tools.FileTools;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public final class PageReader {
    
    public static Page readFile(final Book book, final File htmlFile) throws IOException {
        if (!StringUtils.containsIgnoreCase(FileTools.getExtension(htmlFile), "HTML")) {
            return null;
        }
        
        PageResource resource = (PageResource) book.getResources()
                                                   .getResource(htmlFile);
        
        if (resource == null) {
            resource = (PageResource) book.getResources()
                                          .addResource(htmlFile);
        }
        
        final Document document = Jsoup.parse(htmlFile, "UTF-8");
        
        return new Page(book, document, resource);
    }
    
    public static Page readContent(final Book book, final String href, final String htmlContent)
    throws IOException {
        final Interceptor interceptor = book.getPages()
                                            .getInterceptor();
        
        String content = Entities.unescape(htmlContent);
        
        if (interceptor != null) {
            content = interceptor.intercept(content);
        }
        
        final Document document = Jsoup.parse(content, "UTF-8");
        document.outputSettings()
                .syntax(org.jsoup.nodes.Document.OutputSettings.Syntax.xml);
        
        final File memeFile = FileTools.getFile(book.getTempDirectory(), "Text");
        final File dabFile = new File(memeFile, href);
        
        final File resourceFile = PageWriter.toFile(document, dabFile);
        
        PageResource resource = (PageResource) book.getResources()
                                                   .getResource(resourceFile);
        
        if (resource == null) {
            resource = (PageResource) book.getResources()
                                          .addResource(resourceFile);
        }
        
        return new Page(book, document, resource);
    }
    
    public static Page readResource(final PageResource resource) throws IOException {
        if (!StringUtils.containsIgnoreCase(FileTools.getExtension(resource.getFile()), "HTML")) {
            return null;
        }
        
        final FileInputStream fileInputStream = new FileInputStream(resource.getFile());
        
        String content = Entities.unescape(IOUtils.toString(fileInputStream,
                                                            StandardCharsets.UTF_8));
        
        final Interceptor interceptor = resource.getBook()
                                                .getPages()
                                                .getInterceptor();
        
        if (interceptor != null) {
            content = interceptor.intercept(content);
        }
        
        final Document document = Jsoup.parse(content, "", Parser.xmlParser());
        //final Document document = Jsoup.parse(content, "UTF-8");
        
        fileInputStream.close();
        
        return new Page(resource.getBook(), document, resource);
    }
}