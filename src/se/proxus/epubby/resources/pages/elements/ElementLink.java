package se.proxus.epubby.resources.pages.elements;

// https://www.w3schools.com/Tags/tag_link.asp
public final class ElementLink implements IElement {

    private final String relation;
    private final String type;
    private final String href;

    public ElementLink(final String relation, final String type, final String href) {
        this.relation = relation;
        this.type = type;
        this.href = href;
    }

    @Override
    public String getHtml() {
        return "<" + getTag() + " rel=\"" + getRelation() + "\" type=\"" + getType() + "\" href=\"" +
                getHref() + "\" />";
    }

    @Override
    public String getTag() {
        return "link";
    }

    @Override
    public boolean isSelfClosing() {
        return true;
    }

    public String getRelation() {
        return relation;
    }

    public String getType() {
        return type;
    }

    public String getHref() {
        return href;
    }
}
