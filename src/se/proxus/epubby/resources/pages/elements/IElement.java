package se.proxus.epubby.resources.pages.elements;

public interface IElement {

    public String getHtml();

    public String getTag();

    public boolean isSelfClosing();
    
}