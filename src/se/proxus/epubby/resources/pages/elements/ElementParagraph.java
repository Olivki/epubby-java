package se.proxus.epubby.resources.pages.elements;

public final class ElementParagraph implements IElement {

    private final String content;
    private final String className;
    private final String style;

    public ElementParagraph(final String content, final String className, final String style) {
        this.content = content;
        this.className = className;
        this.style = style;
    }

    public ElementParagraph(final String content, final String className) {
        this(content, className, "");
    }

    public ElementParagraph(final String content) {
        this(content, "", "");
    }

    @Override
    public String getHtml() {
        final StringBuilder builder = new StringBuilder();

        builder.append("<").append(getTag());

        if (hasClass()) {
            builder.append(" class=\"").append(getClassName()).append("\"");
        }

        if (hasStyle()) {
            builder.append(" style=\"").append(getStyle()).append("\"");
        }

        builder.append("> ").append(getContent()).append("</" + getTag() + ">");

        return builder.toString();
    }

    @Override
    public String getTag() {
        return "p";
    }

    @Override
    public boolean isSelfClosing() {
        return false;
    }

    public String getContent() {
        return content;
    }

    public String getClassName() {
        return className;
    }

    public boolean hasClass() {
        return !getClassName().trim().isEmpty();
    }

    public String getStyle() {
        return style;
    }

    public boolean hasStyle() {
        return !getStyle().trim().isEmpty();
    }
}
