package se.proxus.epubby.resources.pages.elements;

import java.util.LinkedList;
import java.util.List;

// https://www.w3schools.com/TAGs/tag_ol.asp
public final class ElementOrderedList implements IElement {

    private final List<ElementList> entries = new LinkedList<>();
    private final String start;
    private final String type;

    public ElementOrderedList(final String start, final String type) {
        this.start = start;
        this.type = type;
    }

    @Override
    public String getHtml() {
        final StringBuilder builder = new StringBuilder();

        builder.append("<").append(getTag());

        if (hasStart()) {
            builder.append(" start=\"").append(getStart()).append("\"");
        }

        if (hasType()) {
            builder.append(" type=\"").append(getType()).append("\"");
        }

        builder.append("> ").append(System.lineSeparator());

        for (final ElementList listElement : getEntries()) {
            builder.append(listElement.getHtml()).append(System.lineSeparator());
        }

        builder.append("</").append(getTag()).append(">");

        return builder.toString();
    }

    @Override
    public String getTag() {
        return "ol";
    }

    @Override
    public boolean isSelfClosing() {
        return false;
    }

    public ElementList addEntry(final String value, final String content) {
        final ElementList elementList = new ElementList(value, content);

        getEntries().add(elementList);

        return elementList;
    }

    public ElementList addEntry(final String content) {
        return addEntry("", content);
    }

    public String getStart() {
        return start;
    }

    public String getType() {
        return type;
    }

    public boolean hasStart() {
        return !getStart().trim().isEmpty();
    }

    public boolean hasType() {
        return !getType().trim().isEmpty();
    }

    public List<ElementList> getEntries() {
        return entries;
    }
}
