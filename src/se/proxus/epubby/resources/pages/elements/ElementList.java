package se.proxus.epubby.resources.pages.elements;

// https://www.w3schools.com/TAGs/tag_ol.asp
public final class ElementList implements IElement {

    private final String value;
    private final String content;

    public ElementList(final String value, final String content) {
        this.value = value;
        this.content = content;
    }

    @Override
    public String getHtml() {
        final StringBuilder builder = new StringBuilder();

        builder.append("<").append(getTag());

        if (hasValue()) {
            builder.append(" value=\"").append(getValue()).append("\"");
        }

        builder.append("> ").append(getContent()).append("</" + getTag() + ">");

        return builder.toString();
    }

    @Override
    public String getTag() {
        return "ol";
    }

    @Override
    public boolean isSelfClosing() {
        return false;
    }

    public String getValue() {
        return value;
    }

    public String getContent() {
        return content;
    }

    public boolean hasValue() {
        return !getValue().trim().isEmpty();
    }
}
