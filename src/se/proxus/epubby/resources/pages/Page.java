package se.proxus.epubby.resources.pages;

import lombok.Data;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import se.proxus.epubby.Book;
import se.proxus.epubby.resources.types.PageResource;
import se.proxus.epubby.resources.stylesheet.StyleSheetReader;
import se.proxus.epubby.tools.JsoupTools;
import se.proxus.epubby.tools.StringTools;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Data
public class Page {
    
    private final String title;
    private final String html;
    private final Book book;
    private final Document document;
    private final PageResource resource;
    private boolean linear = false;
    private final List<StyleSheetReader> styleSheets = new LinkedList<>();
    
    public Page(final Book book, final Document document, final String title,
                final PageResource resource) {
        this.book = book;
        this.document = document;
        this.title = title;
        this.resource = resource;
        html = document.outerHtml();
        
        document.outputSettings()
                .prettyPrint(true);
        document.outputSettings()
                .indentAmount(4);
        
        loadStyleSheets();
    }
    
    public Page(final Book book, final Document document, final PageResource resource) {
        this(book, document, document.title(), resource);
    }
    
    // Make sure to load the stylesheets before the pages to avoid any NullPointerExceptions.
    public void loadStyleSheets() {
        for (final Element linkElement : getDocument().select("link[rel=stylesheet]")) {
            getStyleSheets().add(getBook().getStyleSheets()
                                          .getReader(StringTools.href(linkElement.attr("href"))));
        }
    }
    
    public void removeStyleSheet(final String href) {
        final StyleSheetReader reader = getBook().getStyleSheets()
                                                 .getReader(href);
        
        if (reader != null) {
            getStyleSheets().remove(reader);
        }
    }
    
    public StyleSheetReader addStyleSheet(final StyleSheetReader reader) {
        final String html = "<link rel=\"stylesheet\" type=\"text/css\" href=\"" +
                            reader.getResource()
                                  .getLocationHref() + "\"/>";
        
        if (JsoupTools.exists(getDocument().head()
                                           .select("link[rel=stylesheet]"))) {
            getDocument().head()
                         .selectFirst("[rel=stylesheet]")
                         .after(html);
        } else {
            getDocument().selectFirst("title")
                         .after(html);
        }
        
        getStyleSheets().add(reader);
        
        return reader;
    }
    
    public StyleSheetReader getFirstReaderWithRule(final String ruleName) {
        for (final StyleSheetReader styleSheetReader : getStyleSheets()) {
            if (styleSheetReader.containsRuleName(ruleName)) {
                return styleSheetReader;
            }
        }
        
        return null;
    }
    
    public StyleSheetReader getLastReaderWithRule(final String ruleName) {
        final List<StyleSheetReader> tempReaders = new LinkedList<>();
        tempReaders.addAll(getStyleSheets());
        Collections.reverse(tempReaders);
        
        for (final StyleSheetReader styleSheetReader : tempReaders) {
            if (styleSheetReader.containsRuleName(ruleName)) {
                return styleSheetReader;
            }
        }
        
        return null;
    }
    
    public boolean ruleExists(final String ruleName) {
        return getLastReaderWithRule(ruleName) != null;
    }
    
    public long getOccurrenceOfElementAttribute(final String selector, final String attribute) {
        long occurrences = 0;
        
        for (final Element element : getDocument().select(selector)) {
            if (!element.hasAttr(attribute)) {
                continue;
            }
            
            occurrences += 1;
        }
        
        return occurrences;
    }
    
    public int getOccurrenceOfAttribute(final String attribute, final String content) {
        int occurrences = 0;
        
        for (final Element hrefElement : document.getElementsByAttribute(attribute)) {
            if (!hrefElement.attr(attribute)
                            .equalsIgnoreCase(content)) {
                continue;
            }
            
            occurrences++;
        }
        
        return occurrences;
    }
    
    public boolean containsFragment(final String fragmentIdentifier) {
        for (final Element hrefElement : document.getElementsByAttribute("id")) {
            final String content = hrefElement.attr("id");
            
            if (content.contentEquals(fragmentIdentifier)) {
                return true;
            }
        }
        
        return false;
    }
    
    public boolean hasStyleSheet(final StyleSheetReader reader) {
        for (final StyleSheetReader styleSheetReader : getStyleSheets()) {
            if (styleSheetReader.getResource()
                                .getHref()
                                .equalsIgnoreCase(reader.getResource()
                                                        .getHref())) {
                return true;
            }
        }
        
        return false;
    }
    
    public int getOccurrenceOfClass(final String selector, final String className) {
        return getDocument().body()
                            .select(selector + "." + className)
                            .size();
    }
    
    public int getNumberOfElements(final String selector) {
        return getDocument().body()
                            .select(selector)
                            .size();
    }
    
    public boolean isImagePage() {
        return JsoupTools.containsOnly(getDocument().body(), "img");
    }
    
    public String getHref() {
        final String href;
        
        if (getResource() == null) {
            href = "NIL";
        } else {
            href = getResource().getHref();
        }
        
        return href;
    }
    
    public List<StyleSheetReader> getStyleSheets() {
        return styleSheets;
    }
}