package se.proxus.epubby.resources.pages;

public interface Interceptor {

    public abstract String intercept(final String content);
    
}