package se.proxus.epubby.resources.types;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import se.proxus.epubby.Book;
import se.proxus.epubby.resources.Resource;
import se.proxus.epubby.resources.ResourceType;
import se.proxus.epubby.resources.pages.Page;

import java.io.File;
import java.io.IOException;

@Getter
@Setter
@ToString(callSuper = true)
public final class PageResource extends Resource {

    private Page page;

    public PageResource(final Book book, final String name, final File file) {
        super(book, ResourceType.PAGES, name, file);
    }

    @Override
    public void onLoad() {
        try {
            this.page = getBook().getPages().addPage(getFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUnload() {
        getBook().getPages().removePage(getPage());
    }
    
}