package se.proxus.epubby.resources.types;

import lombok.Getter;
import lombok.ToString;
import se.proxus.epubby.Book;
import se.proxus.epubby.resources.Resource;
import se.proxus.epubby.resources.ResourceType;
import se.proxus.epubby.tools.FileTools;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Getter
@ToString(callSuper = true)
public final class ImageResource extends Resource {

    private BufferedImage image;
    private ImageType imageType;

    public ImageResource(final Book book, final String name, final File file) {
        super(book, ResourceType.IMAGES, name, file);
    }

    @Override
    public void onLoad() {
        try {
            image = ImageIO.read(getFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
        imageType = determineType(FileTools.getExtension(getFile()));
    }

    private ImageType determineType(final String extension) {
        for (final ImageType imageType : ImageType.values()) {
            if (imageType.name().equalsIgnoreCase(extension)) {
                return imageType;
            }
        }

        return ImageType.UNKNOWN;
    }

    public enum ImageType {
        PNG, JPG, GIF, SVG, UNKNOWN
    }
}