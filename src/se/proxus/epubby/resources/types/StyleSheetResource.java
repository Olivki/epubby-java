package se.proxus.epubby.resources.types;

import lombok.Getter;
import lombok.ToString;
import se.proxus.epubby.Book;
import se.proxus.epubby.resources.Resource;
import se.proxus.epubby.resources.ResourceType;
import se.proxus.epubby.resources.stylesheet.StyleSheetReader;

import java.io.File;

@Getter
@ToString(callSuper = true)
public final class StyleSheetResource extends Resource {
    
    private StyleSheetReader styleSheetReader;
    
    public StyleSheetResource(final Book book, final String name, final File file) {
        super(book, ResourceType.STYLES, name, file);
    }
    
    @Override
    public void onLoad() {
        styleSheetReader = getBook().getStyleSheets()
                                    .registerReader(this);
    }
    
    @Override
    public void onUnload() {
        getBook().getPages()
                 .removeStyleSheet(getStyleSheetReader());
        getBook().getStyleSheets()
                 .unregisterReader(this);
    }
}