package se.proxus.epubby.resources.types;

import lombok.Getter;
import lombok.ToString;
import org.xml.sax.SAXException;
import se.proxus.epubby.Book;
import se.proxus.epubby.resources.Resource;
import se.proxus.epubby.resources.ResourceType;
import se.proxus.epubby.resources.toc.TableOfContentsReader;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

@Getter
@ToString(callSuper = true)
public final class NcxResource extends Resource {
    
    public NcxResource(final Book book, final String name, final File file) {
        super(book, ResourceType.NCX, name, file);
    }
    
    @Override
    public void onLoad() {
        try {
            TableOfContentsReader.readFile(getBook(), getFile());
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }
    }
}