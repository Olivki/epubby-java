package se.proxus.epubby.resources.types;

import lombok.Getter;
import lombok.ToString;
import se.proxus.epubby.Book;
import se.proxus.epubby.resources.Resource;
import se.proxus.epubby.resources.ResourceType;
import se.proxus.epubby.resources.content.ContentReader;

import java.io.File;
import java.io.IOException;

@Getter
@ToString(callSuper = true)
public final class OpfResource extends Resource {

    public OpfResource(final Book book, final String name, final File file) {
        super(book, ResourceType.OPF, name, file);
    }

    @Override
    public void onLoad() { // This loads before pages and maybe something more, might cause issues.
        try {
            ContentReader.readFile(getBook(), getFile());
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }
}