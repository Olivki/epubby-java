package se.proxus.epubby.resources.content;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import se.proxus.epubby.Book;
import se.proxus.epubby.resources.Resource;

import java.util.LinkedList;
import java.util.List;

@Data
public final class Guide {

    private final List<Reference> references = new LinkedList<>();

    private final Book book;

    public Reference setCover(final Resource resource) {
        Reference coverReference = getReferenceByType("cover");

        if (coverReference == null) {
            coverReference = addReference("cover", "Cover Image", resource);
        } else {
            coverReference.setHref(resource.getHref());
        }

        return coverReference;
    }

    public Reference getTableOfContents() {
        return getReferenceByType("toc");
    }

    public Reference setTableOfContents(final Resource resource) {
        Reference tocReference = getReferenceByType("toc");

        if (tocReference == null) {
            tocReference = addReference("toc", "Table of Contents", resource);
        } else {
            tocReference.setHref(resource.getHref());
        }

        return tocReference;
    }

    public Reference getCover() {
        return getReferenceByType("cover");
    }

    public Reference addReference(final String type, final String title, final String href) {
        final Reference reference = new Reference(type, title, href);

        getReferences().add(reference);

        if (getBook().getSettings().isVerboseLogging()) {
            getBook().getLog().debug(reference.toString());
        }

        return reference;
    }

    public Reference addReference(final String type, final String title, final Resource resource) {
        return addReference(type, title, resource.getHref());
    }

    public Reference getReferenceByType(final String type) {
        for (final Reference reference : getReferences()) {
            if (reference.getType().equalsIgnoreCase(type)) {
                return reference;
            }
        }

        return null;
    }

    public Reference getReferenceByTitle(final String title) {
        for (final Reference reference : getReferences()) {
            if (reference.getTitle().equalsIgnoreCase(title)) {
                return reference;
            }
        }

        return null;
    }

    public Reference getReferenceByHref(final String href) {
        for (final Reference reference : getReferences()) {
            if (reference.getHref().equalsIgnoreCase(href)) {
                return reference;
            }
        }

        return null;
    }

    public Reference getLooseFirstReference(final String string) {
        for (final Reference reference : getReferences()) {
            if (reference.contains(string)) {
                return reference;
            }
        }

        return null;
    }

    public void removeReference(final Reference reference) {
        getReferences().remove(getReferences().indexOf(reference));
    }

    public void removeReferenceByType(final String type) {
        if (getReferenceByType(type) == null) {
            return;
        }

        removeReference(getReferenceByType(type));
    }

    public void removeReferenceByTitle(final String title) {
        if (getReferenceByTitle(title) == null) {
            return;
        }

        removeReference(getReferenceByTitle(title));
    }

    public void removeReferenceByHref(final String href) {
        if (getReferenceByHref(href) == null) {
            return;
        }

        removeReference(getReferenceByHref(href));
    }

    public boolean hasEntry(final String type) {
        return getReferenceByType(type) != null;
    }

    public List<Reference> getReferences() {
        return references;
    }

    @Data
    public class Reference {

        private final String type;
        private final String title;
        private String href;

        public Reference(final String type, final String title, final String href) {
            this.type = type;
            this.title = title;
            this.href = href;
        }

        public String getOutput() {
            return "<reference type=\"" + getType() + "\" title=\"" + getTitle() + "\" href=\"" + getHref() +
                    "\"/>";
        }

        public boolean contains(final String string) {
            return StringUtils.containsIgnoreCase(getType(), string) ||
                    StringUtils.containsIgnoreCase(getTitle(), string) ||
                    StringUtils.containsIgnoreCase(getHref(), string);
        }
    }
}