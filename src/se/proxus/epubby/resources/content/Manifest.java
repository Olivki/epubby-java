package se.proxus.epubby.resources.content;

import lombok.Data;
import se.proxus.epubby.Book;
import se.proxus.epubby.resources.Resource;
import se.proxus.epubby.tools.FileTools;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@Data
public final class Manifest {
    
    private final List<Entry> manifestEntries = new LinkedList<>();
    private final Book book;
    
    public Resource getManifestResource(final Entry entry) {
        return entry.getResource();
    }
    
    public Resource getManifestResourceByID(final String manifestEntryID) {
        return getEntry(manifestEntryID).getResource();
    }
    
    public Entry addEntry(final String id, final Resource resource, final String mediaType,
                          final String properties) {
        final Entry entry = new Entry(id, resource, mediaType, properties);
        
        if (!contains(resource.getHref())) {
            getEntries().add(entry);
            resource.setManifestIdentifier(id);
        } else {
            getEntryByHref(resource.getHref()).setIdentifier(id);
            resource.setManifestIdentifier(id);
        }
        
        return entry;
    }
    
    public Entry addEntry(final String id, final Resource resource, final String mediaType) {
        return addEntry(id, resource, mediaType, "");
    }
    
    public void removeEntry(final Resource resource) {
        if (contains(resource.getHref())) {
            getEntries().remove(getEntries().indexOf(getEntryByHref(resource.getHref())));
        }
    }
    
    public void changeIndex(final int newIndex, final Entry entry) {
        if (contains(entry.getHref())) {
            getEntries().remove(getEntries().indexOf(entry));
            getEntries().add(newIndex, entry);
        }
    }
    
    public Entry getEntry(final Resource resource) {
        for (final Entry entry : getEntries()) {
            if (entry.getHref()
                     .equalsIgnoreCase(resource.getHref())) {
                return entry;
            }
        }
        
        return null;
    }
    
    public Entry getEntry(final String id) {
        for (final Entry entry : getEntries()) {
            if (entry.getIdentifier()
                     .equalsIgnoreCase(id)) {
                return entry;
            }
        }
        
        return null;
    }
    
    public Entry getEntryByProperties(final String properties) {
        for (final Entry entry : getEntries()) {
            if (entry.getProperties()
                     .equalsIgnoreCase(properties)) {
                return entry;
            }
        }
        
        return null;
    }
    
    public Entry getEntryByHref(final String href) {
        for (final Entry entry : getEntries()) {
            if (entry.getHref()
                     .equalsIgnoreCase(href)) {
                return entry;
            }
        }
        
        return null;
    }
    
    public boolean contains(final String href) {
        return getEntryByHref(href) != null;
    }
    
    public List<Entry> getEntries() {
        return manifestEntries;
    }
    
    public void populateManifestEntries()
    throws IOException {
        getEntries().clear();
        
        for (final Resource resource : getBook().getResources()
                                                .getLoadedResources()) {
            final String extension = FileTools.getExtension(resource.getFile());
            final String mediaType = FileTools.getMediaType(resource.getFile());
            String id = "";
            String href = "";
            
            if (extension.equalsIgnoreCase("ncx")) {
                id = "navigation";
                href = resource.getHref();
            }
            
            if (mediaType == null) {
                continue;
            }
            
            if (!extension.equalsIgnoreCase("ncx")) {
                id = "x_" + resource.getFileName()
                                    .toLowerCase();
                href = resource.getHref();
            }
            
            if (!contains(href)) {
                addEntry(id,
                         getBook().getResources()
                                  .getResourceByHref(href),
                         mediaType);
            }
        }
    }
    
    @Data
    public class Entry {
        
        private String identifier;
        private String href;
        private Resource resource;
        private final String mediaType;
        private String properties;
        
        public Entry(final String identifier, final Resource resource, final String mediaType,
                     final String properties) {
            this.identifier = identifier;
            this.resource = resource;
            href = resource.getHref();
            this.mediaType = mediaType;
            this.properties = properties;
        }
        
        public void updateHref() {
            href = resource.getHref();
        }
        
        public boolean hasProperties() {
            return !getProperties().trim()
                                   .isEmpty();
        }
    }
}
