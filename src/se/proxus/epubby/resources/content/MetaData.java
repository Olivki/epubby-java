package se.proxus.epubby.resources.content;

import com.google.common.collect.Lists;
import lombok.Data;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import se.proxus.epubby.Book;
import se.proxus.epubby.tools.StringTools;

import java.util.*;
import java.util.stream.Collectors;

@Data
public final class MetaData {
    
    //private final Map<String, Field> fields = new LinkedHashMap<>();
    private final List<Field> fields = new LinkedList<>();
    private final Book book;
    
    public Field addField(final String title, final String content) {
        final Field tempField = new Field(title, content);
        
        getFields().add(tempField);
        
        if (getBook().getSettings()
                     .isVerboseLogging()) {
            getBook().getLog()
                     .debug(tempField.toString());
        }
        
        return tempField;
    }
    
    public void removeField(final String title) {
        getFields().remove(title);
    }
    
    public Field getFirstField(final String name) {
        for (final Field field : getFields()) {
            if (field.getName()
                     .equalsIgnoreCase(name)) {
                return field;
            }
        }
        
        return null;
    }
    
    public Field[] getFields(final String name) {
        final List<Field> foundFields = getFields().stream()
                                                   .filter(field -> field.getName()
                                                                         .equalsIgnoreCase(name))
                                                   .collect(Collectors.toCollection(LinkedList::new));
        
        return foundFields.toArray(new Field[foundFields.size()]);
    }
    
    public Field[] getFieldsStartingWith(final String name) {
        final List<Field> foundFields = new LinkedList<>();
        
        for (final Field field : getFields()) {
            if (field.getName()
                     .startsWith("meta")) {
                if (field.getAttribute("name") != null) {
                    if (StringUtils.startsWithIgnoreCase(field.getAttribute("name"), name)) {
                        foundFields.add(field);
                    }
                }
            } else {
                if (StringUtils.startsWithIgnoreCase(field.getName(), name)) {
                    foundFields.add(field);
                }
            }
        }
        
        return foundFields.toArray(new Field[foundFields.size()]);
    }
    
    public String[] getFieldsContent(final String name) {
        final List<String> foundFields = getFields().stream()
                                                    .filter(field -> field.getName()
                                                                          .equalsIgnoreCase(name))
                                                    .map(Field::getContent)
                                                    .collect(Collectors.toCollection(LinkedList::new));
        
        return foundFields.toArray(new String[foundFields.size()]);
    }
    
    public MetaData addAuthor(final String name) {
        addField("dc:creator", name).addAttribute("opf:role", "aut");
        return this;
    }
    
    public MetaData addIllustrator(final String name) {
        addField("dc:creator", name).addAttribute("opf:role", "ilu");
        return this;
    }
    
    public String getContent(final String name) {
        return getFirstField(name).getContent();
    }
    
    public String getAuthor() {
        return getAuthors()[0];
    }
    
    public String[] getAuthors() {
        return getFieldsContent("dc:creator");
    }
    
    public void setTitle(final String title) {
        getFirstField("dc:title").setContent(title);
    }
    
    public String getTitle() {
        return getFirstField("dc:title").getContent();
    }
    
    public void setLanguage(final String language) {
        addField("dc:language", language);
    }
    
    public String getLanguage() {
        Field identifierField = getFirstField("dc:language");
        
        if (identifierField == null) {
            identifierField = addField("dc:language", "en");
        }
        
        return identifierField.getContent();
    }
    
    public void setIdentifier(final String identifier) {
        addField("dc:identifier", identifier);
    }
    
    public String getIdentifier() {
        return getIdentifierField().getContent();
    }
    
    public Field getIdentifierField() {
        Field identifierField = getFirstField("dc:identifier");
        
        if (identifierField == null) {
            identifierField = addField("dc:identifier", "" + StringTools.toBookIdentifier());
            //identifierField = addField("dc:identifier", "SkZAhZlxQ");
            identifierField.addAttribute("id", "BookId");
            identifierField.addAttribute("opf:scheme", "URI");
        }
        
        return identifierField;
    }
    
    public void removeAllFields(final String fieldName) {
        getFields().removeAll(Lists.newArrayList(getFields(fieldName)));
    }
    
    public void removeAllFieldsStartingWith(final String fieldName) {
        getFields().removeAll(Lists.newArrayList(getFieldsStartingWith(fieldName)));
    }
    
    public List<Field> getFields() {
        return fields;
    }
    
    @Data
    public class Field {
        
        private final Map<String, String> attributes = new HashMap<>();
        
        private final String name;
        @NonNull
        private String content;
        
        public String getOutput() {
            final StringBuilder builder = new StringBuilder();
            
            builder.append("<")
                   .append(getName());
            
            if (hasAttributes()) {
                builder.append(" ")
                       .append(attributesToString())
                       .append(">");
            } else {
                builder.append(">");
            }
            
            builder.append(getContent());
            
            builder.append("</")
                   .append(getName())
                   .append(">");
            
            return builder.toString()
                          .trim();
        }
        
        public String attributesToString() {
            final StringBuilder builder = new StringBuilder();
            
            for (final String name : getAttributes().keySet()) {
                final String content = getAttribute(name);
                
                builder.append(name)
                       .append("=\"")
                       .append(content)
                       .append("\"")
                       .append(" ");
            }
            
            return builder.toString()
                          .trim();
        }
        
        public String attributesToInfoString() {
            final StringBuilder builder = new StringBuilder();
            
            builder.append("[");
            
            for (final String name : getAttributes().keySet()) {
                final String content = getAttribute(name);
                
                builder.append("{")
                       .append(name)
                       .append(", ")
                       .append(content)
                       .append("}");
                
                if ((getIndex(name) + 1) != getAttributes().size() && getIndex(name) != -1) {
                    builder.append(", ");
                }
            }
            
            builder.append("]");
            
            return builder.toString()
                          .trim();
        }
        
        private int getIndex(final String key) {
            final List<String> keys = new ArrayList(getAttributes().keySet());
            
            for (int index = 0; index < keys.size(); index++) {
                final String matchKey = keys.get(index);
                
                if (matchKey.equalsIgnoreCase(key)) {
                    return index;
                }
            }
            
            return -1;
        }
        
        public boolean hasAttributes() {
            return getAttributes().size() > 0;
        }
        
        public String getAttribute(final String name) {
            return getAttributes().get(name);
        }
        
        public Field addAttribute(final String name, String content) {
            if (content.startsWith("#")) {
                content = content.substring(0);
            }
            
            if (getBook().isEpub3() && !getName().equalsIgnoreCase("meta")) {
                getAttributes().put("id", content);
            } else {
                getAttributes().put(name, content);
            }
            return this;
        }
        
        public void removeAttribute(final String name) {
            getAttributes().remove(name);
        }
        
        public Map<String, String> getAttributes() {
            return attributes;
        }
        
        @Override
        public String toString() {
            final StringBuilder builder = new StringBuilder();
            
            builder.append("MetaDataField [")
                   .append(getName())
                   .append(", ")
                   .append(getContent());
            
            if (hasAttributes()) {
                builder.append(", ")
                       .append(attributesToInfoString())
                       .append("]");
            } else {
                builder.append("]");
            }
            
            return builder.toString();
        }
    }
}