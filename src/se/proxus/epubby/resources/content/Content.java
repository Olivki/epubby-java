package se.proxus.epubby.resources.content;

import lombok.Data;
import se.proxus.epubby.Book;

@Data
public final class Content {
    
    private final Book book;
    private final MetaData metaData;
    private final Guide guide;
    private final Spine spine;
    private final Manifest manifest;
    
    public Content(final Book book) {
        this.book = book;
        metaData = new MetaData(book);
        guide = new Guide(book);
        spine = new Spine(book);
        manifest = new Manifest(book);
    }
}
