package se.proxus.epubby.resources.content;

import lombok.Data;
import lombok.Value;
import se.proxus.epubby.Book;
import se.proxus.epubby.resources.pages.Page;
import se.proxus.epubby.resources.types.PageResource;

import java.util.LinkedList;
import java.util.List;

@Data
public final class Spine {
    
    private final Book book;
    private String pageProgressionDirection = "ltr";
    private final List<SpineEntry> spineEntries = new LinkedList<>();
    
    public void populateSpine() {
        getEntries().clear();
        
        for (final Page page : getBook().getPages()
                                        .getLoadedPages()) {
            addEntry(page.getResource(), page.isLinear());
        }
    }
    
    public SpineEntry addEntry(final String id, final boolean linear, final int index) {
        final SpineEntry entry = new SpineEntry(id, linear);
        
        if (!contains(id)) {
            getEntries().add(index, entry);
        }
        
        return entry;
    }
    
    public SpineEntry addEntry(final String id, final boolean linear) {
        final SpineEntry entry = new SpineEntry(id, linear);
        
        if (!contains(id)) {
            getEntries().add(entry);
        }
        
        return entry;
    }
    
    public SpineEntry addEntry(final String id, final String linear, final int index) {
        boolean flag = false;
        
        if (linear.equalsIgnoreCase("yes")) {
            flag = true;
        } else if (linear.equalsIgnoreCase("no")) {
            flag = false;
        }
        
        return addEntry(id, flag, index);
    }
    
    public SpineEntry addEntry(final String id, final String linear) {
        boolean flag = false;
        
        if (linear.equalsIgnoreCase("yes")) {
            flag = true;
        } else if (linear.equalsIgnoreCase("no")) {
            flag = false;
        }
        
        return addEntry(id, flag);
    }
    
    public SpineEntry addEntry(final PageResource resource, final boolean linear) {
        return addEntry(resource.getManifestIdentifier(), linear);
    }
    
    public SpineEntry addEntry(final PageResource resource, final String linear) {
        return addEntry(resource.getManifestIdentifier(), linear);
    }
    
    public SpineEntry addEntry(final String id) {
        return addEntry(id, false);
    }
    
    public SpineEntry addEntry(final PageResource resource) {
        return addEntry(resource.getManifestIdentifier(), false);
    }
    
    public SpineEntry addEntry(final PageResource resource, final boolean linear, final int index) {
        return addEntry(resource.getManifestIdentifier(), linear, index);
    }
    
    public SpineEntry addEntry(final PageResource resource, final String linear, final int index) {
        return addEntry(resource.getManifestIdentifier(), linear, index);
    }
    
    public SpineEntry addEntry(final String id, final int index) {
        return addEntry(id, false, index);
    }
    
    public SpineEntry addEntry(final PageResource resource, final int index) {
        return addEntry(resource.getManifestIdentifier(), false, index);
    }
    
    public void removeEntry(final PageResource pageResource) {
        if (contains(pageResource.getManifestIdentifier())) {
            getEntries().remove(getEntries().indexOf(getEntry(pageResource)));
        }
    }
    
    public SpineEntry getEntry(final PageResource pageResource) {
        return getEntry(pageResource.getManifestIdentifier());
    }
    
    public SpineEntry getEntry(final String id) {
        for (final SpineEntry entry : getEntries()) {
            if (entry.getIdentifier()
                     .equalsIgnoreCase(id)) {
                return entry;
            }
        }
        
        return null;
    }
    
    public boolean contains(final String id) {
        return getEntry(id) != null;
    }
    
    public SpineEntry changeIndex(final int newIndex, final SpineEntry entry) {
        if (contains(entry.getIdentifier())) {
            getEntries().remove(getEntries().indexOf(entry));
            getEntries().add(newIndex, entry);
        }
        
        return entry;
    }
    
    public List<SpineEntry> getEntries() {
        return spineEntries;
    }
    
    @Value
    public final class SpineEntry {
        
        private final String identifier;
        private final boolean linear;
        
    }
}