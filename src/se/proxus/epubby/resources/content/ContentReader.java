package se.proxus.epubby.resources.content;

import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import se.proxus.epubby.Book;
import se.proxus.epubby.organizer.FolderStructureHelper;
import se.proxus.epubby.resources.pages.Page;
import se.proxus.epubby.resources.Resource;
import se.proxus.epubby.tools.FileTools;
import se.proxus.epubby.tools.StringTools;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@UtilityClass
public final class ContentReader {
    
    public static void readFile(final Book book, final File opfFile) throws IOException {
        final Document document = Jsoup.parse(opfFile, "UTF-8");
        
        book.setVersion(Double.parseDouble(document.selectFirst("package")
                                                   .attr("version")));
        
        book.getLog()
            .info("EpubVersion [" + book.getVersion() + "]");
        
        populateMetaData(book, document);
        
        populateManifestResourceIDs(book, document);
        
        sortPagesAfterSpine(book, document);
        
        // The guide isn't actually mandatory in an EPUB, so there might not be one.
        if (!document.select("guide")
                     .isEmpty()) {
            populateGuide(book, document);
        }
    }
    
    // Jsoup refuses to properly parse the <meta> elements for some reason.
    private static void populateMetaData(final Book book, final Document document) {
        final Element metaDataElement = document.selectFirst("metadata");
        
        for (final Element metaData : metaDataElement.children()) {
            MetaData.Field field = null;
            
            if (metaData.tagName()
                        .equalsIgnoreCase("meta")) {
                final String content = metaData.nextSibling()
                                               .outerHtml()
                                               .trim();
                
                field = book.getContent()
                            .getMetaData()
                            .addField(metaData.tagName(), content);
            } else {
                field = book.getContent()
                            .getMetaData()
                            .addField(metaData.tagName(), metaData.text());
            }
            
            if (metaData.attributes()
                        .size() > 0) {
                for (final Attribute attribute : metaData.attributes()) {
                    field.addAttribute(attribute.getKey(), attribute.getValue());
                }
            }
        }
    }
    
    private static void populateManifestResourceIDs(final Book book, final Document document) {
        final Element manifestElement = document.selectFirst("manifest");
        
        for (final Element manifestEntryElement : manifestElement.children()) {
            final String id = manifestEntryElement.attr("id");
            String href = manifestEntryElement.attr("href");
            
            if (FileTools.getExtension(href)
                         .toLowerCase()
                         .endsWith("jpeg")) {
                href = href.replace("jpeg", "jpg");
            }
            
            final String location =
                FolderStructureHelper.getFolderFromExtension(FileTools.getExtension(href));
            
            href = FolderStructureHelper.fixHref(href);
            
            if (!StringUtils.startsWithIgnoreCase(href, location)) {
                href = location + "/" + href;
            }
            
            final String mediaType = manifestEntryElement.attr("media-type");
            
            final Resource resource = book.getResources()
                                          .getResourceByHref(href);
            
            if (resource == null) {
                book.getLog()
                    .fatal("ManifestLoadError [" + href + ", NOT_A_LOADED_RESOURCE]");
                continue;
            }
            
            if (manifestEntryElement.hasAttr("properties")) {
                final String properties = manifestEntryElement.attr("properties");
                book.getContent()
                    .getManifest()
                    .addEntry(id, resource, mediaType, properties);
            } else {
                book.getContent()
                    .getManifest()
                    .addEntry(id, resource, mediaType);
            }
        }
    }
    
    private static void sortPagesAfterSpine(final Book book, final Document document) {
        final List<Page> spineEntries = new LinkedList<>();
        final List<Boolean> linearEntries = new LinkedList<>();
        
        final Element spineElement = document.selectFirst("spine");
        
        if (spineElement.hasAttr("page-progression-direction")) {
            book.getContent()
                .getSpine()
                .setPageProgressionDirection(spineElement.attr("page-progression-direction"));
        }
        
        for (final Element spineRefElement : spineElement.select("itemref")) {
            final String id = spineRefElement.attr("idref");
            final Resource tempResource = book.getContent()
                                              .getManifest()
                                              .getManifestResourceByID(id);
            final Page tempPage = book.getPages()
                                      .getPage(tempResource);
            boolean linear = false;
            
            spineEntries.add(tempPage);
            
            if (spineRefElement.hasAttr("linear")) {
                linearEntries.add(linear = StringTools.fromYesOrNo(spineRefElement.attr("linear")));
            }
            
            book.getContent()
                .getSpine()
                .addEntry(id, linear);
        }
        
        for (final Page spinePage : spineEntries) {
            final int index = spineEntries.indexOf(spinePage);
            
            if (linearEntries.size() == spineEntries.size()) {
                spinePage.setLinear(linearEntries.get(index));
            }
            
            book.getPages()
                .getLoadedPages()
                .remove(spinePage);
            book.getPages()
                .getLoadedPages()
                .add(index, spinePage);
        }
    }
    
    private static void populateGuide(final Book book, final Document document) {
        final Element guideElement = document.selectFirst("guide");
        
        for (final Element guideEntry : guideElement.select("reference")) {
            final String type = guideEntry.attr("type");
            final String title = guideEntry.attr("title");
            String href = guideEntry.attr("href");
            
            final String location =
                FolderStructureHelper.getFolderFromExtension(FileTools.getExtension(href));
            
            href = FolderStructureHelper.fixHref(href);
            
            if (!StringUtils.startsWithIgnoreCase(href, location)) {
                href = location + "/" + href;
            }
            
            book.getContent()
                .getGuide()
                .addReference(type, title, href);
        }
    }
    
}