package se.proxus.epubby.resources.content;

import lombok.experimental.UtilityClass;
import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import se.proxus.epubby.Book;
import se.proxus.epubby.tools.StringTools;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;

@UtilityClass
public final class ContentWriter {
    
    public static void toFile(final Book book, final File opfFile)
    throws ParserConfigurationException, TransformerException, IOException {
        if (opfFile.exists()) {
            FileUtils.forceDelete(opfFile);
        }
        
        final DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
        final DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
        final Document document = documentBuilder.newDocument();
        final Element packageElement = document.createElement("package");
        
        packageElement.setAttribute("version", "" + book.getVersion());
        packageElement.setAttribute("unique-identifier",
                                    book.getContent()
                                        .getMetaData()
                                        .getIdentifierField()
                                        .getAttribute("id"));
        packageElement.setAttribute("xmlns", "http://www.idpf.org/2007/opf");
        
        if (book.isEpub3()) {
            packageElement.setAttribute("prefix",
                                        "rendition: http://www.idpf.org/vocab/rendition/#");
        }
        
        document.appendChild(packageElement);
        
        appendMetaData(book, document, packageElement);
        
        appendManifest(book, document, packageElement);
        
        appendSpine(book, document, packageElement);
        
        if (book.hasGuide()) {
            appendGuide(book, document, packageElement);
        }
        
        final TransformerFactory transformerFactory = TransformerFactory.newInstance();
        final Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        final DOMSource source = new DOMSource(document);
        final StreamResult result = new StreamResult(opfFile);
        transformer.transform(source, result);
    }
    
    public static void toFile(final Book book)
    throws IOException, TransformerException, ParserConfigurationException {
        toFile(book,
               book.getOpfResource()
                   .getFile());
    }
    
    private static void appendMetaData(final Book book, final Document document,
                                       final Element parentElement) {
        final Element metaDataElement = document.createElement("metadata");
        metaDataElement.setAttribute("xmlns:dc", "http://purl.org/dc/elements/1.1/");
        metaDataElement.setAttribute("xmlns:opf", "http://www.idpf.org/2007/opf");
        parentElement.appendChild(metaDataElement);
        
        for (final MetaData.Field field : book.getContent()
                                              .getMetaData()
                                              .getFields()) {
            final Element fieldElement = document.createElement(field.getName());
            
            if (field.hasAttributes()) {
                for (final String name : field.getAttributes()
                                              .keySet()) {
                    final String value = field.getAttribute(name);
                    
                    fieldElement.setAttribute(name, value);
                }
            }
            
            if (!field.getContent()
                      .trim()
                      .isEmpty()) {
                fieldElement.appendChild(document.createTextNode(field.getContent()));
            }
            
            metaDataElement.appendChild(fieldElement);
        }
    }
    
    private static void appendManifest(final Book book, final Document document,
                                       final Element parentElement) throws IOException {
        final Element manifestElement = document.createElement("manifest");
        parentElement.appendChild(manifestElement);
        
        for (final Manifest.Entry entry : book.getContent()
                                              .getManifest()
                                              .getEntries()) {
            final Element manifestEntryElement = document.createElement("item");
            
            manifestEntryElement.setAttribute("id", entry.getIdentifier());
            manifestEntryElement.setAttribute("href", entry.getHref());
            manifestEntryElement.setAttribute("media-type", entry.getMediaType());
            
            if (entry.hasProperties()) {
                manifestEntryElement.setAttribute("properties", entry.getProperties());
            }
            
            manifestElement.appendChild(manifestEntryElement);
        }
    }
    
    private static void appendSpine(final Book book, final Document document,
                                    final Element parentElement) {
        final Element spineElement = document.createElement("spine");
        spineElement.setAttribute("toc",
                                  book.getNcxResource()
                                      .getManifestIdentifier());
        
        if (book.isEpub3()) {
            spineElement.setAttribute("page-progression-direction",
                                      book.getContent()
                                          .getSpine()
                                          .getPageProgressionDirection());
        }
        
        parentElement.appendChild(spineElement);
        
        for (final Spine.SpineEntry spineEntry : book.getContent()
                                                     .getSpine()
                                                     .getEntries()) {
            final Element spineEntryElement = document.createElement("itemref");
            
            spineEntryElement.setAttribute("idref", spineEntry.getIdentifier());
            
            if (book.isEpub3()) {
                spineEntryElement.setAttribute("linear",
                                               StringTools.toYesOrNo(spineEntry.isLinear()));
            }
            
            spineElement.appendChild(spineEntryElement);
        }
    }
    
    private static void appendGuide(final Book book, final Document document,
                                    final Element parentElement) {
        final Element guideElement = document.createElement("guide");
        parentElement.appendChild(guideElement);
        
        for (final Guide.Reference reference : book.getContent()
                                                   .getGuide()
                                                   .getReferences()) {
            final Element referenceElement = document.createElement("reference");
            
            referenceElement.setAttribute("type", reference.getType());
            referenceElement.setAttribute("title", reference.getTitle());
            referenceElement.setAttribute("href", reference.getHref());
            
            guideElement.appendChild(referenceElement);
        }
    }
}