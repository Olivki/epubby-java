package se.proxus.epubby.resources.stylesheet;

import lombok.Data;
import org.apache.commons.io.FileUtils;
import se.proxus.epubby.Book;
import se.proxus.epubby.resources.Resource;
import se.proxus.epubby.resources.types.StyleSheetResource;
import se.proxus.epubby.tools.FileTools;
import se.proxus.epubby.tools.StringTools;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Data
public final class StyleSheetReaderFactory {
    
    private final List<StyleSheetReader> loadedReaders = new LinkedList<>();
    private final File directory;
    private final Book book;
    private StyleSheetReader mainReader;
    
    public StyleSheetReaderFactory(final Book book) {
        this.book = book;
        directory = FileTools.getFile(book.getTempDirectory(), "Styles");
    }
    
    public void saveStyleSheets() throws IOException {
        for (final StyleSheetReader styleSheetReader : getReaders()) {
            styleSheetReader.toFile();
        }
    }
    
    public StyleSheetReader registerReader(final StyleSheetResource styleSheet) {
        final StyleSheetReader tempReader = new StyleSheetReader(styleSheet);
        
        if (!containsName(styleSheet.getFile()
                                    .getName())) {
            getReaders().add(tempReader);
            getBook().getLog()
                     .info("StyleSheet [" + styleSheet.getLocationHref() + "]");
        }
        
        try {
            tempReader.read();
        } catch (final IOException e) {
            e.printStackTrace();
        }
        
        return tempReader;
    }
    
    public void unregisterReader(final StyleSheetResource styleSheet) {
        if (containsName(styleSheet.getFileName())) {
            getReaders().remove(styleSheet);
        }
    }
    
    public StyleSheetReader createReader(final String styleSheetName, final boolean useExisting) {
        File styleSheetFile = new File(getDirectory(), styleSheetName + ".css");
        
        if (styleSheetFile.exists()) {
            if (!useExisting) {
                for (int numberAppend = 0; numberAppend < 1000; numberAppend++) {
                    styleSheetFile = new File(getDirectory(),
                                              styleSheetName + "_" + StringTools.pad(numberAppend) +
                                              ".css");
                    
                    if (styleSheetFile.exists()) {
                        continue;
                    } else {
                        try {
                            styleSheetFile.createNewFile();
                        } catch (final IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    }
                }
            }
        } else {
            try {
                styleSheetFile.createNewFile();
            } catch (final IOException e) {
                e.printStackTrace();
            }
        }
        
        StyleSheetResource styleSheetResource = null;
        
        try {
            styleSheetResource = (StyleSheetResource) getBook().getResources()
                                                               .addResource(styleSheetFile);
        } catch (final IOException e) {
            e.printStackTrace();
        }
        
        return registerReader(styleSheetResource);
    }
    
    public StyleSheetReader registerExternalReader(final File styleSheetFile) throws IOException {
        final Resource styleSheetResource;
        
        if (!getBook().getResources()
                      .exists(styleSheetFile)) {
            FileUtils.copyFileToDirectory(styleSheetFile, getDirectory());
            final File newFile = new File(getDirectory(), styleSheetFile.getName());
            styleSheetResource = getBook().getResources()
                                          .addResource(newFile);
        } else {
            styleSheetResource = getBook().getResources()
                                          .getResource(styleSheetFile);
        }
        
        final StyleSheetReader tempReader = new StyleSheetReader(styleSheetResource);
        
        if (!containsName(styleSheetFile.getName())) {
            getReaders().add(tempReader);
            getBook().getLog()
                     .info("ExternalStyleSheet [" + FileTools.getLocation(styleSheetFile) + "]");
        }
        
        tempReader.read();
        
        return tempReader;
    }
    
    public void mergeStyleSheets(final StyleSheetReader originalStyleSheet,
                                 final StyleSheetReader destinationStyleSheet,
                                 final boolean overwrite) {
        for (StyleSheetReader.Rule rule : originalStyleSheet.getRules()) {
            if (destinationStyleSheet.containsRuleName(rule.getName()) && overwrite) {
                destinationStyleSheet.removeRule(rule.getName());
                destinationStyleSheet.addRule(rule);
            } else if (!destinationStyleSheet.containsRuleName(rule.getName())) {
                destinationStyleSheet.addRule(rule);
            }
        }
        
        getBook().getResources()
                 .removeResource(originalStyleSheet.getResource());
    }
    
    public StyleSheetReader getReader(final String name) {
        for (final StyleSheetReader reader : getReaders()) {
            if (reader.getResource()
                      .getFileName()
                      .equalsIgnoreCase(fixName(name))) {
                return reader;
            }
        }
        
        return null;
    }
    
    public boolean containsName(final String name) {
        return getReader(name) != null;
    }
    
    public StyleSheetReader getFirstReaderWithRule(final String ruleName) {
        for (final StyleSheetReader styleSheetReader : getReaders()) {
            if (styleSheetReader.containsRuleName(ruleName)) {
                return styleSheetReader;
            }
        }
        
        return null;
    }
    
    public StyleSheetReader getLastReaderWithRule(final String ruleName) {
        final List<StyleSheetReader> tempReaders = new LinkedList<>();
        tempReaders.addAll(getReaders());
        Collections.reverse(tempReaders);
        
        for (final StyleSheetReader styleSheetReader : tempReaders) {
            if (styleSheetReader.containsRuleName(ruleName)) {
                return styleSheetReader;
            }
        }
        
        return null;
    }
    
    public StyleSheetReader getAutomaticReader() {
        if (containsName("epubby_sheet")) {
            return getReader("epubby_sheet");
        } else {
            return createReader("epubby_sheet", true);
        }
    }
    
    public boolean ruleExists(final String ruleName) {
        return getFirstReaderWithRule(ruleName) != null;
    }
    
    private String fixName(final String name) {
        if (!name.endsWith(".css")) {
            return name + ".css";
        }
        
        return name;
    }
    
    public StyleSheetReader getMainReader() {
        return mainReader;
    }
    
    public void setMainReader(final StyleSheetReader mainReader) {
        this.mainReader = mainReader;
        
        getBook().getPages()
                 .addGlobalStyleSheet(mainReader);
    }
    
    public boolean hasMainReader() {
        return mainReader != null;
    }
    
    private Book getBook() {
        return book;
    }
    
    public File getDirectory() {
        return directory;
    }
    
    public List<StyleSheetReader> getReaders() {
        return loadedReaders;
    }
}
