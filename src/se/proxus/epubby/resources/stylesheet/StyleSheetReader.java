package se.proxus.epubby.resources.stylesheet;


import lombok.Data;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import se.proxus.epubby.Book;
import se.proxus.epubby.resources.pages.Page;
import se.proxus.epubby.resources.Resource;
import se.proxus.epubby.tools.StringTools;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Data
public final class StyleSheetReader {
    
    private final List<Rule> rules = new LinkedList<>();
    private final Resource resource;
    
    public void read() throws IOException {
        final List<String> lines = FileUtils.readLines(getResource().getFile(), "UTF-8");
        int startIndex = -1;
        boolean oneBlock = false;
        String ruleName = "NIL";
        
        for (int index = 0; index < lines.size(); index++) {
            final String line = StringUtils.trim(lines.get(index));
            
            if (line.endsWith("{")) {
                if (line.startsWith("{")) {
                    String previousLine = getPreviousLine(index, lines);
                    
                    if (isValidLine(previousLine)) {
                        if (previousLine.startsWith("}")) {
                            previousLine = previousLine.replace("}", "")
                                                       .replace("{", "")
                                                       .trim();
                        }
                        
                        startIndex = index;
                        ruleName = trimRuleName(previousLine);
                        addRule(ruleName);
                    }
                } else {
                    startIndex = index;
                    ruleName = trimRuleName(line);
                    addRule(ruleName);
                }
            } else if (!line.endsWith("{") && line.contains("{")) {
                final String fixedRuleName = StringTools.substring(line, line.indexOf("{") - 1)
                                                        .trim();
                
                startIndex = index;
                ruleName = trimRuleName(fixedRuleName);
                addRule(ruleName);
                
                final String afterBracket = StringUtils.substring(line, line.indexOf("{") + 1)
                                                       .replace("}", "")
                                                       .trim();
                
                if (afterBracket.contains(";")) {
                    for (final String content : afterBracket.split(";")) {
                        addContent(ruleName, content + ";");
                    }
                }
            }
            
            if (startIndex != -1 && startIndex != index && !oneBlock) {
                oneBlock = true;
            }
            
            if (line.startsWith("}") && oneBlock) {
                oneBlock = false;
            }
            
            if (oneBlock && !(line.contains("{") || line.contains("}")) && !line.isEmpty() &&
                !(line.startsWith("/") || line.startsWith("*"))) {
                final String nextLine = getNextLine(index, lines);
                
                if (isValidLine(nextLine)) {
                    if (!(nextLine.equalsIgnoreCase("{"))) {
                        addContent(ruleName, line);
                    }
                }
            }
        }
    }
    
    public File toFile() throws IOException {
        final String content = StringTools.listToStringList(toStringList());
        
        FileUtils.writeStringToFile(getResource().getFile(), content, "UTF-8");
        
        return getResource().getFile();
    }
    
    public List<String> toStringList() throws IOException {
        final List<String> lines = new LinkedList<>();
        
        for (final Rule rule : getRules()) {
            for (final String content : rule.getRuleBlock()) {
                lines.add(content);
            }
            
            lines.add(" ");
        }
        
        return lines;
    }
    
    public List<String> removeRuleBlock(final String ruleName) throws IOException {
        removeRule(ruleName);
        
        return toStringList();
    }
    
    public List<String> removeRuleBlocks(final String[] ruleNames) throws IOException {
        for (final String ruleName : ruleNames) {
            removeRule(ruleName);
        }
        
        return toStringList();
    }
    
    public List<String> removeRuleBlocks(final List<String> ruleNames) throws IOException {
        for (final String ruleName : ruleNames) {
            removeRule(ruleName);
        }
        
        return toStringList();
    }
    
    public String[] getRuleNames() {
        final String[] ruleNames = new String[getRules().size()];
        
        for (int index = 0; index < getRules().size(); index++) {
            ruleNames[index] = getRules().get(index)
                                         .getName();
        }
        
        return ruleNames;
    }
    
    public String getPreviousLine(final int currentIndex, final List<String> lines) {
        final int previousLineIndex = currentIndex - 1;
        
        if (0 > previousLineIndex) {
            return "[NOT_VALID_LINE_INDEX_TOO_LOW]";
        }
        
        return lines.get(previousLineIndex);
    }
    
    public String getNextLine(final int currentIndex, final List<String> lines) {
        final int nextLineIndex = currentIndex + 1;
        
        if (lines.size() < nextLineIndex) {
            return "[NOT_VALID_LINE_INDEX_TOO_HIGH]";
        }
        
        return lines.get(nextLineIndex);
    }
    
    public boolean isValidLine(final String line) {
        return !(StringUtils.containsIgnoreCase(line, "NOT_VALID_LINE"));
    }
    
    public String trimRuleName(final String ruleName) {
        return ruleName.replace("{", "")
                       .replace("}", "")
                       .trim();
    }
    
    public String getValidRuleName(String ruleName) {
        if (ruleName.contains("{") || ruleName.contains("}")) {
            ruleName = trimRuleName(ruleName);
        }
        
        if (containsRuleName(ruleName)) {
            return ruleName;
        } else if (containsRuleName("." + ruleName)) {
            return "." + ruleName;
        }
        
        return ruleName;
    }
    
    public Rule addRule(String ruleName) {
        ruleName = trimRuleName(ruleName);
        
        final Rule rule = new Rule(ruleName);
        
        if (!(containsRuleName(ruleName))) {
            getRules().add(rule);
        }
        
        return rule;
    }
    
    public Rule addRule(final Rule rule) {
        if (!(containsRuleName(rule.getName()))) {
            final Rule tempRule = addRule(rule.getName());
            
            rule.getContent()
                .forEach(tempRule::add);
            
            return tempRule;
        }
        
        return null;
    }
    
    public Rule addAutomaticRule() {
        String ruleName = trimRuleName("epubby_rule");
        
        if (containsRuleName(ruleName)) {
            for (int numberAppend = 0; numberAppend < 1000; numberAppend++) {
                final String tempName = ruleName + "_" + StringTools.pad(numberAppend);
                
                if (containsRuleName(tempName)) {
                    continue;
                } else {
                    ruleName = tempName;
                    break;
                }
            }
        }
        
        final Rule rule = new Rule("." + ruleName);
        
        getRules().add(rule);
        
        return rule;
    }
    
    public boolean containsRuleName(final String ruleName) {
        for (final Rule rule : getRules()) {
            if (rule.getName()
                    .equals(ruleName)) {
                return true;
            } else if (rule.getName()
                           .equals("." + ruleName)) {
                return true;
            }
        }
        
        return false;
    }
    
    public List<Page> getPagesUsingStyleSheet() {
        final List<Page> pagesUsingStyleSheet = getBook().getPages()
                                                         .getLoadedPages()
                                                         .stream()
                                                         .filter(page -> page.getStyleSheets()
                                                                             .contains(this))
                                                         .collect(Collectors.toCollection(LinkedList::new));
        
        return null;
    }
    
    public void removeRule(String ruleName) {
        ruleName = getValidRuleName(ruleName);
        
        if (containsRuleName(ruleName)) {
            final Rule rule = getRule(ruleName);
            
            getRules().remove(getRules().indexOf(rule));
        }
    }
    
    public void addContent(final String ruleName, final String ruleContent) {
        getRule(getValidRuleName(ruleName)).add(ruleContent.trim());
    }
    
    public Rule getRule(final String ruleName) {
        for (final Rule rule : getRules()) {
            if (rule.getName()
                    .equalsIgnoreCase(getValidRuleName(ruleName))) {
                return rule;
            }
        }
        
        return null;
    }
    
    public String getContent(final String ruleName, final String contetName) {
        return getRule(getValidRuleName(ruleName)).getContent(contetName);
    }
    
    public boolean hasContent(final String ruleName, final String contetName) {
        return getRule(getValidRuleName(ruleName)).hasContent(contetName);
    }
    
    public List<Rule> getRules() {
        return rules;
    }
    
    public Resource getResource() {
        return resource;
    }
    
    public Book getBook() {
        return getResource().getBook();
    }
    
    public class Rule {
        
        private String name;
        private final List<String> content = new LinkedList<>();
        
        public Rule(final String name) {
            setName(name);
        }
        
        public void add(final String content) {
            if (!getContent().contains(content)) {
                getContent().add(content);
            }
        }
        
        public List<String> getRuleBlock() {
            final List<String> ruleBlock = new LinkedList<String>();
            
            ruleBlock.add(getName() + " {");
            
            for (final String content : getContent()) {
                ruleBlock.add("    " + content);
            }
            
            ruleBlock.add("}");
            
            return ruleBlock;
        }
        
        public boolean hasContent(final String contentName) {
            for (final String content : getContent()) {
                if (StringUtils.containsIgnoreCase(content, contentName)) {
                    return true;
                }
            }
            
            return false;
        }
        
        public String getContent(final String name) {
            for (final String content : getContent()) {
                if (StringUtils.containsIgnoreCase(content, name)) {
                    return content;
                }
            }
            
            return null;
        }
        
        public String getName() {
            return name;
        }
        
        public String getClassName() {
            if (getName().startsWith(".")) {
                return getName().substring(1);
            } else {
                return getName();
            }
        }
        
        public void setName(final String name) {
            this.name = name;
        }
        
        public List<String> getContent() {
            return content;
        }
    }
}