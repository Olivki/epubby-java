package se.proxus.epubby.resources.toc;

import lombok.Data;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.io.FileUtils;
import se.proxus.epubby.Book;
import se.proxus.epubby.cleaner.CleanerConstants;
import se.proxus.epubby.resources.content.Guide;
import se.proxus.epubby.resources.content.Manifest;
import se.proxus.epubby.resources.pages.Page;
import se.proxus.epubby.resources.Resource;
import se.proxus.epubby.resources.types.PageResource;
import se.proxus.epubby.tools.FileTools;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

@Data
@Setter
@ToString
public final class TableOfContents {

    private final List<TableOfContentsEntry> entries = new LinkedList<>();
    private final Book book;
    private TableOfContentsPage tableOfContentsPage;

    public void init() {
        if (!getEntries().isEmpty()) {
            getEntries().clear();
        }
    }

    public TableOfContentsEntry toEntry(final String title, final Resource resource) {
        return new TableOfContentsEntry(title, resource);
    }

    public TableOfContentsEntry addEntry(final TableOfContentsEntry entry) {
        getEntries().add(entry);

        if (getBook().getSettings().isVerboseLogging()) {
            getBook().getLog().debug(entry.toString());
        }

        return entry;
    }

    public TableOfContentsEntry addEntry(final TableOfContentsEntry entry, final int index) {
        getEntries().add(index, entry);

        if (getBook().getSettings().isVerboseLogging()) {
            getBook().getLog().debug(entry.toString());
        }

        return entry;
    }

    public void savePage() throws IOException {
        final Manifest.Entry tocEntry = getBook().getContent().getManifest().getEntryByProperties(
                "nav");

        if (tocEntry != null) {
            final TableOfContentsPage tocPage =
                    new TableOfContentsPage(getBook(), (PageResource) tocEntry.getResource());

            if (getBook().hasGuide()) {
                final Guide.Reference reference = getBook().getContent().getGuide().getReferenceByType("toc");

                if (reference != null) {
                    reference.setHref(tocPage.getPage().getHref());
                }
            }

            tocPage.toFile();
        } else if (getBook().hasGuide() && getBook().getContent().getGuide().hasEntry("toc")) {
            final Guide.Reference reference =
                    getBook().getContent().getGuide().getReferenceByType("toc");

            if (getBook().getResources().exists(reference.getHref())) {
                final PageResource pageResource = (PageResource) getBook().getResources()
                                                                          .getResourceByHref(reference.getHref());

                final TableOfContentsPage tocPage = new TableOfContentsPage(getBook(), pageResource);

                getBook().getContent().getManifest().getEntry(pageResource).setProperties("nav");

                tocPage.toFile();
            }
        } else {
            final File navFile =
                    new File(FileTools.getDirectory(getBook().getTempDirectory(), "Text"), "nav.xhtml");

            if (navFile.exists()) {
                FileUtils.forceDelete(navFile);
            }

            final PageResource navPage = (PageResource) getBook().getResources().addResource(navFile);

            final TableOfContentsPage tocPage = new TableOfContentsPage(getBook(), navPage);

            getBook().getContent().getGuide().addReference("toc", "Table of Contents", navPage.getHref());

            tocPage.toFile();

            navPage.onLoad();

            getBook().getContent().getManifest().getEntry(navPage).setProperties("nav");
        }

        removeFakeTocPages();
    }

    public void removeFakeTocPages() {
        final List<Page> loadedPagesClone = new LinkedList<>();

        loadedPagesClone.addAll(getBook().getPages().getLoadedPages());

        for (final Page page : loadedPagesClone) {
            CleanerConstants.REMOVE_FAKE_TOC_PAGES.cleanPage(getBook(), page, page.getDocument());
        }

        loadedPagesClone.clear();
    }

    public TableOfContentsEntry addEntry(final String title, final Resource resource) {
        return addEntry(toEntry(title, resource));
    }

    public TableOfContentsEntry addEntry(final String title, final Resource resource, final int index) {
        return addEntry(toEntry(title, resource), index);
    }

    public void removeEntry(final String title) {
        if (!hasEntry(title)) {
            getEntries().remove(getEntries().indexOf(getEntry(title)));
        }
    }

    public void removeEntriesWithTitle(final String title) {
        final List<TableOfContentsEntry> tempEntries = new LinkedList<>();
        tempEntries.addAll(getEntries());

        for (final TableOfContentsEntry entry : getEntries()) {
            if (entry.getTitle().equalsIgnoreCase(title)) {
                tempEntries.remove(tempEntries.indexOf(entry));
                continue;
            }
        }

        getEntries().removeAll(tempEntries);
    }

    public void removeEntry(final PageResource resource) {
        if (!hasEntry(resource)) {
            getEntries().remove(getEntries().indexOf(getEntry(resource)));
        }
    }

    public void removeEntries(final Collection<Resource> entriesToRemove) {
        final List<TableOfContentsEntry> tempEntries = new LinkedList<>();
        tempEntries.addAll(getEntries());

        for (final TableOfContentsEntry entry : getEntries()) {
            entriesToRemove.stream().filter(entryToRemove -> entry.getResource().getHref()
                    .equalsIgnoreCase(entryToRemove.getHref())).forEach(entryToRemove -> {
                tempEntries.remove(tempEntries.indexOf(entryToRemove));
            });
        }

        getEntries().removeAll(tempEntries);
    }

    public TableOfContentsEntry getEntry(final String title) {
        for (final TableOfContentsEntry entry : getEntries()) {
            if (entry.getTitle().equalsIgnoreCase(title)) {
                return entry;
            }
        }

        return null;
    }

    public TableOfContentsEntry getEntry(final PageResource resource) {
        for (final TableOfContentsEntry entry : getEntries()) {
            if (entry.getSimpleHref().equalsIgnoreCase(resource.getHref())) {
                return entry;
            }
        }

        return null;
    }

    public boolean hasEntry(final String title) {
        return getEntry(title) != null;
    }

    public boolean hasEntry(final PageResource resource) {
        return getEntry(resource) != null;
    }

    public List<TableOfContentsEntry> getEntries() {
        return entries;
    }

    public Book getBook() {
        return book;
    }

    public TableOfContentsPage getTableOfContentsPage() {
        return tableOfContentsPage;
    }

    public void setTableOfContentsPage(TableOfContentsPage tableOfContentsPage) {
        this.tableOfContentsPage = tableOfContentsPage;
    }
}