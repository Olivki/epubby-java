package se.proxus.epubby.resources.toc;

import lombok.Data;
import org.apache.commons.io.FileUtils;
import se.proxus.epubby.Book;
import se.proxus.epubby.resources.pages.Page;
import se.proxus.epubby.resources.types.PageResource;
import se.proxus.epubby.resources.stylesheet.StyleSheetReader;

import java.io.IOException;

@Data
public final class TableOfContentsPage {
    
    private final Book book;
    private final PageResource pageResource;
    
    public String buildPageContent() {
        final Builder builder = new Builder();
        
        builder.appendLine("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
        builder.appendLine("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3" +
                           ".org/TR/xhtml11/DTD/xhtml11.dtd\">");
        builder.appendLine(
            "<html xmlns:epub=\"http://www.idpf.org/2007/ops\" xmlns=\"http://www.w3" +
            ".org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">");
        builder.appendLine("<head>");
        builder.appendLine("<meta http-equiv=\"Content-Type\" content=\"application/xhtml+xml; " +
                           "charset=utf-8\" />");
        builder.appendLine("<title>Table of Contents</title>");
        
        for (final StyleSheetReader styleSheetReader : getBook().getStyleSheets()
                                                                .getReaders()) {
            builder.appendLine("<link rel=\"stylesheet\" type=\"text/css\" href=\"" +
                               styleSheetReader.getResource()
                                               .getLocationHref() + "\" />");
        }
        
        builder.appendLine("</head>");
        builder.appendLine();
        builder.appendLine("<body>");
        
        if (getBook().isEpub3()) {
            builder.appendLine("<nav epub:type=\"toc\" id=\"toc\">");
        }
        
        builder.appendLine("<h1>Table of Contents</h1>");
        
        builder.appendLine("<ol>");
        
        for (final TableOfContentsEntry entry : getBook().getTableOfContents()
                                                         .getEntries()) {
            appendEntries(builder, entry);
        }
        
        builder.appendLine("</ol>");
        
        if (getBook().isEpub3()) {
            builder.appendLine("</nav>");
        }
        
        builder.appendLine("</body>");
        builder.appendLine("</html>");
        
        return builder.toString();
    }
    
    public void toFile() throws IOException {
        if (getPageResource().getFile()
                             .exists()) {
            FileUtils.forceDelete(getPageResource().getFile());
            getPageResource().getFile()
                             .createNewFile();
        }
        
        FileUtils.write(getPageResource().getFile(), buildPageContent(), "UTF-8");
        
        getBook().getTableOfContents()
                 .setTableOfContentsPage(this);
    }
    
    private void appendEntries(final Builder builder, final TableOfContentsEntry entry) {
        entry.correctFragmentIdentifier();

        /*if (entry.getSimpleHref().equalsIgnoreCase(getPage().getHref())) {
            entry.setFragmentIdentifier("");
        }*/
        
        builder.appendLine(
            "<li><a href=\"" + "../" + entry.getHref() + "\">" + entry.getTitle() + "</a>");
        
        if (entry.hasChildren()) {
            builder.appendLine("<ol>");
            
            for (final TableOfContentsEntry childEntry : entry.getChildren()) {
                appendEntries(builder, childEntry);
            }
            
            builder.appendLine("</ol>");
            builder.appendLine("</li>");
        } else {
            builder.append("</li>");
        }
    }
    
    public Page getPage() {
        return pageResource.getPage();
    }
    
    final class Builder {
        
        private final StringBuilder builder = new StringBuilder();
        
        public Builder appendLine(final String html) {
            builder.append(html)
                   .append(System.lineSeparator());
            return this;
        }
        
        public Builder append(final String string) {
            builder.append(string);
            return this;
        }
        
        public Builder appendLine() {
            builder.append(System.lineSeparator());
            return this;
        }
        
        @Override
        public String toString() {
            return builder.toString();
        }
    }
    
}
