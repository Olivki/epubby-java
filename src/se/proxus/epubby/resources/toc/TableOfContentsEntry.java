package se.proxus.epubby.resources.toc;

import lombok.*;
import org.jsoup.nodes.Element;
import se.proxus.epubby.Book;
import se.proxus.epubby.resources.pages.Page;
import se.proxus.epubby.resources.Resource;
import se.proxus.epubby.resources.types.PageResource;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

@Data
public final class TableOfContentsEntry {
    
    private String title;
    private PageResource resource;
    private String fragmentIdentifier = "";
    private final List<TableOfContentsEntry> children = new ArrayList<>();
    
    public TableOfContentsEntry(final String title, final PageResource resource) {
        this.title = title;
        this.resource = resource;
    }
    
    public TableOfContentsEntry(final String title, final Resource resource) {
        this(title, (PageResource) resource);
    }
    
    public TableOfContentsEntry addChild(final TableOfContentsEntry entry) {
        getChildren().add(entry);
        
        return entry;
    }
    
    public TableOfContentsEntry addChild(final String title, final Resource resource) {
        return addChild(new TableOfContentsEntry(title, resource));
    }
    
    public void removeChild(final String title) {
        if (contains(title)) {
            getChildren().remove(getChildren().indexOf(getChild(title)));
        }
    }
    
    public void removeChildrenWithTitle(final String title) {
        final List<TableOfContentsEntry> tempEntries = new LinkedList<>();
        tempEntries.addAll(getChildren());
        
        for (final TableOfContentsEntry entry : getChildren()) {
            if (entry.getTitle()
                     .equalsIgnoreCase(title)) {
                tempEntries.remove(tempEntries.indexOf(entry));
                continue;
            }
        }
        
        getChildren().removeAll(tempEntries);
    }
    
    public void removeChild(final Resource resource) {
        if (contains(resource)) {
            getChildren().remove(getChildren().indexOf(getChild(resource)));
        }
    }
    
    public void removeChildren(final Collection<Resource> entriesToRemove) {
        final List<TableOfContentsEntry> tempEntries = new LinkedList<>();
        tempEntries.addAll(getChildren());
        
        for (final TableOfContentsEntry entry : getChildren()) {
            for (final Resource entryToRemove : entriesToRemove) {
                if (entry.getResource()
                         .getHref()
                         .equalsIgnoreCase(entryToRemove.getHref())) {
                    tempEntries.remove(tempEntries.indexOf(entryToRemove));
                }
            }
        }
        
        getChildren().removeAll(tempEntries);
    }
    
    public TableOfContentsEntry getChild(final String title) {
        for (final TableOfContentsEntry entry : getChildren()) {
            if (entry.getTitle()
                     .equalsIgnoreCase(title)) {
                return entry;
            }
        }
        
        return null;
    }
    
    public TableOfContentsEntry getChild(final Resource resource) {
        for (final TableOfContentsEntry entry : getChildren()) {
            if (resource.getHref()
                        .equalsIgnoreCase(resource.getHref())) {
                return entry;
            }
        }
        
        return null;
    }
    
    public boolean contains(final String title) {
        return getChild(title) != null;
    }
    
    public boolean contains(final Resource resource) {
        return getChild(resource) != null;
    }
    
    public String getHref() {
        if (getResource() == null) {
            return "";
        } else {
            return getResource().getHref() + (hasFragment()
                                              ? "#" + getFragmentIdentifier()
                                              : "");
        }
    }
    
    public String getSimpleHref() {
        if (getResource() == null) {
            return "";
        } else {
            return getResource().getHref();
        }
    }
    
    public void correctFragmentIdentifier() {
        if (hasFragment()) {
            final Page page = getResource().getPage();
            final org.jsoup.nodes.Document document = page.getDocument();
            final Element fragmentElement = document.getElementById(getFragmentIdentifier());
            
            if (fragmentElement == null) {
                setFragmentIdentifier("");
            }
        }
    }
    
    public void correctPageLinks(final PageResource newPageResource) {
        if (hasFragment()) {
            final org.jsoup.nodes.Document oldDocument = getResource().getPage()
                                                                      .getDocument();
            final Element oldHrefElement = oldDocument.getElementById(getFragmentIdentifier());
            
            if (oldHrefElement == null) {
                final org.jsoup.nodes.Document newDocument = newPageResource.getPage()
                                                                            .getDocument();
                final Element newHrefElement = newDocument.getElementById(getFragmentIdentifier());
                
                if (newHrefElement == null) {
                    getBook().getLog()
                             .fatal("NewHrefElementNull [" + getResource().getHref() + ", " +
                                    getTitle() + ", " + getFragmentIdentifier() + "]");
                } else {
                    setResource(newPageResource);
                }
            }
        }
    }
    
    public boolean hasFragment() {
        return !getFragmentIdentifier().trim()
                                       .isEmpty();
    }
    
    public void setFragmentIdentifier(final String fragmentIdentifier) {
        this.fragmentIdentifier = fragmentIdentifier;
        
        correctFragmentIdentifier();
    }
    
    public List<TableOfContentsEntry> getChildren() {
        return children;
    }
    
    public boolean hasChildren() {
        return getChildren().size() > 0;
    }
    
    private Book getBook() {
        return getResource().getBook();
    }
    
    /*@Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        
        builder.append("TableOfContentsEntry [")
               .append(getTitle())
               .append(", ");
        
        if (getResource() == null) {
            builder.append("NIL");
        } else {
            builder.append(getHref());
        }
        
        if (hasChildren()) {
            builder.append(", {")
                   .append(getChildren())
                   .append("}]");
        } else {
            builder.append("]");
        }
        
        return builder.toString();
    }*/
}