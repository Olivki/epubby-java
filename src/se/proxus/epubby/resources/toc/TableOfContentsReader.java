package se.proxus.epubby.resources.toc;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import se.proxus.epubby.Book;
import se.proxus.epubby.resources.Resource;
import se.proxus.epubby.tools.FileTools;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public final class TableOfContentsReader {
    
    public static void readFile(final Book book, final File ncxFile)
    throws ParserConfigurationException, IOException, SAXException {
        if (!FileTools.getExtension(ncxFile)
                      .equalsIgnoreCase("NCX")) { // Add some custom exceptions
            book.getLog()
                .error(ncxFile.getName() + " is not the correct file type.");
            return;
        }
        
        final FileInputStream ncxInputStream = new FileInputStream(ncxFile);
        
        final TableOfContents toc = book.getTableOfContents();
        
        toc.init();
        
        final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);
        documentBuilderFactory.setIgnoringComments(true);
        documentBuilderFactory.setIgnoringElementContentWhitespace(true);
        
        final DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        final Document document = documentBuilder.parse(ncxInputStream);
        
        final Node navMap = document.getElementsByTagName("navMap")
                                    .item(0);
        
        for (int index = 0; index < navMap.getChildNodes()
                                          .getLength(); index++) {
            final Node navPoint = navMap.getChildNodes()
                                        .item(index);
            
            if (!navPoint.getNodeName()
                         .equalsIgnoreCase("navPoint")) {
                continue;
            }
            
            if (!navPoint.hasChildNodes()) {
                continue;
            }
            
            final TableOfContentsEntry entry = toc.addEntry(fromNode(toc, navPoint));
            
            if (contains(navPoint, "navPoint")) {
                addAllChildren(toc, navPoint, entry);
            }
        }
        
        ncxInputStream.close();
    }
    
    private static boolean contains(final Node searchNode, final String localName) {
        return getNode(searchNode, localName) != null;
    }
    
    private static Node getNode(final Node searchNode, final String localName) {
        for (int index = 0; index < searchNode.getChildNodes()
                                              .getLength(); index++) {
            final Node node = searchNode.getChildNodes()
                                        .item(index);
            
            if (node.getNodeName()
                    .equalsIgnoreCase(localName)) {
                return node;
            }
        }
        
        return null;
    }
    
    private static String getTitle(final Node node) {
        for (int childIndex = 0; childIndex < node.getChildNodes()
                                                  .getLength(); childIndex++) {
            final Node childNode = node.getChildNodes()
                                       .item(childIndex);
            
            if (childNode.getLocalName()
                         .equalsIgnoreCase("navLabel")) {
                return childNode.getFirstChild()
                                .getTextContent();
            }
        }
        
        return "";
    }
    
    private static TableOfContentsEntry fromNode(final TableOfContents toc, final Node node) {
        String title = "NIL";
        String fragment = "NIL";
        Resource resource = null;
        
        for (int i = 0; i < node.getChildNodes()
                                .getLength(); i++) {
            final Node childNode = node.getChildNodes()
                                       .item(i);
            
            if (childNode.getNodeName()
                         .equalsIgnoreCase("navLabel")) {
                for (int index = 0; index < childNode.getChildNodes()
                                                     .getLength(); index++) {
                    final Node navLabelChildNode = childNode.getChildNodes()
                                                            .item(index);
                    
                    if (navLabelChildNode.getNodeName()
                                         .equalsIgnoreCase("text")) {
                        title = navLabelChildNode.getTextContent();
                    }
                }
            } else if (childNode.getNodeName()
                                .equalsIgnoreCase("content")) {
                final String srcValue = childNode.getAttributes()
                                                 .getNamedItem("src")
                                                 .getNodeValue();
                String href;
                
                if (srcValue.contains("#")) {
                    href = srcValue.split("#")[0];
                    fragment = srcValue.split("#")[1];
                } else {
                    href = srcValue;
                }
                
                resource = toc.getBook()
                              .getResources()
                              .getResourceByHref(href);
            }
        }
        
        final TableOfContentsEntry tocEntry = toc.toEntry(title, resource);
        
        if (!fragment.equalsIgnoreCase("NIL")) {
            tocEntry.setFragmentIdentifier(fragment);
        }
        
        return tocEntry;
    }
    
    private static void addAllChildren(final TableOfContents toc, final Node searchNode,
                                       final TableOfContentsEntry entry) {
        for (int index = 0; index < searchNode.getChildNodes()
                                              .getLength(); index++) {
            final Node node = searchNode.getChildNodes()
                                        .item(index);
            
            if (!node.getLocalName()
                     .equalsIgnoreCase(searchNode.getLocalName())) {
                continue;
            }
            
            final TableOfContentsEntry childEntry = fromNode(toc, node);
            
            entry.addChild(childEntry);
            
            if (contains(node, "navPoint")) {
                addAllChildren(toc, node, childEntry);
            }
        }
    }
    
    private static String getAllParents(final Node node) {
        final StringBuilder builder = new StringBuilder();
        
        if (node.getLocalName()
                .equalsIgnoreCase(node.getParentNode()
                                      .getLocalName())) {
            final Node parent = node.getParentNode();
            
            builder.append(getTitle(node))
                   .append(" - ");
            
            if (parent.getLocalName()
                      .equalsIgnoreCase(parent.getLocalName())) {
                builder.append(getAllParents(parent));
            }
        }
        
        return builder.toString();
    }
}