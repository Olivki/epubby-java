package se.proxus.epubby.resources.toc;

import org.w3c.dom.*;
import se.proxus.epubby.Book;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

public final class TableOfContentsWriter {
    
    private static int index = 0;
    
    public static void toFile(final Book book, final File ncxFile)
    throws TransformerException, ParserConfigurationException {
        index = 0;
        final DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
        final DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
        final Document document = documentBuilder.newDocument();
        final TransformerFactory transformerFactory = TransformerFactory.newInstance();
        final Transformer transformer = transformerFactory.newTransformer();
        
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        
        if (book.getVersion() < 3.0D) {
            final DOMImplementation documentImplementation = document.getImplementation();
            final DocumentType documentType = documentImplementation.createDocumentType("ncx",
                                                                                        "-//NISO" +
                                                                                        "//DTD " +
                                                                                        "ncx " +
                                                                                        "2005-1" +
                                                                                        "//EN",
                                                                                        "http" +
                                                                                        "://www" +
                                                                                        ".daisy" +
                                                                                        ".org" +
                                                                                        "/z3986" +
                                                                                        "/2005" +
                                                                                        "/ncx" +
                                                                                        "-2005-1" +
                                                                                        ".dtd");
            transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, documentType.getPublicId());
            transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, documentType.getSystemId());
        }
        
        final Element ncxElement = document.createElement("ncx");
        ncxElement.setAttribute("version", "2005-1");
        ncxElement.setAttribute("xmlns", "http://www.daisy.org/z3986/2005/ncx/");
        ncxElement.setAttribute("xml:lang", "en");
        
        document.appendChild(ncxElement);
        
        appendHead(book, document, ncxElement);
        
        appendDocTitle(book, document, ncxElement);
        
        appendNavMap(book, document, ncxElement);
        
        final DOMSource source = new DOMSource(document);
        final StreamResult result = new StreamResult(ncxFile);
        transformer.transform(source, result);
    }
    
    public static void toFile(final Book book)
    throws TransformerException, ParserConfigurationException {
        toFile(book,
               book.getNcxResource()
                   .getFile());
    }
    
    private static void appendHead(final Book book, final Document document,
                                   final Element parentElement) {
        final Element headElement = document.createElement("head");
        parentElement.appendChild(headElement);
        
        appendMeta(book.getContent()
                       .getMetaData()
                       .getIdentifier(), "dtb:uid", document, headElement);
        appendMeta("1", "dtb:depth", document, headElement);
        appendMeta("0", "dtb:totalPageCount", document, headElement);
        appendMeta("0", "dtb:maxPageNumber", document, headElement);
    }
    
    private static void appendMeta(final String content, final String name, final Document document,
                                   final Element parentElement) {
        final Element metaElement = document.createElement("meta");
        metaElement.setAttribute("content", content);
        metaElement.setAttribute("name", name);
        parentElement.appendChild(metaElement);
    }
    
    private static void appendDocTitle(final Book book, final Document document,
                                       final Element parentElement) {
        final Element docTitleElement = document.createElement("docTitle");
        parentElement.appendChild(docTitleElement);
        
        docTitleElement.appendChild(textElement(book.getContent()
                                                    .getMetaData()
                                                    .getTitle(), document));
    }
    
    private static void appendDocAuthor(final Book book, final Document document,
                                        final Element parentElement) {
        final Element docAuthorElement = document.createElement("docAuthor");
        parentElement.appendChild(docAuthorElement);
        
        docAuthorElement.appendChild(textElement(book.getContent()
                                                     .getMetaData()
                                                     .getAuthor(), document));
    }
    
    private static void appendNavMap(final Book book, final Document document,
                                     final Element parentElement) {
        final Element navMapElement = document.createElement("navMap");
        parentElement.appendChild(navMapElement);
        
        for (final TableOfContentsEntry tableOfContentsEntry : book.getTableOfContents()
                                                                   .getEntries()) {
            tableOfContentsEntry.correctFragmentIdentifier();
            appendNavPoint(book, document, navMapElement, tableOfContentsEntry);
        }
    }
    
    private static void appendNavPoint(final Book book, final Document document,
                                       final Element parentElement,
                                       final TableOfContentsEntry tableOfContentsEntry) {
        index++;
        final Element navPointElement = navPointElement(book, document, tableOfContentsEntry);
        parentElement.appendChild(navPointElement);
        
        if (tableOfContentsEntry.hasChildren()) {
            for (final TableOfContentsEntry childEntry : tableOfContentsEntry.getChildren()) {
                appendNavPoint(book, document, navPointElement, childEntry);
            }
        }
    }
    
    private static Element navPointElement(final Book book, final Document document,
                                           final TableOfContentsEntry tableOfContentsEntry) {
        final Element navPointElement = document.createElement("navPoint");
        
        navPointElement.setAttribute("id", "navPoint-" + index);
        navPointElement.setAttribute("playOrder", "" + index);
        
        final Element navLabelElement = document.createElement("navLabel");
        
        navLabelElement.appendChild(textElement(tableOfContentsEntry.getTitle(), document));
        navPointElement.appendChild(navLabelElement);
        
        final Element contentElement = document.createElement("content");
        contentElement.setAttribute("src", tableOfContentsEntry.getHref());
        navPointElement.appendChild(contentElement);
        
        return navPointElement;
    }
    
    private static Element textElement(final String text, final Document document) {
        final Element textElement = document.createElement("text");
        final Text textContentElement = document.createTextNode(text);
        
        textElement.appendChild(textContentElement);
        
        return textElement;
    }
}