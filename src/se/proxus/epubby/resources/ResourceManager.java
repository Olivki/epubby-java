package se.proxus.epubby.resources;

import lombok.Data;
import org.apache.commons.io.FileUtils;
import se.proxus.epubby.Book;
import se.proxus.epubby.resources.types.*;
import se.proxus.epubby.tools.FileTools;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Data
public final class ResourceManager {
    
    private final List<Resource> loadedResources = new LinkedList<>();
    private final Book book;
    
    public Resource addResource(final File resource) throws IOException {
        final String name = FileTools.getNameWithoutExtension(resource);
        
        Resource tempResource = null;
        
        switch (FileTools.getExtension(resource)) {
            case "CSS":
                getLoadedResources().add(
                    tempResource = new StyleSheetResource(getBook(), name, resource));
                break;
            
            case "HTML":
                getLoadedResources().add(
                    tempResource = new PageResource(getBook(), name, resource));
                break;
            
            case "XHTML":
                getLoadedResources().add(
                    tempResource = new PageResource(getBook(), name, resource));
                break;
            
            case "PNG":
                getLoadedResources().add(
                    tempResource = new ImageResource(getBook(), name, resource));
                break;
            
            case "JPG":
                getLoadedResources().add(
                    tempResource = new ImageResource(getBook(), name, resource));
                break;
            
            case "JPEG":
                final File newResourceFile = new File(resource.getParentFile(), name + ".jpg");
                final File originalResource = resource;
                resource.renameTo(newResourceFile);
                originalResource.delete();
                
                getLoadedResources().add(
                    tempResource = new ImageResource(getBook(), name, newResourceFile));
                break;
            
            case "GIF":
                getLoadedResources().add(
                    tempResource = new ImageResource(getBook(), name, resource));
                break;
            
            case "NCX":
                getLoadedResources().add(tempResource = new NcxResource(getBook(), name, resource));
                //getBook().getLog().debug(tempResource.toString());
                break;
            
            case "OPF":
                getLoadedResources().add(tempResource = new OpfResource(getBook(), name, resource));
                break;
            
            default:
                getBook().getLog()
                         .debug("UnknownFile [" + resource.getName() + "]");
                break;
        }
        
        if (tempResource != null && !(tempResource instanceof OpfResource)) {
            getBook().getContent()
                     .getManifest()
                     .addEntry("x_" + tempResource.getName()
                                                  .toLowerCase(),
                               tempResource,
                               FileTools.getMediaType(tempResource.getFile()));
        }
        
        if (tempResource != null && getBook().getSettings()
                                             .isVerboseLogging()) {
            getBook().getLog()
                     .debug(tempResource.toString()
                                        .replace("Resource [", "ResourceAdd ["));
        }
        
        return tempResource;
    }
    
    public void resetManifestIDs() {
        for (Resource resource : getLoadedResources()) {
            resource.setManifestIdentifier("x_" + resource.getFileName()
                                                          .toLowerCase());
        }
    }
    
    public void onLoad() {
        final OpfResource opfResource = (OpfResource) getFirstResourceByExtension("opf");
        
        if (opfResource == null) {
            getBook().getLog()
                     .fatal("There's no OPF file in the loaded resources, aborting.");
            return;
        }
        
        getLoadedResources().remove(getLoadedResources().indexOf(opfResource));
        getLoadedResources().add(opfResource);
        
        for (final Resource resource : getLoadedResources()) {
            resource.onLoad();
        }
    }
    
    public Resource getResourceByName(final String resourceName) {
        for (final Resource resource : getLoadedResources()) {
            if (resource.getName()
                        .equalsIgnoreCase(resourceName)) {
                return resource;
            }
        }
        
        return null;
    }
    
    public Resource getResourceByHref(final String href) {
        for (final Resource resource : getLoadedResources()) {
            if (href.equalsIgnoreCase(resource.getHref())) {
                return resource;
            } else if (href.equalsIgnoreCase(resource.getLocationHref())) {
                return resource;
            }
        }
        
        return null;
    }
    
    public Resource getFirstResourceByExtension(final String extension) {
        for (final Resource resource : getLoadedResources()) {
            if (FileTools.getExtension(resource.getFile())
                         .equalsIgnoreCase(extension)) {
                return resource;
            }
        }
        
        return null;
    }
    
    public Resource getResource(final File resourceFile) {
        for (final Resource resource : getLoadedResources()) {
            if (resource.getFile()
                        .getName()
                        .equalsIgnoreCase(resourceFile.getName())) {
                return resource;
            }
        }
        
        return null;
    }
    
    public Resource[] getResourcesByType(final ResourceType resourceType) {
        final List<Resource> tempResources = getLoadedResources().stream()
                                                                 .filter(resource -> resource.getType()
                                                                                             .equals(
                                                                                                 resourceType))
                                                                 .collect(Collectors.toCollection(
                                                                     LinkedList::new));
        
        return tempResources.toArray(new Resource[tempResources.size()]);
    }
    
    public ResourceType getType(final File resourceFile) {
        return getType(FileTools.getExtension(resourceFile));
    }
    
    public ResourceType getType(final String extension) {
        switch (extension.toUpperCase()) {
            case "CSS":
                return ResourceType.STYLES;
            
            case "HTML":
                return ResourceType.PAGES;
            
            case "XHTML":
                return ResourceType.PAGES;
            
            case "PNG":
                return ResourceType.IMAGES;
            
            case "JPG":
                return ResourceType.IMAGES;
            
            case "JPEG":
                return ResourceType.IMAGES;
            
            case "GIF":
                return ResourceType.IMAGES;
            
            case "NCX":
                return ResourceType.NCX;
            
            case "OPF":
                return ResourceType.OPF;
        }
        
        return null;
    }
    
    public void removeResource(final Resource resource) {
        if (exists(resource.getHref())) {
            if (getBook().getSettings()
                         .isVerboseLogging()) {
                getBook().getLog()
                         .debug(resource.toString()
                                        .replace("Resource [", "ResourceRemoval ["));
            }
            
            resource.onUnload();
            getLoadedResources().remove(getLoadedResources().indexOf(resource));
            getBook().getContent()
                     .getManifest()
                     .removeEntry(resource);
            
            if (resource.getFile()
                        .exists()) {
                try {
                    FileUtils.forceDelete(resource.getFile());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            
            System.gc();
        }
    }
    
    public boolean exists(final String href) {
        return getResourceByHref(href) != null;
    }
    
    public boolean exists(final File resourceFile) {
        return getResource(resourceFile) != null;
    }
    
    public List<Resource> getLoadedResources() {
        return loadedResources;
    }
}