package se.proxus.epubby.resources;

import lombok.Getter;
import se.proxus.utils.collections.Arrayz;

@Getter
public enum ResourceType {
    
    PAGES("Text/", "XHTML", "HTML"),
    STYLES("Styles/", "CSS"),
    IMAGES("Images/", "JPG", "JPEG", "PNG", "GIF", "SVG"),
    FONTS("Fonts/", "TTF"),
    AUDIO("Audio/", "MP3", "MPEG", "WAV"),
    VIDEO("Video/", "WEBM", "MP4", "MKV"),
    MISC("Misc/"),
    OPF("", "OPF"),
    NCX("", "NCX");
    
    private final String location;
    private final String[] fileTypes;
    
    ResourceType(final String location, final String... fileTypes) {
        this.location = location;
        this.fileTypes = fileTypes;
    }
    
    public static ResourceType fromExtension(final String extension) {
        for (final ResourceType type : values()) {
            if (Arrayz.contains(type.getFileTypes(), extension.toUpperCase())) { return type; }
        }
        
        return ResourceType.MISC;
    }
}
