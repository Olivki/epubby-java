package se.proxus.epubby.resources;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import se.proxus.epubby.Book;
import se.proxus.epubby.resources.content.Manifest;
import se.proxus.epubby.resources.types.PageResource;
import se.proxus.epubby.tools.FileTools;

import java.io.File;

@Data
@Accessors(chain = true)
public abstract class Resource {
    
    private final Book book;
    private final ResourceType type;
    @NonNull
    private String name;
    @NonNull
    @Setter(AccessLevel.PRIVATE)
    private File file;
    private String manifestIdentifier;
    
    /*public Resource(final Book book, final ResourceType type, final String name, final File
    file) {
        this.book = book;
        this.type = type;
        this.name = name;
        this.file = file;
    }*/
    
    public abstract void onLoad();
    
    public void onUnload() {
    }
    
    public String getFileName() {
        return getFile().getName();
    }
    
    public String getHref() {
        return getType().getLocation() + getFile().getName();
    }
    
    public String getLocationHref() {
        return "../" + getHref();
    }
    
    public void renameResourceFile(final String newName) {
        final String oldFileName = getFile().getName();
        final File newFile = new File(getFile().getParentFile(),
                                      newName + "." + FileTools.getExtension(getFile())
                                                               .toLowerCase());
        final String oldHref = getHref();
        final String oldLocationHref = "../" + getType().getLocation() + oldFileName;
        final Resource oldResource = this;
        
        if (getBook().getSettings()
                     .isVerboseLogging()) {
            getBook().getLog()
                     .debug("ResourceRename [" + getFile().getName() + " -> " + newFile.getName() +
                            "]");
        }
        
        if (getFile().renameTo(newFile)) {
            setFile(newFile);
            name = FileTools.getNameWithoutExtension(newFile);
            final Manifest.Entry manifestEntry = getBook().getContent()
                                                          .getManifest()
                                                          .getEntryByHref(oldHref);
            manifestEntry.setResource(this);
            manifestEntry.updateHref();
            
            if (this instanceof PageResource) {
                final PageResource oldPageResource = (PageResource) oldResource;
                final PageResource newPageResource = (PageResource) this;
                
                if (getBook().getTableOfContents()
                             .hasEntry(oldPageResource)) {
                    getBook().getTableOfContents()
                             .getEntry(oldPageResource)
                             .setResource(newPageResource);
                }
            }
            
            getBook().getPages()
                     .updateHrefReferences(oldLocationHref, this);
        }
    }
    
    /*@Override
    public String toString() {
        return "Resource [" + getLocationHref() + ", " + WordUtils.capitalize(getType().name()) +
               "]";
    }*/
}